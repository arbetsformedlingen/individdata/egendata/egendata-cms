# syntax=docker/dockerfile:1
FROM odoo:15.0

WORKDIR /
USER root
COPY requirements.txt .
RUN apt update -y &&\
    apt install -y python3-dev python3-pip python3-virtualenv libsasl2-dev python-dev libldap2-dev \
        build-essential autoconf libtool pkg-config libffi-dev tzdata && apt remove curl -y &&\
    pip3 install --upgrade pip &&\
    pip3 install -r requirements.txt &&\
# temporary deps, to be removed in production
# COPY requirements_utils.txt .
# RUN pip3 install -r requirements_utils.txt
# Upgrade some pip packages and Clean up
    pip3 install --upgrade certifi &&\
    apt autoremove -y

# Copy extra modules code
COPY ./extra-addons /mnt/extra-addons

# Patch Odoo to prevent longpolling
COPY longpolling.patch /usr/lib/python3/dist-packages/
WORKDIR /usr/lib/python3/dist-packages/
RUN patch -p1 < longpolling.patch
WORKDIR /

# Fix permissions for odoo user
RUN mkdir -p /.local/share/Odoo &&\
    chmod -R 777 /.local/ &&\
    chmod -R 777 /.local/share &&\
    chmod -R 777 /.local/share/Odoo

USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["odoo", "-i", "base"]
