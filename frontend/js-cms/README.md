# CMS Frontend

## Setup:
`npm install`

## Info:
* build.sh has instructions on how to build a deployable javascript file of the frontend 
* serve.sh has instructions on how to get a local hot-reload environment up

* The backend-url is customizable in `src/app/data.service.ts`, all back-end calls are here and this service is injected into other components through their constructors. All calls return an Observable which can be subscribed to with a function that is called once it is finished.

* Design wanted to use components from a package called "digi-components" which are Arbetsförmedlingens own components in order for things to look unified across multiple UIs. These components are located in `node_modules/@digi` but they work haphazardly, sometimes do not render, apply CSS or can not be styled.

* There is a "global" status-modal that is initialized in the `app`-component and a reference to that (`modalEvents`) is passed around to sub-components in order for them to spawn a modal with a status-message. The type for that is `ModalEvent` and the `status`-variable denotes whether the modal is red or green.

* The columns for all tables are stored in `src/app/columns.ts` and are provided to the components that iterate the response from the backend to extract the values. It also extracts distinct values in order to provide a filter.

# Angular generated
# JsCms

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
