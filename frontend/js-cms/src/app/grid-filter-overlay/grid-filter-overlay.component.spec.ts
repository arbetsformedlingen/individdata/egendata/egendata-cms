import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridFilterOverlayComponent } from './grid-filter-overlay.component';

describe('GridFilterOverlayComponent', () => {
  let component: GridFilterOverlayComponent;
  let fixture: ComponentFixture<GridFilterOverlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridFilterOverlayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridFilterOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
