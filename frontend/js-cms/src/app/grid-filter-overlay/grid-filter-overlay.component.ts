import { Component, OnInit,
         Input, Output,
         EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

import { Filter, Column } from '../filter';
import { DisplayColumn } from '../app.component';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';

import { faCoffee, faFilter } from '@fortawesome/free-solid-svg-icons';

export interface FilterOption {
    key: string;
    display: string;
    value: string;
    selected: boolean;
}

@Component({
  selector: 'grid-filter-overlay',
  templateUrl: './grid-filter-overlay.component.html',
  styleUrls: ['./grid-filter-overlay.component.scss']
})
export class GridFilterOverlayComponent implements OnInit, OnChanges {

    // column.display
    // column.key
    // column.uniqueValues
    @Input() column!: DisplayColumn;
    @Input() canBeFiltered!: boolean;
    @Input() indexNumber!: number;
    @Input() currentFilters!: Array<FilterOption>;

    @Output() filterSelect: EventEmitter<Array<FilterOption>> = new EventEmitter();
    @Output() closeAllOtherFilters: EventEmitter<number> = new EventEmitter();

    private shouldCloseSubscription!: Subscription;

    @Input() shouldClose!: Observable<number>;

    private currentlyFilteredValues: Array<any> = [];
    hasActiveRole: boolean = true;
    filterOptions: Array<FilterOption> = [];

    faCoffee = faCoffee;
    faFilter = faFilter;
    
    open: boolean = false;
    
    constructor() { }

    ngOnInit(): void {

        this.shouldCloseSubscription = this.shouldClose.subscribe((value: number) => this.handleShouldClose(value));
        
        let tempFilterOptions: Array<FilterOption> = [
            {key: "sort-desc",
             selected: false,
             value: "",
             display: "Sortera A-Ö"},
            {key: "sort-asc",
             value: "",
             display: "Sortera Ö-A",
             selected: false}
        ];

        let defaultOptions: Array<FilterOption> = [
            {key: "approval",
             value: "-",
             display: "Behörighet",
             selected: true},
            {key: "approval",
             value: "Inaktuell",
             display: "Behörighet",
             selected: true}
        ];

        // this.filterSelect.emit(defaultOptions);
        if (this.currentFilters !== null && this.currentFilters !== undefined) {
            this.currentlyFilteredValues = this.currentFilters.map(cf => {
                return { value: cf.value,
                         selected: cf.selected }
            });
            let instanceHasActiveRole = this.currentFilters.filter((v) => {
                return v.key === this.column.key && v.selected
            }).length > 0;

            for (var i = 0; i < this.currentFilters.length; i++) {
                this.filterOptions.push(this.currentFilters[i]);
            }
            this.hasActiveRole = instanceHasActiveRole;
        }


        
    }

    isCurrentlyFilteredValue(value: String) {
        return this.currentlyFilteredValues.filter((v) => {
            return v.value === value && v.selected
        }).length > 0;
    }

    handleShouldClose(value: number) {
        if (value != this.indexNumber) {
            this.open = false;
        }
    }

    ngOnDestroy() {
        this.shouldCloseSubscription.unsubscribe();
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    toggleFilterOptions(event: any) {
        this.closeAllOtherFilters.emit(this.indexNumber);
        this.open = !this.open;
    }

    inputChange(event: any) {

        const id = event.target.id;
        const key = event.target.getAttribute("key");
        
        const on = (event.target.checked === undefined) ? false : event.target.checked;

        const display = event.target.getAttribute("display");

        if (id === "clear") {
            this.filterOptions = this.filterOptions.filter((fil: FilterOption) => {
                return fil.key !== key;
            });

            // Iterate all checkboxes and clear them
            let checkbox_elements = document.querySelectorAll("input[key=" + key + "][type='checkbox']");
            checkbox_elements.forEach((el: any) => {
                el.checked = false;
            });
        }

        // Get currently selected values
        let currentFilter = this.filterOptions.filter((fil: FilterOption) => {
            return (fil.key === key) && (fil.value === id || fil.value.includes("sort-"));
        });

        if (currentFilter.length > 0 || id === "clear") {

            let filter = currentFilter[0];

            if (filter !== undefined) {

                if (filter.value.includes("sort-")) {
                    filter.value = (filter.value === "sort-desc") ? "sort-asc" : "sort-desc";
                } else {
//                    filter.selected = !filter.selected;
                }
            }
        } else {

            let filterSelected = on;

            if (id.includes("sort")) {
                filterSelected = true;
            }

            let filterOption = {
                key: key,
                value: id,
                display: display,
                selected: filterSelected
            };

            this.filterOptions.push(filterOption);
        }

        this.hasActiveRole = this.filterOptions.filter((v) => {
            return v.key === this.column.key && v.selected;
        }).length > 0;

        // Send our collected filterOptions to our parent
        this.filterSelect.emit(this.filterOptions);
    }

    
    
}
