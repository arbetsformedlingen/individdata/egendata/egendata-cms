import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantPopoverComponent } from './applicant-popover.component';

describe('ApplicantPopoverComponent', () => {
  let component: ApplicantPopoverComponent;
  let fixture: ComponentFixture<ApplicantPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicantPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
