import {
    Component, OnInit, Input,
    Output, EventEmitter, HostListener
} from '@angular/core';

import {
    faTimesCircle,
    faPaperclip,
    faAngleDown,
    faCheckCircle,
    faMinusCircle,
    faPauseCircle,
    faExclamationCircle,
} from '@fortawesome/free-solid-svg-icons';

import { DataService } from '../data.service';

import { ModalEvent } from '../status-modal/status-modal.component';
import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'applicant-popover',
    templateUrl: './applicant-popover.component.html',
    styleUrls: ['./applicant-popover.component.scss'],
})
export class ApplicantPopoverComponent implements OnInit {

    @Input() applicant: any = null;
    @Input() application: any = null;
    @Input() position: Array<Number> = [];
    @Input() width: String = "";
    @Input() calledFromApr: boolean = false;

    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() applicationChange: EventEmitter<any> = new EventEmitter();
    @Output() close = new EventEmitter<any>();

    currentTransform: string = 'scale(0)';
    @Input() transformOrigin: string = "";

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {

        switch (event.key) {
            case "Escape": {

                // Only close popover if no modal is spawned,
                // they have their own key binds
                if (!this.showNotePopoverVal && !this.showContactPopoverVal &&
                    !this.showAppealPopoverVal && !this.showChangeDecisionPopoverVal &&
                    !this.showApprovalPopoverVal) {
                    this.closeHandler();
                }
                break;
            }
            case "Enter": {

                // Only toggle the approval popup if no other pop up is shown
                if (!this.showNotePopoverVal && !this.showContactPopoverVal &&
                    !this.showAppealPopoverVal) {
                    this.showApprovalPopover();
                }
                break;
            }

            default: {
                break;
            }
        }
    }

    showNotePopoverVal: boolean = false;
    showContactPopoverVal: boolean = false;
    showApprovalPopoverVal: boolean = false;
    showAprPopoverVal: boolean = false;
    showConfirmDoneAisPopoverVal: boolean = false;
    showAppealPopoverVal: boolean = false;
    showChangeDecisionPopoverVal: boolean = false;
    showSetInactivePopoverVal: boolean = false;
    showSetRestingPopoverVal: boolean = false;

    editingDecisionType: boolean = false;
    oldApplicantDecisionType: string = "";

    editingAisCaseNumber: boolean = false;
    oldAisCaseNumber: string = "";

    showAllActivities: boolean = false;
    activityLog: Array<any> = [];

    // Enum of 0, 1, 2 depending on which state the application is in
    // Not approved = 0, approved = 1, APR ready = 2
    popoverState: Number = 0;

    faTimesCircle = faTimesCircle;
    faPaperclip = faPaperclip;
    faAngleDown = faAngleDown;
    faCheckCircle = faCheckCircle;
    faMinusCircle = faMinusCircle;
    faPauseCircle = faPauseCircle;
    faExclamationCircle = faExclamationCircle;

    selectedCountry: string = "";
    availableCountries: Array<any> = [
        { display: "Ej EU", value: "non-eu" },
        { display: "EU", value: "eu" },
        { display: "Norden", value: "scandinavia" },
        { display: "Sverige", value: "sweden" },
        { display: "Obehandlad (--)", value: "--" }
    ];

    selectedResidencePermitType: string = "";
    availableResidencePermitTypes: Array<any> = [
        {
            display: "Permanent (PUT)",
            value: "put"
        },
        {
            display: "Permanent (B-PUT)",
            value: "b-put"
        },
        {
            display: "Uppeh. & arbetstillst. (UAT)",
            value: "uat"
        },
        {
            display: "Uppeh. & arbetstillst. (UAT-G)",
            value: "uat-g"
        },
        {
            display: "Uppeh. & arbetstillst. (T-UAT)",
            value: "t-uat"
        },
        {
            display: "Tillf. uppehållstillst. (T-UT)",
            value: "t-ut"
        },
        {
            display: "Uppehållstillst. (UT)",
            value: "ut"
        },
        {
            display: "Uppehållskort (UK)",
            value: "uk"
        }
    ];

    constructor(private dataService: DataService) { }

    ngOnInit(): void {
        let popoverState = 0;

        switch (this.application.approval) {
            case "-":
                popoverState = 0;
                break;
            case "Behörig":
                popoverState = 1;
                break;
            default:
                break;
        }

        if (popoverState == 1 && this.application.apr_status == 'Godkänd') {
            popoverState = 2;
        }

        this.popoverState = popoverState;

        // Show all activities if we have less than 3 entries
        this.showAllActivities = this.application.activity_log.length < 3;
        this.activityLog = (this.showAllActivities) ? this.application.activity_log : this.application.activity_log.slice(0, 2);

        this.selectedCountry = this.availableCountries.filter((ey: any) => ey.display === this.application.applicant.origin_country)[0].value;
        this.selectedResidencePermitType = this.availableResidencePermitTypes.filter((ey: any) => ey.display === this.application.applicant.residence_permit_type)[0].value;

        // For some reason activity_log.attachment_urls comes json encoded
        // The string is "['/attachment/4']", but it does not json parse
        // So for now assume only one attachment per comment and strip [ and ]
        /*        this.activityLog = this.activityLog.map((elem: any) => {
                    elem.attachment_urls = elem.attachment_urls.replace('[', '').replace(']', '')
                        .replaceAll("'", '');
                    elem.attachment_filenames = elem.attachment_filenames.replace("[", "").replace("]", "").replaceAll("'", "");
                    return elem;
                    }); */

        setTimeout(() => {
            this.currentTransform = 'scale(1)';
        }, 250)

    }

    showNotePopover(event: MouseEvent) {
        this.showNotePopoverVal = !this.showNotePopoverVal;
    }

    showContactPopover(event: MouseEvent) {
        this.showContactPopoverVal = !this.showContactPopoverVal;
    }

    showApprovalPopover() {
        this.showApprovalPopoverVal = !this.showApprovalPopoverVal;
    }

    showAprPopover() {
        this.showAprPopoverVal = !this.showAprPopoverVal;
    }

    showConfirmDoneAisPopover() {
        this.showConfirmDoneAisPopoverVal = !this.showConfirmDoneAisPopoverVal;
    }

    showAppealPopover(event: MouseEvent) {
        this.showAppealPopoverVal = !this.showAppealPopoverVal;
    }

    showChangeDecisionPopover(event: Event) {
        this.showChangeDecisionPopoverVal = !this.showChangeDecisionPopoverVal;
    }

    showAllActivitiesToggle(event: Event) {
        this.showAllActivities = true;
        this.activityLog = this.application.activity_log;
    }

    showSetInactivePopover(event: any) {
        this.showSetInactivePopoverVal = !this.showSetInactivePopoverVal;
    }

    showSetRestingPopover(event: any) {
        this.showSetRestingPopoverVal = !this.showSetRestingPopoverVal;
    }

    cancelEditingDecision(event: any) {
        this.editingDecisionType = false;
    }

    startEditingDecision(event: any) {
        this.oldApplicantDecisionType = this.application.decision_type;
        this.editingDecisionType = true;
    }

    saveDecisionType(event: any) {
        this.editingDecisionType = false;

        this.dataService.setDecisionType(this.application.id, this.application.decision_type)
            .subscribe((data: any) => {

                let modalEvent: ModalEvent = {
                    status: true,
                    text: "Sparade beslutsklass"
                };

                this.modalPipe.next(modalEvent);

                let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                this.activityLog.unshift({
                    comment_text: 'Ändrade beslutsklass till ' + "'" + this.application.decision_type + "'",
                    create_date: create_date,
                    attachment_ids: [],
                    attachment: [],
                });

                this.applicationChange.emit(this.application);
            },
                err => {
                    console.log(err);

                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte uppdatera beslutsklass",
                    };

                    this.modalPipe.next(modalEvent);

                    this.application.decision_type = this.oldApplicantDecisionType;
                })
    }

    isDecisionTypeValid(decisionType: string) {
        let validDecisionTypes: Array<string> = [
            "AC",
            "AF",
            "AI",
            "AT",
            "G1",
            "G2",
            "SA",
            "SF",
            "A",
            "A3",
            "A5",
            "A6",
            "A7",
            "A8",
            "A9",
            "AK",
            "AM",
            "AS",
            "AV",
            "G",
            "SÖ",
            "AX",
            "N1",
            "N2",
            "N3",
            "N4",
            "N5",
            "N6",
            "N7",
            "N8",
            "B",
            "BB",
            "BF",
            "BO",
            "BR",
            "B3",
            "B4",
            "B5",
            "B6",
            "B7",
            "B8",
            "K",
            "KB",
            "KF",
            "KK",
            "KR",
            "K1",
            "K4",
            "K5",
            "K7",
            "K8",
            "B2",
            "B9",
            "K2",
            "K9",
            "KV",
            "Q1",
            "Q2",
            "Q3",
            "Q4",
            "R6",
            "R7",
            "M1",
            "M2",
            "M3",
            "M4",
            "M5",
            "M6",
            "T1",
            "T2",
            "T6",
            "TA",
            "TT",
            "T3",
            "F",
            "I",
        ];

        if (!decisionType) {
            return false;
        }

        return validDecisionTypes.includes(decisionType.toUpperCase());
    }

    isSeekerCategoryCodeValid(categoryCode: string) {
        let validDecisionTypes: Array<string> = [
            "11",
            "22",
            "68",
            "69",
            "70",
        ];

        if (!categoryCode) {
            return false;
        }

        return validDecisionTypes.includes(categoryCode);
    }

    cancelEditingAisCaseNumber(event: any) {
        this.editingAisCaseNumber = false;
    }

    startEditingAisCaseNumber(event: any) {
        this.oldAisCaseNumber = this.application.ais_case_number;
        this.editingAisCaseNumber = true;
    }

    saveAisCaseNumber(event: any) {
        this.editingAisCaseNumber = false;

        this.dataService.setAisCaseNumber(this.application.id, this.application.ais_case_number)
            .subscribe((data: any) => {

                let modalEvent: ModalEvent = {
                    status: true,
                    text: "Sparade AIS-ärendenummer"
                };

                this.modalPipe.next(modalEvent);

                let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                this.activityLog.unshift({
                    comment_text: 'Ändrade AIS ärendenummer till ' + "'" + this.application.ais_case_number + "'",
                    create_date: create_date,
                    attachment_ids: [],
                    attachment: [],
                });

                this.applicationChange.emit(this.application);
            },
                err => {
                    console.log(err);

                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte uppdatera AIS-ärendenummer",
                    };

                    this.modalPipe.next(modalEvent);

                    this.application.ais_case_number = this.oldAisCaseNumber;
                })
    }

    isAisCaseNumberValid(caseNumber: string) {
        if (!caseNumber) {
            return false;
        }

        // TODO: Implement case number check if needed
        return true;
    }

    countryChange(newValue: any) {
        this.dataService.changeCountry(this.application.applicant.id, newValue)
            .subscribe((data: any) => {

                let modalEvent: ModalEvent = {
                    status: true,
                    text: "Uppdaterade ursprungsland"
                };

                this.modalPipe.next(modalEvent);

                let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                this.activityLog.unshift({
                    comment_text: 'Ändrade ursprungsland till ' + "'" + newValue + "'",
                    create_date: create_date,
                    attachment_ids: [],
                    attachment: [],
                });

                this.applicationChange.emit(this.application);

            },
                err => {
                    console.log(err);

                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte uppdatera ursprungsland",
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

    residencePermitTypeChange(newValue: any) {

        this.dataService.changeResidencePermitType(this.application.applicant.id, newValue)
            .subscribe((data) => {
                let modalEvent: ModalEvent = {
                    status: true,
                    text: "Uppdaterade uppehållstillstånd"
                };

                this.modalPipe.next(modalEvent);

                let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                this.activityLog.unshift({
                    comment_text: 'Ändrade uppehållstillstånd till ' + "'" + newValue + "'",
                    create_date: create_date,
                    attachment_ids: [],
                    attachment: [],
                });

                this.applicationChange.emit(this.application);
            },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Kunde inte uppdatera uppehållstillstånd"
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

    closeHandler() {
        this.currentTransform = 'scale(0)';

        setTimeout(() => {
            this.close.emit(null);
        }, 500);

    }

}
