import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridPageSelectorComponent } from './grid-page-selector.component';

describe('GridPageSelectorComponent', () => {
  let component: GridPageSelectorComponent;
  let fixture: ComponentFixture<GridPageSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridPageSelectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridPageSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
