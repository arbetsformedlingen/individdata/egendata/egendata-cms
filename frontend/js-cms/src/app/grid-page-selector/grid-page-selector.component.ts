import { Component, OnInit, Input,
         Output, EventEmitter
       } from '@angular/core';

@Component({
  selector: 'grid-page-selector',
  templateUrl: './grid-page-selector.component.html',
  styleUrls: ['./grid-page-selector.component.scss']
})
export class GridPageSelectorComponent implements OnInit {

    @Input() totalPages!: number;

    // Store and retrieve from localStorage
    currentPage: number = 1;
    
    @Output() selectPage: EventEmitter<number> = new EventEmitter();
    
    constructor() { }
    
    ngOnInit(): void {
        this.selectPage.next(1);
    }

    previousPage(event: any) {
        if (this.currentPage > 1) {
            this.currentPage = this.currentPage - 1;
            this.selectPage.next(this.currentPage);
        }
    }

    nextPage(event: any) {
        if (this.currentPage < this.totalPages) {
            this.currentPage = this.currentPage + 1;
            this.selectPage.next(this.currentPage);
        }
    }
}
