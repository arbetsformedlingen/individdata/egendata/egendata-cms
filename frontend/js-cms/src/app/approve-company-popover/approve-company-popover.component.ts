import {
    Component, OnInit, Input,
    Output, EventEmitter,
    HostListener
} from '@angular/core';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';
import { ModalEvent } from '../status-modal/status-modal.component';

import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'approve-company-popover',
    templateUrl: './approve-company-popover.component.html',
    styleUrls: ['./approve-company-popover.component.scss']
})
export class ApproveCompanyPopoverComponent implements OnInit {

    @Input() company: any;

    @Input() position: Array<Number> = [];
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() companyChange: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {

        switch (event.key) {
            case "Escape": {
                this.closeHandler();
                break;
            }
            case "Enter": {
                this.approve();
                break;
            }

            default: {
                break;
            }
        }
    }

    textValue: String;
    barValue: String;
    faPaperclip = faPaperclip;

    constructor(private dataService: DataService) {
        this.textValue = "";
        this.barValue = "";
    }

    ngOnInit(): void {
    }

    closeHandler() {
        this.close.emit(null);
    }

    approve() {
        this.dataService.approveEmployer(this.company.id, this.textValue, this.barValue)
            .subscribe(
                (data: any) => {
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Godkände företag"
                    };
                    this.modalPipe.next(modalEvent);

                    this.company.approval = "OK";
                    this.company.bar_number = this.barValue;

                    let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                    let comment_text = 'Email sent: Employer approved for internships via JobbSprånget ---------- ' + this.textValue;

                    this.company.activity_log.unshift({
                        comment_text: 'Godkände företag, BÄR ärendenummer ' + "'" + this.barValue + "'",
                        create_date: create_date,
                        attachment_ids: [],
                        attachment: [],
                    });

                    this.company.activity_log.unshift({
                        comment_text: comment_text,
                        create_date: create_date,
                        attachment_ids: [],
                        attachment: [],
                    });
                    this.companyChange.emit(this.company);
                    this.closeHandler();
                },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte godkänna företag"
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

}
