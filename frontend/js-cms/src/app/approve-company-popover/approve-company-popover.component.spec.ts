import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveCompanyPopoverComponent } from './approve-company-popover.component';

describe('ApproveCompanyPopoverComponent', () => {
  let component: ApproveCompanyPopoverComponent;
  let fixture: ComponentFixture<ApproveCompanyPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApproveCompanyPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveCompanyPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
