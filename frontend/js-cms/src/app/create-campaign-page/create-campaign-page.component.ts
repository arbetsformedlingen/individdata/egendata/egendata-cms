import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { DataService } from '../data.service';

import { ModalEvent } from '../status-modal/status-modal.component';
import { DisplayColumn } from '../app.component';
import { companyColumns } from '../columns';



interface UnionContact {
    unionName: String;
    unionEmail: String;
}

@Component({
    selector: 'create-campaign-page',
    templateUrl: './create-campaign-page.component.html',
    styleUrls: ['./create-campaign-page.component.scss']
})
export class CreateCampaignPageComponent implements OnInit, OnChanges {

    @Input() modalPipe!: Subject<ModalEvent>;
    @Input() companies!: Array<any>;

    campaignStopDate: string = "2023-07-30";
    uploadSuccess?: Boolean = undefined;
    selectedCsvFile?: File = undefined;

    addUnionName: String = "";
    addUnionEmail: String = "";
    listReadDate?: String = undefined;
    unionContacts: Array<UnionContact> = [];
    createCampaignCompanyColumns: Array<DisplayColumn> = companyColumns;
    showConfirmModalVal: boolean = false;

    constructor(private dataService: DataService) { }

    ngOnChanges(changes: SimpleChanges): void {
    }

    ngOnInit() {
    }

    selectFile(event: Event) {
        const element = event.currentTarget as HTMLInputElement;
        let fileList: FileList | null = element.files;
        if (fileList) {
            const file = fileList[0];
            this.dataService.uploadCampaignCsv(this.campaignStopDate, file.name, file)
                .subscribe((data: any) => {

                    let importLength: Number = data.data.length;
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: `Laddade upp CSV-fil, importerade ${importLength} arbetsgivare`
                    };

                    this.modalPipe.next(modalEvent);
                },
                    (error) => {
                        console.log(error.message);
                        let modalEvent: ModalEvent = {
                            status: true,
                            text: 'Kunde inte ladda up CSV-fil'
                        };

                        this.modalPipe.next(modalEvent);
                    })

        }
    }

    showConfirmModal(event: Event) {
        this.showConfirmModalVal = true;
    }

    cancel(event: any) {
        this.showConfirmModalVal = false;
    }

    confirm(event: any) {

        this.showConfirmModalVal = false;

        this.dataService.confirmCompaniesAndUnionsAndCreateCampaign(this.unionContacts, this.companies).subscribe(
            (data) => {

                let modalEvent: ModalEvent = {
                    status: true,
                    text: "Skapade kampanj"
                };

                this.modalPipe.next(modalEvent);
            },
            err => {
                let modalEvent: ModalEvent = {
                    status: false,
                    text: "Kunde inte skapa kampanj"
                };

                this.modalPipe.next(modalEvent);
            });
    }

    addUnionContactSubmit(event: Event) {

        let unionContact = {
            unionName: this.addUnionName,
            unionEmail: this.addUnionEmail
        };

        this.unionContacts.push(unionContact);
    }

}
