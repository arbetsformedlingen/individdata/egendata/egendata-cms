import { DisplayColumn } from './app.component';

export let companyColumns: Array<DisplayColumn> = [
    {
        key: "display_name",
        display: "Företag",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
    {
        key: "write_date",
        display: "Ändringsdatum",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
    {
        key: "org_number",
        display: "Org.nr",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
    {
        key: "bar_number",
        display: "BÄR-nr",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
    {
        key: "union_approved",
        display: "Fackförbund",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
    {
        key: "approval",
        display: "Godkännande",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
];

export let applicantColumns: Array<DisplayColumn> = [
    {
        key: "name",
        display: "Sökandes namn",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "start_date",
        display: "Ankomstdatum",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "registration_date",
        display: "Inskriven",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "decision_type",
        display: "Beslutsklass",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "seeker_category_code",
        display: "Kategori",
        sort: "",
        extraKey: "",
        uniqueValues: []
    },
    {
        key: "origin_country",
        display: "Ursprungsland",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "approval",
        display: "Behörighet",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
];

export let aprColumns: Array<DisplayColumn> = [
    {
        key: "name",
        display: "Sökandes namn",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "write_date",
        display: "Ändringsdatum",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "start_date",
        display: "Inskriven",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "approval",
        display: "Behörighet",
        sort: "",
        extraKey: "",
        uniqueValues: [],
    },
    {
        key: "apr_status",
        display: "APR-status",
        sort: "",
        extraKey: "fully_done",
        uniqueValues: [],
    },
];
