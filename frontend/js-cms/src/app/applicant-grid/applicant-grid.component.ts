import {
    Component, OnInit,
    Input,
    HostListener
} from '@angular/core';
import { FilterOption } from '../grid-filter-overlay/grid-filter-overlay.component';

import {
    faCoffee, faShieldAlt, faCheckCircle, faSearch, faPauseCircle,
    faAngleDoubleDown, faAngleDown, faExclamationCircle
} from '@fortawesome/free-solid-svg-icons';

import { ModalEvent } from '../status-modal/status-modal.component';
import { Observable, Subject } from 'rxjs';
import { trigger, state, style, animate, transition } from '@angular/animations';

export interface DisplayColumn {
    key: string;
    display: string;
    sort: string;
    extraKey: string,
    uniqueValues: Array<string>
}

/* animations: [
  trigger('flyInOut', [
    state('in', style({ transform: 'translateX(0)' })),
    transition('void => *', [
      style({ transform: 'translateX(-100%)' }),
      animate(100)
    ]),
    transition('* => void', [
      animate(100, style({ transform: 'translateX(100%)' }))
    ])
  ])
] */

@Component({
    selector: 'applicant-grid',
    templateUrl: './applicant-grid.component.html',
    styleUrls: ['./applicant-grid.component.scss'],
    animations: [
        trigger('openClose', [
            state('true', style({ opacity: 1 })),
            state('false', style({ opacity: 0 })),
            transition('false <=> true', animate(500))
        ]),
        trigger('flyInOut', [
            state('in', style({ transform: 'translateX(0)' })),
            transition('void => *', [
                style({ transform: 'translateX(-100%)' }),
                animate(100)
            ]),
            transition('* => void', [
                animate(100, style({ transform: 'translateX(100%)' }))
            ])
        ]),
        trigger(
            'inOutAnimation',
            [
                transition(
                    ':enter',
                    [
                        style({ height: 0, opacity: 0, transform: 'translateX(-100%)' }),
                        animate('0.5s ease-out',
                            style({ height: 500, opacity: 1, transform: 'translateX(0)' }))
                    ]
                ),
                transition(
                    ':leave',
                    [
                        style({ opacity: 1, transform: 'scale(1)' }),
                        animate('0.5s ease-in',
                            style({ opacity: 0, transform: 'scale(0)' }))
                    ]
                )
            ]
        )
    ]
})
export class ApplicantGridComponent implements OnInit {

    @Input() applicants!: Array<any>;
    @Input() columnsDisplayName!: Array<DisplayColumn>;

    @Input() baseApplicants: any = [];

    @Input() baseApplications: any = [];
    @Input() applications: any = [];

    @Input() modalPipe!: Subject<ModalEvent>;

    @HostListener("window:keyup", ["$event"])
    keyUp(event: KeyboardEvent) {

        switch (event.key) {
            case "Enter": {

                // Only click the focused TR if we have no application selected,
                // otherwise the applicant-popover component will handle the events
                if (this.selectedApplication === null) {
                    const activeElement = document.activeElement as HTMLElement;
                    activeElement.click();
                }
                break;
            }
            case "ArrowUp": {
                const allRows = document.querySelectorAll(".pointer");
                const current = document.activeElement as Element;

                // Find the index of the currently focused row
                const elementIndex: number | null = Array.from(allRows).map((row: Element, index: number) => {
                    if (row === current) { return index; }
                    else { return null; }
                }).filter((el: number | null) => el != null)[0];

                // Focus the next element
                if (elementIndex !== null && elementIndex > 0) {
                    const next = elementIndex - 1;
                    (Array.from(allRows)[next] as HTMLElement).focus();
                }

                break;
            }
            case "ArrowDown": {
                const allRows = document.querySelectorAll(".pointer");
                const current = document.activeElement as Element;

                // Find the index of the currently focused row
                const elementIndex: number | null = Array.from(allRows).map((row: Element, index: number) => {
                    if (row === current) { return index; }
                    else { return null; }
                }).filter((el: number | null) => el != null)[0];

                // Focus the next element
                if (elementIndex !== null && elementIndex < Array.from(allRows).length - 1) {
                    const next = elementIndex + 1;
                    (Array.from(allRows)[next] as HTMLElement).focus();
                }

                break;
            }
            default: {
                break;
            }

        }
    }

    focusedRow = null;

    // Font awesome icons
    faCoffee = faCoffee;
    faShieldAlt = faShieldAlt;
    faCheckCircle = faCheckCircle;
    faPauseCircle = faPauseCircle;
    faExclamationCircle = faExclamationCircle;
    faSearch = faSearch;

    selectedApplicant: any = null;
    selectedApplication: any = null;
    popOverPosition: Array<Number> = [];
    popOverTransformOrigin: string = '500px 500px';
    popOverWidth: String = "";
    showApplicantPopover: boolean = false;
    appliedDefaultFilters: boolean = false;
    filters: Array<FilterOption> = [];
    searchValue: string = "";
    shouldCloseFilters: Subject<number> = new Subject<number>();

    totalPages: number = 0;
    canShowMoreApplications = false;
    currentPage: number = 1;
    entriesPerPage: number = 25;
    applicationsByPages: Array<Array<any>> = [];
    showingApplicationsAmount: number = 25;
    faAngleDoubleDown = faAngleDoubleDown;
    faAngleDown = faAngleDown;

    /* behörighet
 ursprungsland
 uppehållstillstånd
 beslutsklass
  */
    filterableColumns: Array<string> = ["approval",
        "origin_country",
        "residence_permit_type",
        "decision_type",
        "seeker_category_code"
    ];

    constructor() { }

    ngOnInit(): void {
        // Get filters from localStorage
        const lsJsData = localStorage.getItem("js-data");

        if (lsJsData !== null) {
            let jsData = JSON.parse(lsJsData);
            let appliedFilters = jsData.filters;
            this.filters = appliedFilters;
        } else {
            this.filters = [
                {
                    key: "approval",
                    value: "--",
                    display: "Behörighet",
                    selected: true
                },
                {
                    key: "approval",
                    value: "Inaktuell",
                    display: "Behörighet",
                    selected: true
                }
            ]
        }
    }

    ngOnChanges(change: any) {

        /* Apply default filters when the user first arrived */
        /* let defaultOptions: Array<FilterOption> = [
            {key: "approval",
             value: "-",
             display: "Behörighet",
             selected: true},
            {key: "approval",
             value: "Inaktuell",
             display: "Behörighet",
             selected: true}
        ]; */

        // We end up here after the REST call to fetch applications has finished
        // we should show a spinner meanwhile
        this.applications = this.applications.map((application: any) => {
            if (!application.seeker_category_code) {
                application.seeker_category_code = "--";
            }

            return application;
        });

        if (!this.appliedDefaultFilters && this.baseApplications.length > 0) {
            this.applications = this.applyFilters(this.baseApplications, this.filters);
            this.totalPages = (Math.floor(this.baseApplications.length / this.entriesPerPage)) + 1;

            let totalApplications = this.baseApplications.length;

            if (totalApplications / this.entriesPerPage) {
                this.applications = this.applications.slice(0, 25);
                this.canShowMoreApplications = true;
            }

            /*            console.log(this.totalPages);
            
                        let totalApplications = this.baseApplications.length;
            
                        // this.showPagination switch here if you want
                        if (totalApplications > this.entriesPerPage) {
                            // index 0..25, 25..49, 49..
                            for (var i = 0; i < this.totalPages - 1; i++) {
                                let sliceStart = i * this.entriesPerPage;
                                let sliceEnd = (i * this.entriesPerPage) + this.entriesPerPage;
                                console.log(sliceStart, sliceEnd);
                                let applications = this.baseApplications.slice(sliceStart, sliceEnd);
                                //console.log(applications);
                                this.applicationsByPages.push(applications);
                            }
                        } */


        }
    }

    showMoreApplications(event: any) {
        this.showingApplicationsAmount = this.showingApplicationsAmount + this.entriesPerPage;
        this.applications = this.applyFilters(this.baseApplications, this.filters);
        this.applications = this.applications.slice(0, this.showingApplicationsAmount);
    }

    selectPage(event: number) {
        this.applications = this.applicationsByPages[event];
    }

    closeFilters(event: number) {
        this.shouldCloseFilters.next(event);
    }

    searchValueChanged(searchValue: any) {
        // Look for applicant name, personal number and case worker and filter
        // the grid based on that

        if (searchValue === "") {
            // Reset to no filters
            this.applications = this.baseApplications;
        } else {

            this.applications = this.baseApplications.filter((appl: any) => {
                return appl["applicant"]["name"].toLowerCase().includes(searchValue.toLowerCase()) || appl["applicant"]["personal_number"].toLowerCase().includes(searchValue.toLowerCase())
                    || appl["case_worker"].toLowerCase().includes(searchValue.toLowerCase());
            });
        }
    }

    expandRow(application: any, event: any) {

        // Get the size of the applicant-grid-div and set the popoverwidth to that
        let applicantGridDiv = document.getElementById("applicant-grid-wrapper") as HTMLElement;

        let targetBounds = event.target.getBoundingClientRect();
        /*         let clickX = event.clientX;
                let clickY = event.clientY;
                console.log(clickY);
                console.log(targetBounds); */
        let transformFromY = targetBounds.y - 200;

        // Tells the page where to animate from
        this.popOverTransformOrigin = '50' + '% ' + transformFromY + 'px';
        let width = window.getComputedStyle(applicantGridDiv, null).getPropertyValue("width");

        let boundingRect = applicantGridDiv.getBoundingClientRect();

        let widthAsNumber = parseInt(width.replace("px", ""));

        let scrollY = event.target.scrollTop;

        let popoverYPosition = transformFromY + scrollY;
        if (transformFromY < boundingRect.y) {
            popoverYPosition = boundingRect.y;
        }

        //this.popOverPosition = [boundingRect.x, event.clientY];
        // Tells the page where to put the pop over
        this.popOverPosition = [boundingRect.x, popoverYPosition];
        //        this.popOverPosition = [boundingRect.x, boundingRect.y];

        if (this.selectedApplication && this.selectedApplication.id === application.id) {
            this.selectedApplication = null;
            this.showApplicantPopover = false;
        } else {

            this.selectedApplication = application;
            this.showApplicantPopover = true;

        }

        if (application.expanded) {
            application.expanded = !application.expanded;
        } else {
            application.expanded = true;
        }

    }

    applyFilters(applicants: Array<any>, filters: Array<FilterOption>): Array<any> {

        let filtered_applicants: any = [];
        let num_filters_applied = 0;
        let sort_filters = [];

        if (filters === undefined || filters === null) {
            return applicants;
        }

        // Iterate filters and check if they're selected i.e applied
        // Else skip
        for (var i = 0; i < filters.length; i++) {
            let filter: FilterOption = filters[i];
            if (!filter.selected) { continue; }

            // Collect sorting options and apply them after filtering
            if (filter.value.includes("sort")) {
                sort_filters.push(filter);
                continue;
            }

            let temp_filtered_applicants = applicants.filter((applicant: any) => {
                return applicant[filter.key] === filter.value;
            });

            /*            for (var j = 0; j < temp_filtered_applicants.length; j++) {
                            filtered_applicants.push(temp_filtered_applicants[j]);
                        } */
            filtered_applicants = filtered_applicants.concat(temp_filtered_applicants);

            num_filters_applied += 1;

        }

        if (num_filters_applied < 1) {

            if (sort_filters.length > 0) {
                console.log(sort_filters);

                // Only use the first sort
                let sort_filter = sort_filters[0];

                if (sort_filter.value.includes("desc")) {
                    applicants.sort(function (a: any, b: any) {
                        if (a[sort_filter.key] < b[sort_filter.key]) return -1;
                        if (a[sort_filter.key] > b[sort_filter.key]) return 1;
                        return 0;
                    });
                } else {
                    applicants.sort(function (a: any, b: any) {
                        if (a[sort_filter.key] > b[sort_filter.key]) return -1;
                        if (a[sort_filter.key] < b[sort_filter.key]) return 1;
                        return 0;
                    });
                }
            }
            return applicants;
        } else {

            if (sort_filters.length > 0) {
            }
            return filtered_applicants;
        }
    }

    filterSelect(appliedFilters: Array<FilterOption>) {

        // Store filters in localstorage
        const lsJsData = localStorage.getItem("js-data");

        if (lsJsData !== null) {
            let jsData = JSON.parse(lsJsData);
            jsData.filters = appliedFilters;
            localStorage.setItem("js-data", JSON.stringify(jsData));
        } else {
            let jsData = { filters: appliedFilters };
            localStorage.setItem("js-data", JSON.stringify(jsData));
        }

        //        this.applicants = this.applyFilters(this.baseApplicants, appliedFilters);
        this.applications = this.applyFilters(this.baseApplications, appliedFilters);
        this.applications = this.applications.slice(0, this.showingApplicationsAmount);
    }

    closeApplicantPopup(event: any) {
        this.showApplicantPopover = false;
        this.selectedApplication = null;
    }

}
