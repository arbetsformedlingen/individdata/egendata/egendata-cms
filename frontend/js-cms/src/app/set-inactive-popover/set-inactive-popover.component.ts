import {
    Component, OnInit, Input, Output, EventEmitter,
    HostListener
} from '@angular/core';
import { Subject } from 'rxjs';

import { ModalEvent } from '../status-modal/status-modal.component';

import { DataService } from '../data.service';


@Component({
    selector: 'set-inactive-popover',
    templateUrl: './set-inactive-popover.component.html',
    styleUrls: ['./set-inactive-popover.component.scss']
})
export class SetInactivePopoverComponent implements OnInit {

    @Input() application: any;
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() applicationChange: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    textContent: string = "";

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {
        switch (event.key) {
            case "Escape": {
                this.closeHandle();
                break;
            }
            case "Enter": {
                this.setStatusInactive(null);
                break;
            }
            default: {
                break;
            }
        }
    }


    constructor(private dataService: DataService) {
        this.textContent = "";
    }

    ngOnInit(): void {
    }

    closeHandle() {
        this.close.emit(null);
    }

    setStatusInactive(event: any) {
        this.dataService.changeDecision(this.application.id, "rejected", this.textContent)
            .subscribe(
                (data: any) => {

                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Satte status Inaktuell"
                    };

                    this.modalPipe.next(modalEvent);

                    this.application.approval = "Inaktuell";
                    let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                    this.application.activity_log.unshift({
                        comment_text: 'Ändrade behörighet till ' + "'Inaktuell'" + " ---------- " + this.textContent,
                        create_date: create_date,
                        attachment_ids: [],
                        attachment: [],
                    });

                    this.applicationChange.emit(this.application);
                    this.closeHandle();
                },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte sätta status Inaktuell"
                    };

                    this.modalPipe.next(modalEvent);
                })

    }

}
