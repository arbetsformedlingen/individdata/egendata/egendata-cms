import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetInactivePopoverComponent } from './set-inactive-popover.component';

describe('SetInactivePopoverComponent', () => {
  let component: SetInactivePopoverComponent;
  let fixture: ComponentFixture<SetInactivePopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetInactivePopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetInactivePopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
