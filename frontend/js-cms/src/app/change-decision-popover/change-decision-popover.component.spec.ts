import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDecisionPopoverComponent } from './change-decision-popover.component';

describe('ChangeDecisionPopoverComponent', () => {
  let component: ChangeDecisionPopoverComponent;
  let fixture: ComponentFixture<ChangeDecisionPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeDecisionPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeDecisionPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
