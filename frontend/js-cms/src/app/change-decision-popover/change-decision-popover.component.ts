import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../data.service';
import { ModalEvent } from '../status-modal/status-modal.component';
import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'change-decision-popover',
    templateUrl: './change-decision-popover.component.html',
    styleUrls: ['./change-decision-popover.component.scss']
})
export class ChangeDecisionPopoverComponent implements OnInit {

    @Input() application: any;
    @Input() position: Array<Number> = [];
    @Output() close: EventEmitter<any> = new EventEmitter();
    @Output() applicationChange: EventEmitter<any> = new EventEmitter();

    @Input() modalPipe!: Subject<ModalEvent>;

    selectedValue: any;

    optionValues: any = [
        {
            "display": "Behörig",
            "value": "eligible",
        },
        {
            "display": "Vilande",
            "value": "pending",
        },
        {
            "display": "Inaktuell",
            "value": "rejected",
        },
        {
            "display": "Rensa",
            "value": "--",
        }
    ];

    constructor(private dataService: DataService) { }

    ngOnInit(): void {
    }

    closeHandle() {
        this.close.emit(null);
    }

    approve() {

        let newValue = this.optionValues.filter((el: any) => el["value"] === this.selectedValue)[0].display;

        if (newValue === "Rensa") {
            newValue = "--";
        }

        this.dataService.changeDecision(this.application.id, this.selectedValue)
            .subscribe(
                (data: any) => {

                    this.application.approval = newValue;
                    this.applicationChange.emit(this.application);

                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Uppdaterade behörighet"
                    };

                    this.modalPipe.next(modalEvent);
                    this.closeHandle();
                },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte uppdatera behörighet"
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

}
