import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteCompanyPopoverComponent } from './note-company-popover.component';

describe('NoteCompanyPopoverComponent', () => {
  let component: NoteCompanyPopoverComponent;
  let fixture: ComponentFixture<NoteCompanyPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoteCompanyPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteCompanyPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
