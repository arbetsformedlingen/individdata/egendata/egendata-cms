import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import {
    GridFilterOverlayComponent,
    FilterOption
} from './grid-filter-overlay/grid-filter-overlay.component';
import { Filter } from './filter';
import {
    companyColumns,
    applicantColumns,
    aprColumns
} from './columns';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Observable, Subject } from 'rxjs';

import { ModalEvent } from './status-modal/status-modal.component';

export interface DisplayColumn {
    key: string;
    display: string;
    sort: string;
    extraKey: string,
    uniqueValues: Array<string>
}

declare var odoo: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Jobbsprånget';

    private odooReference?: any;

    // This is also loaded from localStorage
    view: string = "applicants";

    // Applicants received from back-end
    baseApplicants: any = [];

    baseApplications: any = [];

    applicationsNum: Number = 0;
    applications: any = [];

    // Pagination
    pagination: Object = {};

    // Columns received from back-end
    baseColumns: Array<string> = [];

    // Calculated applicants based on front-end selections
    applicants: any = [];
    applicantsNum: Number = 0;
    companiesNum: Number = 0;

    aprApplications: Array<any> = [];
    aprNum: Number = 0;

    // Joined baseColumns with display name
    columns: Array<DisplayColumn> = [];

    // Calculated columns based on front-end selections
    columnsDisplayName: Array<DisplayColumn> = applicantColumns;

    companyColumns: Array<DisplayColumn> = companyColumns;
    aprColumns: Array<DisplayColumn> = aprColumns;
    companies: Array<any> = [];

    public _reload = true;

    modalEvents: Subject<ModalEvent> = new Subject<ModalEvent>();

    constructor(private dataService: DataService,
        private route: ActivatedRoute,
        private router: Router) {

        try {
            this.odooReference = odoo;

            odoo.define("newguy", function (require: any) {
                var rpc: any = require("web.rpc");
            });
        } catch (error) {
            this.odooReference = null;
        }

    }


    ngOnInit() {

        // Load view from localstorage
        const lsJsData = localStorage.getItem("js-data");

        if (lsJsData !== null) {
            const jsData = JSON.parse(lsJsData);
            let view = (jsData.view !== null && jsData.view !== undefined) ? jsData.view : "applications";
            this.view = view;
        } else {
            //console.log("no data in localstorage");
        }

        // TODO: call this.dataService.getApplications() with "live" arguments
        this.dataService.getApplications(1, 20).subscribe(
            (data: any) => {

                if (data && data.data) {
                    this.baseApplications = data.data;
                    this.applicationsNum = data.data.length;

                    this.pagination = data.pagination;

                    const approved = this.baseApplications.filter((app: any) => {
                        return app.applicant.approval === "eligible";
                    });

                    this.aprNum = approved.length;
                }

                this.setupData();
            },
            (error) => {

                let modalEvent: ModalEvent = {
                    status: false,
                    text: "Kunde inte hämta sökanden"
                };

                this.modalEvents.next(modalEvent);

            });

        this.dataService.getCompanies().subscribe((data: any) => {
            if (data) {
                this.companies = data;
                this.companiesNum = data.length;
            }

            this.calculateUniqueValues(this.companies, this.companyColumns);
        });

        this.dataService.getWhoami().subscribe((data: any) => {
        });


    }

    calculateUniqueValues(applicants: any, columns: Array<DisplayColumn>) {

        // For each column get all values of the applicants table and add
        // uniques to column.uniqueValues

        for (var i = 0; i < columns.length; i++) {
            var column = columns[i];
            const columnKey = column.key;

            for (var j = 0; j < applicants.length; j++) {
                const columnValue = applicants[j][columnKey];

                if (!column.uniqueValues.includes(columnValue)) {
                    column.uniqueValues.push(columnValue);
                }
            }
        }

    }

    setupData() {
        // this.baseColumns should be a list containing column keys
        // we can calulate it based on the keys on applicants instead of
        // getting it from the backend

        const firstApplicant = this.baseApplications[0].applicant;
        const applicantKeys = Object.keys(firstApplicant);

        this.baseColumns = applicantKeys;

        if (this.baseColumns) {

            this.columns = this.baseColumns.map(str => ({ key: str, display: "", sort: '', extraKey: "", uniqueValues: [] }));

            for (var i = 0; i < this.columnsDisplayName.length; i++) {
                const displayColumn = this.columnsDisplayName[i];
                var column = this.columns.filter((col: DisplayColumn) => col.key === displayColumn.key);

                if (column.length > 0) {
                    const columnIndex = this.columns.indexOf(column[0]);

                    var extraKey = "";

                    if (column[0].key === "decision_type") {
                        extraKey = "decision_type_good";
                    } else if (column[0].key === "residence_permit_type") {
                        extraKey = "residence_permit_type_good";
                    } else if (column[0].key === "seeker_category_code") {
                        extraKey = "seeker_category_code_good";
                    }

                    this.columns[columnIndex] = {
                        key: column[0].key,
                        display: displayColumn.display,
                        sort: '',
                        extraKey: extraKey,
                        uniqueValues: [],
                    }

                    displayColumn.extraKey = extraKey;
                }
            }

            this.columns = this.columns.filter((obj: DisplayColumn) => obj.display !== "" || obj.extraKey !== "");

        }

        // Remap backend values of residence_permit_type from
        // ["permanent", "work-visa", "temporary", "permit", "card"] to
        // ["Permanent (PUT)", "Uppeh. & arbetstillst. (T-UAT)", "Tillf. uppehållstills. (T-UT)", "Uppehållstillst. (UT)", "Uppehållskort (UK)"]
        // and origin_country from
        // ["non-eu", "eu", "northern", "sweden", "unprocessed"]
        // to
        // ["Ej EU", "EU", "Norden", "Sverige", "Obehandlad (--)"]
        for (var i = 0; i < this.baseApplicants.length; i++) {
            let applicant = this.baseApplicants[i];

            let newResidencePermitType = "";

            switch (applicant.origin_country) {
                case "non-eu":
                    applicant.origin_country = "Ej EU";
                    break;
                case "eu":
                    applicant.origin_country = "EU";
                    break;
                case "scandinavia":
                    applicant.origin_country = "Norden";
                    break;
                case "sweden":
                    applicant.origin_country = "Sverige";
                    break;
                default:
                    applicant.origin_country = "Obehandlad (--)";
                    break;
            }

            switch (applicant.auto_suggestion_approval) {
                case "eligible":
                    applicant.auto_suggestion_approval = "OK";
                    break;
                case "rejected":
                    applicant.auto_suggestion_approval = "Nej";
                    break;
                default:
                    applicant.auto_suggestion_approval = "Oklar";
            }
            applicant.residence_permit_type = newResidencePermitType;
        }

        if (this.baseApplicants) {
            this.applicants = this.baseApplicants;
        }

        if (this.baseApplications) {
            this.applications = this.baseApplications;

            // Copy keys and values from applications[i].applicant to applications[i]
            for (var i = 0; i < this.applications.length; i++) {
                let application = this.applications[i];
                let applicant = application.applicant;

                let newResidencePermitType = "";

                // Remap date format from YYYY-MM-dd HH:mm to YYYY-MM-dd
                if (applicant.write_date) {
                    applicant.write_date = applicant.write_date.slice(0, 10);
                }

                /* TODO: Break out into a backend-frontend-mappings.js */
                switch (applicant.residence_permit_type) {
                    case "put":
                        newResidencePermitType = "Permanent (PUT)";
                        break;
                    case "b-put":
                        newResidencePermitType = "Permanent (B-PUT)";
                        break;
                    case "uat":
                        newResidencePermitType = "Uppeh. & arbetstillst. (UAT)";
                        break;
                    case "uat-g":
                        newResidencePermitType = "Uppeh. & arbetstillst. (UAT-G)";
                        break;
                    case "t-uat":
                        newResidencePermitType = "Uppeh. & arbetstillst. (T-UAT)";
                        break;
                    case "t-ut":
                        newResidencePermitType = "Tillf. uppehållstillst. (T-UT)"
                        break;
                    case "ut":
                        newResidencePermitType = "Uppehållstillst. (UT)"
                        break;
                    case "uk":
                        newResidencePermitType = "Uppehållskort (UK)"
                        break;
                    default:
                        newResidencePermitType = "Obehandlad (--)";
                }

                switch (applicant.origin_country) {
                    case "non-eu":
                        applicant.origin_country = "Ej EU";
                        break;
                    case "eu":
                        applicant.origin_country = "EU";
                        break;
                    case "scandinavia":
                        applicant.origin_country = "Norden";
                        break;
                    case "sweden":
                        applicant.origin_country = "Sverige";
                        break;
                    default:
                        applicant.origin_country = "Obehandlad (--)";
                        break;
                }

                switch (applicant.auto_suggestion_approval) {
                    case "eligible":
                        applicant.auto_suggestion_approval = "OK";
                        break;
                    case "examine":
                        applicant.auto_suggestion_approval = "Nej";
                        break;
                }

                switch (applicant.approval) {
                    case "eligible":
                        application.approval = "Behörig";
                        break;
                    case "examine":
                        application.approval = "Utreds";
                        break;
                    case "pending":
                        application.approval = "Vilande";
                        break;
                    case "rejected":
                        application.approval = "Inaktuell";
                        break;
                    default:
                        application.approval = "--";
                        break;
                }

                switch (applicant.seeker_category_code) {
                    case "false":
                        applicant.seeker_category_code = "--";
                        break;
                }

                switch (application.apr_status) {
                    case "approved":
                        application.apr_status = "Godkänd";
                        break;
                }

                applicant.residence_permit_type = newResidencePermitType;

                Object.keys(applicant).forEach((key: string) => {

                    if (key !== "approval") {
                        application[key] = applicant[key];
                    }

                });

            }
        }

        this.aprApplications = this.baseApplications.filter((app: any) => app.approval === "Behörig");

        this.calculateUniqueValues(this.applications, this.columnsDisplayName);
        this.calculateUniqueValues(this.aprApplications, this.aprColumns);

        let modalEvent: ModalEvent = {
            status: true,
            text: "Hämtade sökanden"
        };

        this.modalEvents.next(modalEvent);

    }

    applyFilters(applicants: Array<any>, filters: Array<FilterOption>) {

        let filtered_applicants: any = [];
        let num_filters_applied = 0;
        let sort_filters = [];

        // Iterate filters and check if they're selected i.e applied
        // Else skip
        for (var i = 0; i < filters.length; i++) {
            let filter: FilterOption = filters[i];
            if (!filter.selected) { continue; }

            // Collect sorting options and apply them after filtering
            if (filter.value.includes("sort")) {
                sort_filters.push(filter);
                continue;
            }

            let temp_filtered_applicants = applicants.filter((applicant: any) => {
                return applicant[filter.key] === filter.value;
            });

            filtered_applicants = filtered_applicants.concat(temp_filtered_applicants);

            num_filters_applied += 1;

        }

        if (num_filters_applied < 1) {

            if (sort_filters.length > 0) {

                // Only use the first sort
                let sort_filter = sort_filters[0];

                if (sort_filter.value.includes("desc")) {
                    applicants.sort(function (a: any, b: any) {
                        if (a[sort_filter.key] < b[sort_filter.key]) return -1;
                        if (a[sort_filter.key] > b[sort_filter.key]) return 1;
                        return 0;
                    });
                } else {
                    applicants.sort(function (a: any, b: any) {
                        if (a[sort_filter.key] > b[sort_filter.key]) return -1;
                        if (a[sort_filter.key] < b[sort_filter.key]) return 1;
                        return 0;
                    });
                }
            }
            return applicants;
        } else {

            if (sort_filters.length > 0) {
            }
            return filtered_applicants;
        }
    }

    private reload() {
        setTimeout(() => this._reload = false);
        setTimeout(() => this._reload = true);
    }

    filterSelect(appliedFilters: Array<FilterOption>) {
        this.applicants = this.applyFilters(this.baseApplicants, appliedFilters);
    }

    columnSelect(event: any) {
        const selectedInput: string = event.target.value;

        // selectedInput = displayName [eg Personnnummer],
        // get

        const key = this.columns.filter((col: DisplayColumn) => col.display === selectedInput)[0].key;
        const extraKey = this.columns.filter((col: DisplayColumn) => col.display === selectedInput)[0].extraKey;

        const isShown = this.columnsDisplayName.filter((col: DisplayColumn) => col.display === selectedInput).length > 0;
        // If the column is shown, remove it, else add it
        if (isShown) {
            this.columnsDisplayName = this.columnsDisplayName.filter((col: any) => col.display !== selectedInput);
        } else {
            this.columnsDisplayName.push({
                key: key,
                display: selectedInput,
                sort: '',
                extraKey: extraKey,
                uniqueValues: []
            });
        }

        this.reload();
    }

    expandRow(event: any) { }

    setSort(event: any) {
        const sortColumn = event.target.getAttribute("sort-value");

        if (sortColumn === "" || sortColumn === undefined) {
            console.error("Sort error, could not get attribute 'sort-value'");
        }

        this.columnsDisplayName = this.columnsDisplayName.map((col: DisplayColumn) => {
            if (col.key === sortColumn) {
                return {
                    key: col.key, display: col.display,
                    sort: (col.sort === "desc") ? "asc" : "desc",
                    extraKey: col.extraKey,
                    uniqueValues: col.uniqueValues
                };

            } else {
                return { key: col.key, display: col.display, sort: '', extraKey: col.extraKey, uniqueValues: col.uniqueValues };
            }

        });

        this.sortApplicants();
        //        this.reload();


    }

    showSortOptions(event: any) { }

    sortApplicants() {
        const sortOnKey = this.columnsDisplayName.filter((col: DisplayColumn) => col.sort === "asc" || col.sort === "desc");

        if (sortOnKey.length > 0) {
            const sortKey = sortOnKey[0].key;

            if (sortOnKey[0].sort === "desc") {

                this.applicants.sort(function (a: any, b: any) {
                    if (a[sortKey] < b[sortKey]) return -1;
                    if (a[sortKey] > b[sortKey]) return 1;
                    return 0;
                });
            } else {
                this.applicants.sort(function (a: any, b: any) {
                    if (a[sortKey] > b[sortKey]) return -1;
                    if (a[sortKey] < b[sortKey]) return 1;
                    return 0;
                });
            }

        } else {
            //console.log("I want to sort but I don't know on what");
        }
    }

    changeView(viewName: string) {
        const lsJsData = localStorage.getItem("js-data");

        if (lsJsData !== null) {
            let jsData = JSON.parse(lsJsData);
            jsData.view = viewName;
            localStorage.setItem("js-data", JSON.stringify(jsData));
        } else {
            let jsData = { view: viewName };
            localStorage.setItem("js-data", JSON.stringify(jsData));

        }
        this.view = viewName;
    }
}
