import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'confirm-create-campaign-modal',
  templateUrl: './confirm-create-campaign-modal.component.html',
  styleUrls: ['./confirm-create-campaign-modal.component.scss']
})
export class ConfirmCreateCampaignModalComponent implements OnInit {

    @Output() onConfirm: EventEmitter<any> = new EventEmitter();
    @Output() onCancel: EventEmitter<any> = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

    confirm(event: any) {
      this.onConfirm.next(null);
  }

    cancel(event: any) {
      this.onCancel.next(null);
  }

}
