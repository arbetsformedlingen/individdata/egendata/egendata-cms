import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmCreateCampaignModalComponent } from './confirm-create-campaign-modal.component';

describe('ConfirmCreateCampaignModalComponent', () => {
  let component: ConfirmCreateCampaignModalComponent;
  let fixture: ComponentFixture<ConfirmCreateCampaignModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmCreateCampaignModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmCreateCampaignModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
