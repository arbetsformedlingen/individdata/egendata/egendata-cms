import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetRestingPopoverComponent } from './set-resting-popover.component';

describe('SetRestingPopoverComponent', () => {
  let component: SetRestingPopoverComponent;
  let fixture: ComponentFixture<SetRestingPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetRestingPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetRestingPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
