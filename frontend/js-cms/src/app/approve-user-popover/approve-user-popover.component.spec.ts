import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveUserPopoverComponent } from './approve-user-popover.component';

describe('ApproveUserPopoverComponent', () => {
  let component: ApproveUserPopoverComponent;
  let fixture: ComponentFixture<ApproveUserPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApproveUserPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveUserPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
