import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyPopoverComponent } from './company-popover.component';

describe('CompanyPopoverComponent', () => {
  let component: CompanyPopoverComponent;
  let fixture: ComponentFixture<CompanyPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
