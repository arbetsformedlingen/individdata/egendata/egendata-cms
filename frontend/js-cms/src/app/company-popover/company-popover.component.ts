import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

import { DataService } from '../data.service';

import { ModalEvent } from '../status-modal/status-modal.component';

import { Observable, Subject } from 'rxjs';

import {
    faTimesCircle,
    faPaperclip,
    faAngleDown,
    faCheckCircle,
    faExclamationCircle,
    faMinusCircle,
    faPauseCircle,
} from '@fortawesome/free-solid-svg-icons';


@Component({
    selector: 'company-popover',
    templateUrl: './company-popover.component.html',
    styleUrls: ['./company-popover.component.scss']
})
export class CompanyPopoverComponent implements OnInit {

    @Input() company: any = null;
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() companyChange: EventEmitter<any> = new EventEmitter();
    @Output() close = new EventEmitter<any>();

    activityLog: Array<any> = [];
    currentTransform: string = 'scale(0)';

    showApprovalPopoverVal: boolean = false;
    showExaminePopoverVal: boolean = false;
    showNotePopoverVal: boolean = false;

    faTimesCircle = faTimesCircle;
    faPaperclip = faPaperclip;
    faAngleDown = faAngleDown;
    faCheckCircle = faCheckCircle;
    faExclamationCircle = faExclamationCircle;
    faMinusCircle = faMinusCircle;
    faPauseCircle = faPauseCircle;


    textContent: string = "";

    constructor(private dataService: DataService) {
        this.textContent = "";
    }

    ngOnInit(): void {
        this.activityLog = this.company.activity_log;

        setTimeout(() => {
            this.currentTransform = 'scale(1)';
        }, 250);
    }

    closeHandler() {
        this.currentTransform = 'scale(0)';

        setTimeout(() => {
            this.close.emit(null);
        }, 500);
    }

    showApprovalPopover() {
        this.showApprovalPopoverVal = !this.showApprovalPopoverVal;
    }

    showExaminePopover() {
        this.showExaminePopoverVal = !this.showExaminePopoverVal;
    }

    showNotePopover() {
        this.showNotePopoverVal = !this.showNotePopoverVal;
    }

}
