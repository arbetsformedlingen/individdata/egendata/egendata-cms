import { Component, OnInit, Input } from '@angular/core';

import { DisplayColumn } from '../applicant-grid/applicant-grid.component';
import { faCoffee, faShieldAlt, faCheckCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

interface GridRow {
}

@Component({
    selector: 'grid-component',
    templateUrl: './grid-component.component.html',
    styleUrls: ['./grid-component.component.scss']
})
export class GridComponentComponent implements OnInit {

    @Input() columns!: Array<DisplayColumn>;

    @Input() values!: Array<any>;

    // Font awesome icons
    faCoffee = faCoffee;
    faShieldAlt = faShieldAlt;
    faCheckCircle = faCheckCircle;
    faExclamationCircle = faExclamationCircle;

    constructor() { }

    ngOnInit(): void {
    }

    filterSelect(event: any) {
    }

    expandRow(event: any) {
    }

}
