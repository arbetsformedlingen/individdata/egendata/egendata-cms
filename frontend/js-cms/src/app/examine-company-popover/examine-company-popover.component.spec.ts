import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamineCompanyPopoverComponent } from './examine-company-popover.component';

describe('ExamineCompanyPopoverComponent', () => {
  let component: ExamineCompanyPopoverComponent;
  let fixture: ComponentFixture<ExamineCompanyPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamineCompanyPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamineCompanyPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
