import {
    Component, OnInit, Input,
    Output, EventEmitter,
    HostListener
} from '@angular/core';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';
import { ModalEvent } from '../status-modal/status-modal.component';

import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'examine-company-popover',
    templateUrl: './examine-company-popover.component.html',
    styleUrls: ['./examine-company-popover.component.scss']
})
export class ExamineCompanyPopoverComponent implements OnInit {

    @Input() company: any;

    @Input() position: Array<Number> = [];
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() companyChange: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {

        switch (event.key) {
            case "Escape": {
                this.closeHandler();
                break;
            }

            default: {
                break;
            }
        }
    }

    textValue: String;
    barValue: String;
    faPaperclip = faPaperclip;

    constructor(private dataService: DataService) {
        this.textValue = "";
        this.barValue = "";
    }

    ngOnInit(): void {
    }

    closeHandler() {
        this.close.emit(null);
    }

    examine() {
        this.dataService.changeEmployerDecision(this.company.id, 'examine')
            .subscribe(
                (data: any) => {
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Ändrade godkännande till 'examine'"
                    };
                    this.modalPipe.next(modalEvent);

                    this.company.approval = "examine";

                    let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');

                    this.company.activity_log.unshift({
                        comment_text: "Ändrade godkännande till 'examine'",
                        create_date: create_date,
                        attachment_ids: [],
                        attachment: [],
                    });

                    this.companyChange.emit(this.company);
                    this.closeHandler();
                },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte ändra godkännande på företag"
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

}
