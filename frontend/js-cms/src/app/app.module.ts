import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { XmlrpcModule } from 'angular2-xmlrpc';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { JsTableRowComponent } from './js-table-row/js-table-row.component';
import { GridFilterOverlayComponent } from './grid-filter-overlay/grid-filter-overlay.component';

import { AppRoutingModule } from './app-routing.module';
import { ApplicantGridComponent } from './applicant-grid/applicant-grid.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CompanyGridComponent } from './company-grid-component/company-grid-component.component';
import { JsHeaderComponent } from './js-header/js-header.component';
import { GridComponentComponent } from './grid-component/grid-component.component';
import { ApplicantPopoverComponent } from './applicant-popover/applicant-popover.component';
import { NotePopoverComponent } from './note-popover/note-popover.component';
import { NoteCompanyPopoverComponent } from './note-company-popover/note-company-popover.component';
import { ContactUserPopoverComponent } from './contact-user-popover/contact-user-popover.component';
import { ApproveUserPopoverComponent } from './approve-user-popover/approve-user-popover.component';
import { ApproveCompanyPopoverComponent } from './approve-company-popover/approve-company-popover.component';
import { ExamineCompanyPopoverComponent } from './examine-company-popover/examine-company-popover.component';
import { StatusModalComponent } from './status-modal/status-modal.component';
import { AprGridComponent } from './apr-grid/apr-grid.component';
import { ApproveAprPopoverComponent } from './approve-apr-popover/approve-apr-popover.component';
import { ConfirmDoneAisPopoverComponent } from './confirm-done-ais-popover/confirm-done-ais-popover.component';
import { ChangeDecisionPopoverComponent } from './change-decision-popover/change-decision-popover.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridPageSelectorComponent } from './grid-page-selector/grid-page-selector.component';
import { CreateCampaignPageComponent } from './create-campaign-page/create-campaign-page.component';
import { SetInactivePopoverComponent } from './set-inactive-popover/set-inactive-popover.component';
import { SetRestingPopoverComponent } from './set-resting-popover/set-resting-popover.component';
import { ConfirmCreateCampaignModalComponent } from './confirm-create-campaign-modal/confirm-create-campaign-modal.component';
import { CompanyPopoverComponent } from './company-popover/company-popover.component';


@NgModule({
    declarations: [
        AppComponent,
        JsTableRowComponent,
        GridFilterOverlayComponent,
        ApplicantGridComponent,
        CompanyGridComponent,
        JsHeaderComponent,
        GridComponentComponent,
        ApplicantPopoverComponent,
        NotePopoverComponent,
        NoteCompanyPopoverComponent,
        ContactUserPopoverComponent,
        ApproveUserPopoverComponent,
        ApproveCompanyPopoverComponent,
        ExamineCompanyPopoverComponent,
        StatusModalComponent,
        AprGridComponent,
        ApproveAprPopoverComponent,
        ConfirmDoneAisPopoverComponent,
        ChangeDecisionPopoverComponent,
        GridPageSelectorComponent,
        CreateCampaignPageComponent,
        SetInactivePopoverComponent,
        SetRestingPopoverComponent,
        ConfirmCreateCampaignModalComponent,
        CompanyPopoverComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        FontAwesomeModule,
        FormsModule,
        XmlrpcModule,
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],  
})
export class AppModule { }
