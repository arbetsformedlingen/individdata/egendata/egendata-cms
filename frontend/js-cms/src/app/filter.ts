
export interface Column {
    key: string;
    sort?: string;
    values: Array<string>;
}

export interface Sorting {
    column: Column;
}

export interface Filter {
    column: Column;
}
