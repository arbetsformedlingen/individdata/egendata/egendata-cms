import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { AppComponent } from './app.component';
import { ApplicantGridComponent } from './applicant-grid/applicant-grid.component';
import { CompanyGridComponent } from './company-grid-component/company-grid-component.component';

const routes: Routes = [
    { path: 'casemanagement/', component: AppComponent },
    { path: 'casemanagement/companies', component: CompanyGridComponent },
    { path: 'apr-decisions', component: AppComponent },
];

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
