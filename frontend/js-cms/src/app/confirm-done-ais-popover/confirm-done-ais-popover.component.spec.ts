import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDoneAisPopoverComponent } from './confirm-done-ais-popover.component';

describe('ApproveAprPopoverComponent', () => {
  let component: ConfirmDoneAisPopoverComponent;
  let fixture: ComponentFixture<ConfirmDoneAisPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmDoneAisPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDoneAisPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
