import {
    Component, OnInit, Input,
    Output, EventEmitter,
    HostListener
} from '@angular/core';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';
import { ModalEvent } from '../status-modal/status-modal.component';

import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'confirm-done-ais-popover',
    templateUrl: './confirm-done-ais-popover.component.html',
    styleUrls: ['./confirm-done-ais-popover.component.scss']
})
export class ConfirmDoneAisPopoverComponent implements OnInit {

    @Input() application: any;

    @Input() position: Array<Number> = [];
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() applicationChange: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {

        switch (event.key) {
            case "Escape": {
                this.closeHandler();
                break;
            }
            case "Enter": {
                this.confirm();
                break;
            }

            default: {
                break;
            }
        }
    }

    textValue: String;
    faPaperclip = faPaperclip;

    constructor(private dataService: DataService) {
        this.textValue = "";
    }

    ngOnInit(): void {
    }

    closeHandler() {
        this.close.emit(null);
    }

    confirm() {
        console.log("confirming done in AIS");
        this.dataService.confirmDoneAis(this.application.id)
            .subscribe(
                (data: any) => {
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Bekräftade klar i AIS"
                    };
                    this.modalPipe.next(modalEvent);

                    this.application.done_in_ais = true;
                    this.applicationChange.emit(this.application);
                    this.closeHandler();
                },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: false,
                        text: "Kunde inte bekräfta klar i AIS"
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

}
