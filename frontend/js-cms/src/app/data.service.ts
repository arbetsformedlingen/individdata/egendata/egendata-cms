import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { XmlrpcService } from 'angular2-xmlrpc';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    // When deploying inject with deployment url
    private REST_API_SERVER = "/casemanagement";

    constructor(private httpClient: HttpClient, private xmlRpc: XmlrpcService) {
        console.log("REST_API_SERVER: ", this.REST_API_SERVER);
    }

    public getApplicants() {
        return this.httpClient.get(this.REST_API_SERVER + "/applicant");
    }

    public getCompanies() {
        return this.httpClient.get(this.REST_API_SERVER + "/employer");
    }

    public getWhoami() {
        return this.httpClient.put(this.REST_API_SERVER + "/whoami", {});
    }

    public saveApplicantNote(applicant_id: Number, note: string, file: File | undefined) {

        let url = this.REST_API_SERVER + "/applicant/" + applicant_id + "/note";

        let noteFormData = new FormData();

        if (file !== undefined) {
            noteFormData.append("attachment", file, file.name);
        }

        noteFormData.append("note", note);

        return this.httpClient.put(url, noteFormData);
    }

    public saveEmployerNote(employer_id: Number, note: string, file: File | undefined) {

        let url = this.REST_API_SERVER + "/employer/" + employer_id + "/note";

        let noteFormData = new FormData();

        if (file !== undefined) {
            noteFormData.append("attachment", file, file.name);
        }

        noteFormData.append("note", note);

        return this.httpClient.put(url, noteFormData);
    }

    public getApplications(page: Number, perPage: Number | undefined) {

        let url = this.REST_API_SERVER + "/application";

        if (page && perPage) {
            url += "/" + page + "/" + perPage;
        } else if (page) {
            url += "/" + page;
        }

        return this.httpClient.get(url);
    }

    public exampleRpcCalls() {

        let url = "http://localhost:8069";

        let xmlRpc = this.xmlRpc.callMethod(
            url + "/xmlrpc/2/common",
            "version",
            []
        )

        xmlRpc.subscribe(data => console.log(this.xmlRpc.parseResponse(data)));

        let uid = null;

        let login = this.xmlRpc.callMethod(
            url + "/xmlrpc/2/common",
            "login",
            ["odooapp", "admin", "admin"]
        );

        let authenticate = this.xmlRpc.callMethod(
            url + "/xmlrpc/2/common",
            "authenticate",
            ["odooapp", "admin", "admin", {}]
        );

        login.subscribe(data => console.log(this.xmlRpc.parseResponse(data)));

        authenticate.subscribe(data => {

            uid = this.xmlRpc.parseResponse(data);

            let modelsUrl = url + "/xmlrpc/2/object";

            let modelsCall = this.xmlRpc.callMethod(
                modelsUrl,
                "execute_kw",
                ["odooapp", "" + uid, "admin", "casemanagement.application", "search", [[]]]
            );

            modelsCall.subscribe(data => { console.log(data); console.log(this.xmlRpc.parseResponse(data))});

        });
    }

    /*
     *   /casemanagement/applicant/email/application_id
     */
    public sendApplicantEmail(application_id: Number, emailText: string, file?: File) {

        let url = this.REST_API_SERVER + "/applicant/email/" + application_id;

        let emailFormData = new FormData();

        if (file !== undefined) {
            emailFormData.append("attachment", file, file.name);
        }
        emailFormData.append("content", emailText);
        emailFormData.append("subject", "Meddelande ifrån Jobbsprånget");

        return this.httpClient.post(url, emailFormData);
    }

    /*
     *   /casemanagement/application/<id>/approve
     */
    public approveApplication(application_id: Number, noteText: String) {

        let url = this.REST_API_SERVER + "/application/" + application_id + "/approve";

        let payload = {
            "note": noteText,
        };

        return this.httpClient.put(url, payload);
    }

    /*   
     *   /casemanagement/application/<application_id>/approve_apr
     */
    public approveApr(application_id: Number, barNumber: String) {

        let url = this.REST_API_SERVER + "/application/" + application_id + "/approve_apr";

        let payload = {
            "bar_number": barNumber,
        };

        return this.httpClient.put(url, payload);
    }

    /*
     *   /casemanagement/application/<application_id>/confirm_done_ais_apr
     */
    public confirmDoneAis(application_id: Number) {

        let url = this.REST_API_SERVER + "/application/" + application_id + "/confirm_done_ais_apr";

        let payload = {};

        return this.httpClient.put(url, payload);
    }

    public changeDecision(application_id: Number, value: String, textContent?: String) {
        /* Ok values of `value`: [
           'eligible',
           'rejected'
           '--'
           ]
        */

        let url = this.REST_API_SERVER + "/application/" + application_id + "/set_approval";

        let payload = {
            "approval": value,
            "textContent": textContent
        };

        return this.httpClient.put(url, payload);

    }

    public changeCountry(applicant_id: Number, value: String) {
        let url = this.REST_API_SERVER + "/applicant/" + applicant_id + "/set_country";

        let payload = {
            "country": value
        };

        return this.httpClient.put(url, payload);
    }

    public confirmCompaniesAndUnionsAndCreateCampaign(unionContacts: Array<any>, companies: Array<any>) {

        let url = this.REST_API_SERVER + "/uploadcompanyunionsandcreatecampaign";

        let payload = {
            "unionContacts": unionContacts,
            "companies": companies
        };

        return this.httpClient.post(url, payload);
    }

    public setDecisionType(applicationId: Number, value: String) {

        let url = this.REST_API_SERVER + "/application/" + applicationId + "/set_decision_type";

        let payload = {
            "decision_type": value
        };

        return this.httpClient.put(url, payload);
    }

    public setAisCaseNumber(applicationId: Number, value: String) {

        let url = this.REST_API_SERVER + "/application/" + applicationId + "/set_ais_case_number";

        let payload = {
            "ais_case_number": value
        };

        return this.httpClient.put(url, payload);
    }

    public changeResidencePermitType(applicant_id: Number, value: String) {
        let url = this.REST_API_SERVER + "/applicant/" + applicant_id + "/set_residence_permit_type";

        let payload = {
            "type": value
        };

        return this.httpClient.put(url, payload);
    }

    public uploadCampaignCsv(campaignEnd: string,
        fileName: string,
        csvFile: File | undefined) {

        let url = this.REST_API_SERVER + "/upload/employer-csv";

        let formData = new FormData();

        formData.append("campaign_end", campaignEnd);
        formData.append("name", fileName);

        if (csvFile !== undefined) {
            formData.append("csv_file", csvFile);
        }

        return this.httpClient.post(url, formData);

    }

    /*
     *   /casemanagement/employer/<id>/approve
     */
    public approveEmployer(employer_id: Number, noteText: String, barValue: String) {

        let url = this.REST_API_SERVER + "/employer/" + employer_id + "/approve";

        let payload = {
            "note": noteText,
            "barValue": barValue,
        };

        return this.httpClient.put(url, payload);
    }

    /*
     *   /casemanagement/employer/<id>/set_approval
     */
    public changeEmployerDecision(employer_id: Number, value: String, textContent?: String) {

        let url = this.REST_API_SERVER + "/employer/" + employer_id + "/set_approval";

        let payload = {
            "approval": value,
            "note": textContent
        };

        return this.httpClient.put(url, payload);

    }
}
