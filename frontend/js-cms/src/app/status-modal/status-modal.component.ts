import { Component, OnInit, Input, Output } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { faInfo } from '@fortawesome/free-solid-svg-icons';

export interface ModalEvent {
    status: boolean,
    text: String
}

@Component({
  selector: 'status-modal',
  templateUrl: './status-modal.component.html',
  styleUrls: ['./status-modal.component.scss']
})
export class StatusModalComponent implements OnInit {

    private eventsSubscription!: Subscription;
    @Input() events!: Observable<ModalEvent>;

    showing: boolean = false;
    text: String = "";
    status: boolean = false;
    faInfo = faInfo;

    visibleTime: number = 5000;

    constructor() { }
    
    ngOnInit(): void {
        this.eventsSubscription = this.events.subscribe((event: ModalEvent) => this.handleEvent(event));
    }

    handleEvent(event: ModalEvent) {
        this.status = event.status;
        this.text = event.text;
        this.showing = true;

        setTimeout(() => {
            this.showing = false;
        }, this.visibleTime);
        
    }

    ngOnDestroy() {
        this.eventsSubscription.unsubscribe();
    }

    closeModal(event: Event) {
        this.showing = false;
    }
    
}
