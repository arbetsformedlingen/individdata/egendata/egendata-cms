import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AprGridComponent } from './apr-grid.component';

describe('AprGridComponent', () => {
  let component: AprGridComponent;
  let fixture: ComponentFixture<AprGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AprGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AprGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
