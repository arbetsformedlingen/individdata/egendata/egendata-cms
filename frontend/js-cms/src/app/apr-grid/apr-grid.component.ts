import {
    Component, OnInit,
    Input, Output, OnChanges,
    SimpleChanges
} from '@angular/core';

import { DisplayColumn } from '../app.component';
import { ModalEvent } from '../status-modal/status-modal.component';

import { FilterOption } from '../grid-filter-overlay/grid-filter-overlay.component';

import { faCoffee, faShieldAlt, faCheckCircle, faSearch, faPauseCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'apr-grid',
    templateUrl: './apr-grid.component.html',
    styleUrls: ['./apr-grid.component.scss']
})
export class AprGridComponent implements OnInit, OnChanges {

    @Input() aprColumns!: Array<DisplayColumn>;
    @Input() baseApplicants!: Array<any>;
    @Input() applicants!: Array<any>;
    //    @Input() columnsDisplayName!: Array<DisplayColumn>;
    @Input() baseApplications!: Array<any>;
    @Input() applications!: Array<any>;

    @Input() modalPipe!: Subject<ModalEvent>;

    shouldCloseFilters: Subject<number> = new Subject<number>();

    // Font awesome icons
    faCoffee = faCoffee;
    faShieldAlt = faShieldAlt;
    faCheckCircle = faCheckCircle;
    faPauseCircle = faPauseCircle;
    faExclamationCircle = faExclamationCircle;
    faSearch = faSearch;

    selectedApplicant: any = null;
    selectedApplication: any = null;
    popOverPosition: Array<Number> = [];
    popOverWidth: String = "";
    showApplicantPopover: boolean = false;
    calledFromApr: boolean = true;
    appliedDefaultFilters: boolean = false;
    filters: Array<FilterOption> = [];
    searchValue: string = "";

    constructor() { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes["baseApplications"]) {
            this.applications = this.baseApplications.filter((app: any) => app.approval === "Behörig");
        }
    }

    ngOnInit(): void { }

    searchValueChanged(searchValue: any) {
        // Start with searching for names only, filter applicants
        // based on if the applicant["name"] contains searchValue

        if (searchValue === "") {
            // Reset to no filters
            this.applications = this.baseApplications;
        } else {

            this.applications = this.applications.filter((appl: any) => {
                return appl["applicant"]["name"].toLowerCase().includes(searchValue.toLowerCase());
            });
        }
    }

    filterSelect(appliedFilters: Array<FilterOption>) {

        // Store filters in localstorage
        const lsJsData = localStorage.getItem("js-data");

        if (lsJsData !== null) {
            let jsData = JSON.parse(lsJsData);
            jsData.filters = appliedFilters;
            localStorage.setItem("js-data", JSON.stringify(jsData));
        } else {
            let jsData = { filters: appliedFilters };
            localStorage.setItem("js-data", JSON.stringify(jsData));
        }

        //        this.applicants = this.applyFilters(this.baseApplicants, appliedFilters);
        this.applications = this.applyFilters(this.baseApplications, appliedFilters);
    }


    closeApplicantPopup(event: any) {
        this.showApplicantPopover = false;
    }

    applyFilters(applicants: Array<any>, filters: Array<FilterOption>): Array<any> {

        let filtered_applicants: any = [];
        let num_filters_applied = 0;
        let sort_filters = [];

        if (filters === undefined || filters === null) {
            return applicants;
        }

        // Iterate filters and check if they're selected i.e applied
        // Else skip
        for (var i = 0; i < filters.length; i++) {
            let filter: FilterOption = filters[i];
            if (!filter.selected) { continue; }

            // Collect sorting options and apply them after filtering
            if (filter.value.includes("sort")) {
                sort_filters.push(filter);
                continue;
            }


            let temp_filtered_applicants = applicants.filter((applicant: any) => {
                return applicant[filter.key] === filter.value;
            });

            /*            for (var j = 0; j < temp_filtered_applicants.length; j++) {
                            filtered_applicants.push(temp_filtered_applicants[j]);
                        } */
            filtered_applicants = filtered_applicants.concat(temp_filtered_applicants);

            num_filters_applied += 1;

        }

        if (num_filters_applied < 1) {

            if (sort_filters.length > 0) {
                console.log(sort_filters);

                // Only use the first sort
                let sort_filter = sort_filters[0];

                if (sort_filter.value.includes("desc")) {
                    applicants.sort(function (a: any, b: any) {
                        if (a[sort_filter.key] < b[sort_filter.key]) return -1;
                        if (a[sort_filter.key] > b[sort_filter.key]) return 1;
                        return 0;
                    });
                } else {
                    applicants.sort(function (a: any, b: any) {
                        if (a[sort_filter.key] > b[sort_filter.key]) return -1;
                        if (a[sort_filter.key] < b[sort_filter.key]) return 1;
                        return 0;
                    });
                }
            }
            return applicants;
        } else {

            if (sort_filters.length > 0) {
            }
            return filtered_applicants;
        }
    }

    closeFilters(event: any) { }

    expandRow(application: any, event: any) {

        // Get the size of the applicant-grid-div and set the popoverwidth to that
        let applicantGridDiv = document.getElementById("apr-grid-wrapper") as HTMLElement;

        let width = window.getComputedStyle(applicantGridDiv, null).getPropertyValue("width");

        let boundingRect = applicantGridDiv.getBoundingClientRect();

        let widthAsNumber = parseInt(width.replace("px", ""));

        this.popOverPosition = [boundingRect.x, boundingRect.y];

        if (this.selectedApplication && this.selectedApplication.id === application.id) {
            this.selectedApplication = null;
            this.showApplicantPopover = false;
        } else {

            this.selectedApplication = application;
            this.showApplicantPopover = true;

        }

        if (application.expanded) {
            application.expanded = !application.expanded;
        } else {
            application.expanded = true;
        }

    }

}

