import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotePopoverComponent } from './note-popover.component';

describe('NotePopoverComponent', () => {
  let component: NotePopoverComponent;
  let fixture: ComponentFixture<NotePopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotePopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotePopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
