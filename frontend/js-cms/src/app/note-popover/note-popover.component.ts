import {
    Component, OnInit, Input,
    Output,
    EventEmitter,
    HostListener
} from '@angular/core';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';

import { ModalEvent } from '../status-modal/status-modal.component';
import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'note-popover',
    templateUrl: './note-popover.component.html',
    styleUrls: ['./note-popover.component.scss']
})
export class NotePopoverComponent implements OnInit {

    @Input() application: any;
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() applicantChange: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    @Input() position: Array<Number> = [];

    noteText: string = "";
    faPaperclip = faPaperclip;
    file: File | undefined = undefined;

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {
        switch (event.key) {
            case "Escape": {
                this.closeHandle();
                break;
            }
            case "Enter": {
                this.saveNote();
                break;
            }
            default: {
                break;
            }
        }
    }

    constructor(private dataService: DataService) { }

    ngOnInit(): void {
        this.noteText = this.application.note || "";
    }

    fileChange(event: any) {
        this.file = event.target.files[0];
    }

    saveNote() {

        this.dataService.saveApplicantNote(this.application.applicant.id, this.noteText, this.file).subscribe(
            (data: any) => {
                let modalEvent: ModalEvent = {
                    status: true,
                    text: "Sparade anteckning"
                };

                this.modalPipe.next(modalEvent);
                
                let create_date = new Date().toISOString().replace('T', ' ').replace(/\.\d\d\dZ/g, '');
                this.application.activity_log.unshift({
                    comment_text: 'Anteckning: ' + "'" + this.noteText + "'",
                    create_date: create_date,
                    attachment_ids: [],
                    attachment: [],
                });

                this.applicantChange.emit(this.application.applicant);
                this.applicantChange.emit(this.application.activity_log);
                this.applicantChange.emit(this.application);
                this.closeHandle();
            },
            (err) => {
                let modalEvent: ModalEvent = {
                    status: false,
                    text: "Kunde inte spara anteckning"
                };

                this.modalPipe.next(modalEvent);
            });
    }

    closeHandle() {
        this.close.emit(null);
    }

}
