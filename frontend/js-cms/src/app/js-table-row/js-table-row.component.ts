import { Component, OnInit,
         Input } from '@angular/core';

import { DisplayColumn } from '../applicant-grid/applicant-grid.component';

@Component({
  selector: 'tr[js-table-row]',
  templateUrl: './js-table-row.component.html',
  styleUrls: ['./js-table-row.component.scss']
})
export class JsTableRowComponent implements OnInit {

    @Input() displayColumns!: Array<DisplayColumn>;
    @Input() applicant!: any;

  constructor() { }

  ngOnInit(): void {
  }

}
