import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JsTableRowComponent } from './js-table-row.component';

describe('JsTableRowComponent', () => {
  let component: JsTableRowComponent;
  let fixture: ComponentFixture<JsTableRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JsTableRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JsTableRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
