import {
    Component, OnInit,
    Input
} from '@angular/core';

import { DisplayColumn } from '../applicant-grid/applicant-grid.component';
import { FilterOption } from '../grid-filter-overlay/grid-filter-overlay.component';
import { DataService } from '../data.service';
import { faCoffee, faShieldAlt, faCheckCircle, faExclamationCircle, faSearch, faPauseCircle} from '@fortawesome/free-solid-svg-icons';

import { ModalEvent } from '../status-modal/status-modal.component';

import { Subject } from 'rxjs';

@Component({
    selector: 'company-grid',
    templateUrl: './company-grid-component.component.html',
    styleUrls: ['./company-grid-component.component.scss']
})
export class CompanyGridComponent implements OnInit {

    @Input() companyColumns: Array<DisplayColumn> = [];

    @Input() companies: Array<any> = [];
    @Input() modalPipe!: Subject<ModalEvent>;

    faCoffee = faCoffee;
    faShieldAlt = faShieldAlt;
    faCheckCircle = faCheckCircle;
    faExclamationCircle = faExclamationCircle;
    faPauseCircle = faPauseCircle;
    faSearch = faSearch;

    shouldCloseFilters: Subject<number> = new Subject<number>();

    done_mapping: boolean = false;

    showCompanyPopover: boolean = false;
    selectedCompany: any = null;

    constructor(private dataService: DataService) { }

    ngOnChanges() {
        // Map true/false values to ok/not ok
        this.companies = this.companies.map((company: any) => {
            if (company.no_payment_problem) {
                company.no_payment_problem = "OK";
            } else {
                company.no_payment_problem = "Inte OK";
            }

            if (company.no_bankruptcy) {
                company.no_bankruptcy = "OK";
            } else {
                company.no_bankruptcy = "Inte OK";
            }

            if (company.no_tax_debt) {
                company.no_tax_debt = "OK";
            } else {
                company.no_tax_debt = "Inte OK";
            }

            if (!company.bar_number) {
                company.bar_number = "--";
            }

            if (company.union_approved) {
                company.union_approved = "OK";
            } else {
                company.union_approved = "Inte OK";
            }

            if (company.auto_suggestion_approval === "approved") {
                company.auto_suggestion_approval = "OK";
            } else {
                company.auto_suggestion_approval = "Inte OK";
            }

            // Undecided, rejected, approved
            switch (company.approval) {
                case "examine":
                    company.approval = "Utred";
                    break;
                case "approved":
                    company.approval = "Godkänd";
                    break;
                default:
                    company.approval = "--";
                    break;
            }

            return company;
        });
    }


    ngOnInit(): void {
    }

    closeFilters(event: any) { }

    setupData() {
    }

    filterSelect(event: FilterOption[]) {
    }

    expandRow(company: any, event: Event) {
        this.selectedCompany = company;
        this.showCompanyPopover = true;
    }

    closeCompanyPopup(event: Event) {
        this.selectedCompany = null;
        this.showCompanyPopover = false;
    }

}
