import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyGridComponentComponent } from './company-grid-component.component';

describe('CompanyGridComponentComponent', () => {
  let component: CompanyGridComponentComponent;
  let fixture: ComponentFixture<CompanyGridComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyGridComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyGridComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
