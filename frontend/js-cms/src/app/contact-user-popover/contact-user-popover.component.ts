import {
    Component, OnInit, Input,
    Output, EventEmitter,
    HostListener
} from '@angular/core';

import { faPaperclip } from '@fortawesome/free-solid-svg-icons';

import { ModalEvent } from '../status-modal/status-modal.component';
import { Observable, Subject } from 'rxjs';


import { DataService } from '../data.service';

@Component({
    selector: 'contact-user-popover',
    templateUrl: './contact-user-popover.component.html',
    styleUrls: ['./contact-user-popover.component.scss']
})
export class ContactUserPopoverComponent implements OnInit {

    @Input() applicant: any;
    @Input() position: Array<Number> = [];
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() close: EventEmitter<any> = new EventEmitter();

    emailText: string = "";
    file?: File = undefined;
    faPaperclip = faPaperclip;

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {
        switch (event.key) {
            case "Escape": {
                this.closeHandle();
                break;
            }
            case "Enter": {
                this.sendEmail();
                break;
            }
            default: {
                break;
            }
        }
    }

    constructor(private dataService: DataService) {
        this.emailText = "";
    }

    ngOnInit(): void { }

    fileChange(event: any) {
        this.file = event.target.files[0];
    }

    sendEmail() {
        this.dataService.sendApplicantEmail(this.applicant.id, this.emailText, this.file).subscribe(
            (data: any) => {
                let modalEvent: ModalEvent = {
                    status: true,
                    text: 'Meddelandet skickat'
                };

                this.modalPipe.next(modalEvent);
                this.closeHandle();
            },
            (err) => {
                let modalEvent: ModalEvent = {
                    status: false,
                    text: "Kunde inte skicka meddelandet"
                };

                this.modalPipe.next(modalEvent);
            });
    }

    closeHandle() {
        this.close.emit(null);
    }

}
