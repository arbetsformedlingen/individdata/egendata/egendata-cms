import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactUserPopoverComponent } from './contact-user-popover.component';

describe('ContactUserPopoverComponent', () => {
  let component: ContactUserPopoverComponent;
  let fixture: ComponentFixture<ContactUserPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactUserPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactUserPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
