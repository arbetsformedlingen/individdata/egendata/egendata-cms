import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JsHeaderComponent } from './js-header.component';

describe('JsHeaderComponent', () => {
  let component: JsHeaderComponent;
  let fixture: ComponentFixture<JsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JsHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
