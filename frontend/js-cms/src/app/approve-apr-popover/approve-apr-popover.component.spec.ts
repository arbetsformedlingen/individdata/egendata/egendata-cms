import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveAprPopoverComponent } from './approve-apr-popover.component';

describe('ApproveAprPopoverComponent', () => {
  let component: ApproveAprPopoverComponent;
  let fixture: ComponentFixture<ApproveAprPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApproveAprPopoverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveAprPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
