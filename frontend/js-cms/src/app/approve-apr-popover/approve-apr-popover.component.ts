import {
    Component, OnInit, Input,
    Output, EventEmitter,
    HostListener
} from '@angular/core';
import { faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { DataService } from '../data.service';
import { ModalEvent } from '../status-modal/status-modal.component';

import { Observable, Subject } from 'rxjs';

@Component({
    selector: 'approve-apr-popover',
    templateUrl: './approve-apr-popover.component.html',
    styleUrls: ['./approve-apr-popover.component.scss']
})
export class ApproveAprPopoverComponent implements OnInit {

    @Input() application: any;

    @Input() position: Array<Number> = [];
    @Input() modalPipe!: Subject<ModalEvent>;

    @Output() applicationChange: EventEmitter<any> = new EventEmitter();
    @Output() close: EventEmitter<any> = new EventEmitter();

    @HostListener("window:keyup", ["$event"])
    keyEvent(event: KeyboardEvent) {

        switch (event.key) {
            case "Escape": {
                this.closeHandler();
                break;
            }
            case "Enter": {
                this.approve();
                break;
            }

            default: {
                break;
            }
        }
    }

    textValue: String;
    faPaperclip = faPaperclip;

    constructor(private dataService: DataService) {
        this.textValue = "";
    }

    ngOnInit(): void {
    }

    closeHandler() {
        this.close.emit(null);
    }

    approve() {
        this.dataService.approveApr(this.application.id, this.textValue)
            .subscribe(
                (data: any) => {
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Godkände APR"
                    };
                    this.modalPipe.next(modalEvent);

                    this.application.apr_status = "Godkänd";
                    this.applicationChange.emit(this.application);
                    this.closeHandler();
                },
                (err) => {
                    let modalEvent: ModalEvent = {
                        status: true,
                        text: "Kunde inte godkänna APR"
                    };

                    this.modalPipe.next(modalEvent);
                });
    }

}
