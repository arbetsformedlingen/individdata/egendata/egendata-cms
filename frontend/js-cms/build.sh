node --max_old_space_size=102400 node_modules/@angular/cli/bin/ng.js build --output-hashing=none
cat dist/js-cms/*.js > dist/js-cms/packaged.js
mv dist/js-cms/packaged.js ../../extra-addons/case_management/static/src/js/packaged.js
echo "Copied dist/js-cms/packaged.js into ../../extra-addons/case_management/static/src/js/packaged.js"
