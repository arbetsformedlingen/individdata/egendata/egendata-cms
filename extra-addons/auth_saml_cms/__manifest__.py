# -*- coding: utf-8 -*-
{
    'name': "Auth SAML CMS",
    'summary': "Set up SAML for users",
    'description': "",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',
    'depends': ['auth_saml_ol_create_user',
                'auth_saml_ol_groups'],
    'data': [
        "data/res_groups.xml",
        "security/cms_security.xml",
        "data/auth_saml.xml",
    ],
}
