import json
import logging
import os.path
import subprocess
from json import JSONDecodeError

_logger = logging.getLogger(__name__)


class NodeVC:
    def __init__(self, vc_script=None):
        """
        Wrapper for running NodeJS script producing a Verifiable Credential
        :param vc_script: Absolute path to NodeJS script producing a Verifiable Credential
        :type vc_script: str
        """
        error_message = f'vc_script has to be a valid path to NodeJS script producing a Verifiable Credential, ' \
                        f'given path: "{vc_script}"'
        if not vc_script:
            raise ValueError(error_message)

        if not os.path.exists(vc_script):
            raise ValueError(error_message)

        self.vc_script = os.path.abspath(vc_script)
        self.vc_script_directory = os.path.abspath(os.path.dirname(self.vc_script))

    def issue_vc(self, subject, solid_guid, **kwargs):
        """
        Invoke local NodeJS script issuing Verifiable Credential
        :param subject: Personal number used to identify a person receiving Verifiable Credential, like '870820-5441'
        :type subject: str
        :param solid_guid: GUID used to identify resources belonging to a particular transaction
        :type solid_guid: str
        :return: Dict with string representations of Verifiable Credential issued by NodeJS script
        :rtype: dict
        """
        try:
            result = subprocess.check_output(
                ['node', self.vc_script, subject, solid_guid],
                stderr=subprocess.STDOUT,
                cwd=self.vc_script_directory)

            # Fails with "out of memory" when testing
            if hasattr(result, 'stderr'):
                _logger.info(result.stderr)
            _logger.info(result)

            return json.loads(result)
        except JSONDecodeError as e:
            _logger.error(f'Error decoding JSON of VC: "{e}"')
        except Exception as e:
            _logger.error(f'Error when invoking script to issue VC: "{e}"')
            if hasattr(e, 'stdout'):
                _logger.error(e.stdout)

        return dict()
