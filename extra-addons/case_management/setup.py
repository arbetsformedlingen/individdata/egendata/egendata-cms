import logging
import os
import subprocess
import sys

_logger = logging.getLogger(__name__)


def install_dependencies():
    """
    Installs required packages defined in docker/requirements.txt
    """
    try:
        get_packages = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze'])
        installed_packages = [r.decode().split('==')[0] for r in get_packages.split()]

        # List of required packages
        dir_path = os.path.dirname(os.path.realpath(__file__))
        requirements_path = os.path.join(dir_path, 'docker', 'requirements.txt')
        with open(requirements_path, 'r') as f:
            requirements = f.readlines()
            required_packages = [r.strip().split('==')[0] for r in requirements]

        for package in required_packages:
            if package in installed_packages:
                _logger.info('package %s already installed' % package)
            else:
                _logger.info('installing package %s' % package)
                # os.system('%s -m pip install %s' % (sys.executable, package))
                os.system('pip3 install %s' % package)
    except Exception as e:
        _logger.error('Error when running install_dependencies()', e)
        raise e
