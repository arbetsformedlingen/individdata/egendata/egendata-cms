from odoo.tests import tagged, common

from ..util.vc import NodeVC


@tagged('casemanagement')
class TestNodeVC(common.TransactionCase):

    def test_issue_vc(self):
        node_script_path = '/var/oak/solid-client-node-experimentation/issue-vc.js'
        issuer = NodeVC(node_script_path)
        result = issuer.issue_vc('890909-1234', '')
        self.assertTrue(result)
        self.assertIn('vc', result)
        self.assertIn('deflated', result)
        self.assertIn('jsonld', result)
