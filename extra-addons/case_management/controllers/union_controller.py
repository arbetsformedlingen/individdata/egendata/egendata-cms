# -*- coding: utf-8 -*-
import logging

from werkzeug import Response

from odoo import http
from odoo.http import request, content_disposition

from ..util.email import send_emails_to_unions
from ..util.serialize_exception import serialize_exception

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class Union(http.Controller, CaseManagementHelper):
    @http.route("/casemanagement/send-campaign-to-unions",
                methods=["POST"],
                auth="user",
                website=True)
    @serialize_exception
    def send_campaign_to_unions(self, **post):
        """
        Create Approval records, send emails with unique links for every Union
        """
        campaign_id = post.get('campaign_id')
        if not campaign_id:
            return self.upload_csv_file_get('No Campaign found to send to Unions!')

        campaign = http.request.env['casemanagement.campaign'].search([['id', 'in', [campaign_id]]])
        if not campaign:
            return self.upload_csv_file_get('No Campaign found to send to Unions!')

        # Create Approvals
        guid_mapping = campaign.create_union_approvals()
        if not guid_mapping or not campaign.employer_ids:
            return self.upload_csv_file_get('No Unions or Employers found!')

        send_emails_to_unions(guid_mapping, http.request.env)

        return self.upload_csv_file_get(f'Emails sent to {len(guid_mapping)} Unions')

    @http.route('/casemanagement/union/csv/<guid>',
                methods=['GET'],
                auth='public')
    def union_csv_download_employers(self, guid):
        """
        Download CSV of Employer records corresponding to the GUID of a particular Union
        :param guid: Unique ID linking a specific Union to a batch of Employer records to approve
        :type guid: str
        """
        try:
            fetched_approvals = http.request.env['casemanagement.approval'].search([
                ('guid', '=', guid),
                ('status', '=', 'waiting')
            ])
            employers = [approval.employer_id for approval in fetched_approvals]
            binary_data = http.request.env['casemanagement.employer'].employers_to_csv(employers)
            content_type = 'text/csv;charset=utf8'
            union_name = fetched_approvals[0].union_id.name if fetched_approvals else ''

            return request.make_response(
                binary_data,
                headers=[('Content-Type', content_type),
                         ('Content-Disposition', content_disposition(f'employers-{union_name}-{guid}.csv')),
                         ('Content-Length', len(binary_data))],
            )

        except Exception as e:
            _logger.error(f"Error when processing Union CSV download with guid '{guid}': '{e}'")
            return Response(status=400)

    @http.route('/casemanagement/unions/update',
                methods=['GET', 'POST'],
                auth='user')
    @serialize_exception
    def unions_update(self, **post):
        """
        Update Unions' emails
        """
        try:
            union_model = http.request.env['casemanagement.union']
            fetched_unions = union_model.search([])
            modal_message = ''
            if fetched_unions:
                if post:
                    # Update email addresses for Unions
                    updated_unions = union_model.update_email(**post)
                    if updated_unions:
                        modal_message = f'Thank you! {len(updated_unions)} Unions were updated!'
            else:
                modal_message = 'No Unions found to be updated!'

            return http.request.render('case_management.update-unions',
                                       {'unions': fetched_unions,
                                        'modal_message': modal_message
                                        })
        except Exception as e:
            _logger.error(f"Error when processing Union updates: '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/union/sign/<guid>',
                methods=['GET', 'POST'],
                auth='public',
                website=True)
    @serialize_exception
    def union_sign_employer(self, guid, **post):
        """
        Approve a batch of Employer records tied to GUID specific to a particular Union
        :param guid: Unique ID linking a specific Union to a batch of Employer records to approve
        :type guid: str
        """
        try:
            fetched_approvals = http.request.env['casemanagement.approval'].search([
                ('guid', '=', guid),
                ('status', '=', 'waiting')
            ])
            modal_message = ''
            union = 0
            if fetched_approvals:
                union = fetched_approvals[0].union_id
                if post:
                    personal_number = post.get('personal_number')
                    if request.env['casemanagement.validation'].valid_personal_number(personal_number):
                        # Sign/Approve Employers
                        fetched_approvals.sign_approval(personal_number)
                        modal_message = f'Thank you! {len(fetched_approvals)} signed/approved by {personal_number}!'
                    else:
                        modal_message = 'Signature failed: invalid personal number!'
            else:
                modal_message = 'No Employers found to be signed. Someone already signed?'

            return http.request.render('case_management.sign-employers',
                                       {'approvals': fetched_approvals,
                                        'modal_message': modal_message,
                                        'union': union,
                                        'guid': guid})
        except Exception as e:
            _logger.error(f"Error when processing Union Approval with guid '{guid}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")
