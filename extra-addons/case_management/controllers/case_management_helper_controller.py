from odoo.http import request


class CaseManagementHelper:

    @staticmethod
    def pop_from_session(session_key):
        """
        Helper for retrieving a value from session
        :param session_key: Session key to retrieve value for
        """
        return request.session.pop(session_key, None)

    @staticmethod
    def pop_flash_from_session(session_key="cm_flash"):
        """
        Helper for retrieving flash message from session
        :param session_key: Session key to retrieve value for
        """
        return CaseManagementHelper.pop_from_session(session_key)

    @staticmethod
    def set_flash_in_session(message, session_key="cm_flash"):
        """
        Helper for setting flash message in session
        :param message: Message to save in session
        :param session_key: Session key to use to store message at
        """
        request.session[session_key] = message
