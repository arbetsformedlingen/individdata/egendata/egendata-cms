# -*- coding: utf-8 -*-
import logging
import math
import traceback

from werkzeug import Response

from odoo.tools import date_utils

from odoo import http
from odoo.http import request

import json

from ..util.email import send_email_with_attachment, JS_EMAIL
from ..util.serialize_exception import serialize_exception
from ..util.solid import put_response_to_solid

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class Application(http.Controller, CaseManagementHelper):
    @staticmethod
    def _response_from_update(application_id, updated_application):
        """
        Produces a JSON response from updated Application object
        :param application_id: Application ID, int
        :param updated_application: Updated Application object
        :rtype: Response
        """
        # Datetime values can not be serialized out of the box, so odoo provides a serializer
        json_result = json.dumps(updated_application, indent=4, default=date_utils.json_default)
        if not updated_application:
            _logger.error(f"No Application found by ID '{application_id}'")

        return Response(json_result, content_type="application/json")

    @staticmethod
    def get_applications_by_applicant(applicant_id):
        applications = http.request.env["casemanagement.application"].search([
            ("applicant_id", "=", applicant_id)
        ]).read()

        return applications

    @http.route("/casemanagement/application/<int:application_id>/approve",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def approve_application(self, application_id):
        try:
            application = http.request.env['casemanagement.application'].browse([application_id])
            application.applicant_id.write({"approval": "eligible"})
            http.request.env["attachment.helper"].attach_document(application.applicant_id, None, None,
                                                                  "Ändrade behörighet till 'Behörig'")
            json_data = request.jsonrequest

            # Send email to applicant and CC JobbSprånget, save message in log
            send_email_with_attachment(application.applicant_id,
                                       {"subject": "Eligible to apply for internship via JobbSprånget",
                                        "content": json_data["note"]},
                                       http.request.env,
                                       cc=JS_EMAIL)
            return application.read()
        except Exception as e:
            _logger.exception(e)
            return Response(status=500, content_type="application/json")

    @http.route("/casemanagement/application/<int:application_id>/approve_apr",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def approve_apr(self, application_id):
        json_data = request.jsonrequest
        response = self._not_key_and_application_exists("bar_number", application_id, json_data)
        try:
            if not response:
                application = http.request.env['casemanagement.application'].browse([application_id])
                application.write({"apr_status": "approved"})
                http.request.env["attachment.helper"].attach_document(application.applicant_id, None, None,
                                                                      "Godkände APR")
                application.employer_id.write({"bar_number": json_data["bar_number"]})
                http.request.env["attachment.helper"].attach_document(application.employer_id, None, None,
                                                                      "Ändrade BÄR ärendenummer till '%s'" %
                                                                      json_data["bar_number"])
                # TODO: Issuing the APR using AIS Arendehantering is called here
                return application.read()
            else:
                return response
        except Exception as e:
            _logger.exception(e)
            return Response(status=500)

    @http.route("/casemanagement/application/<int:application_id>/confirm_done_ais_apr",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def confirm_done_ais_apr(self, application_id):
        try:
            application = http.request.env['casemanagement.application'].browse([application_id])
            if application:
                application.write({"done_in_ais": True})
                http.request.env["attachment.helper"].attach_document(application.applicant_id, None, None,
                                                                      "Bekräftade klart i AIS")

                return application.read()
            else:
                return Response("", content_type="application/json", status=404)
        except Exception as e:
            _logger.exception(e)
            return Response(status=500)

    @staticmethod
    def _not_key_and_application_exists(key, application_id, json_data):
        """
        Returns Response with error status code if key or application_id doesn't exist otherwise False.
        """
        if key not in json_data:
            return Response("", status=400)

        application = http.request.env["casemanagement.application"].browse(application_id)

        if not application:
            return Response("", content_type="application/json", status=404)

        return False

    @http.route("/casemanagement/application/<int:application_id>/set_approval",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def set_application_approval(self, application_id):
        json_data = request.jsonrequest

        response = self._not_key_and_application_exists("approval", application_id, json_data)
        if not response:
            try:
                application = http.request.env['casemanagement.application'].browse([application_id])
                application.applicant_id.write({"approval": json_data["approval"]})
                http.request.env["attachment.helper"].attach_document(application.applicant_id, None, None,
                                                                      "Ändrade behörighet till '%s'\n----------\n%s" %
                                                                      (json_data["approval"], json_data["textContent"]))
                return application
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route("/casemanagement/application/<int:application_id>/set_decision_type",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def set_applicant_decision_type(self, application_id):
        json_data = request.jsonrequest

        response = self._not_key_and_application_exists("decision_type", application_id, json_data)
        if not response:
            try:
                application = http.request.env['casemanagement.application'].browse([application_id])

                existing_code = http.request.env['casemanagement.code'].search([
                    ['name', '=', str(json_data['decision_type']).upper()]
                ])
                if existing_code:
                    application.applicant_id.write({'classification_code_id': existing_code[0].id})
                    http.request.env["attachment.helper"].attach_document(application.applicant_id, None, None,
                                                                          "Ändrade beslutsklass till '%s'" % str(
                                                                              json_data['decision_type']).upper())

                return application
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route("/casemanagement/application/<int:application_id>/set_ais_case_number",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def set_applicant_ais_case_number(self, application_id):
        json_data = request.jsonrequest

        response = self._not_key_and_application_exists("ais_case_number", application_id, json_data)
        if not response:
            try:
                application = http.request.env['casemanagement.application'].browse([application_id])

                if application:
                    application.write({'ais_case_number': str(json_data['ais_case_number'])})
                    http.request.env["attachment.helper"].attach_document(application.applicant_id, None, None,
                                                                          "Ändrade AIS ärendenummer till '%s'" % str(
                                                                              json_data['ais_case_number']))

                return application
            except Exception as e:
                _logger.exception(e)
                return Response(status=500)
        return response

    @http.route(["/casemanagement/application/",
                 "/casemanagement/application/<int:page>",
                 "/casemanagement/application/<int:page>/<int:per_page>"],
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_applications(self, page=1, per_page=25):

        try:

            total = http.request.env["casemanagement.application"].search_count([])
            applications = http.request.env["casemanagement.application"].search([],
                                                                                 offset=(page - 1) * per_page,
                                                                                 limit=per_page).read()
            for application in applications:
                applicant_id = application["applicant_id"][0]
                applicant = http.request.env["casemanagement.applicant"].search([
                    ("id", "=", applicant_id)]
                )

                application["applicant"] = applicant.read()[0]

                # if applicant.classification_code_id:
                #     application["decision_type"] = applicant.classification_code_id.name
                #     application["decision_type_good"] = applicant.classification_code_good

                application["residence_permit_type"] = applicant.residence_permit_type if applicant.residence_permit_type else None

                # Include comments made on Applicant
                activity_log_entries = http.request.env["casemanagement.comment"].search([
                    ("applicant_id", "=", applicant["id"])]
                ).read()

                # Append comments made on Application itself
                activity_log_entries.extend(
                    http.request.env["casemanagement.comment"].search([
                        ("application_id", "=", application["id"])]
                    ).read()
                )

                # Join in attachments for comments, add creator_name
                for activity_entry in activity_log_entries:
                    activity_entry["attachment"] = http.request.env["casemanagement.attachment"].search(
                        [("comment_id", "=", activity_entry["id"])]).read()
                    creating_user = http.request.env["res.users"].search([("id", "=", activity_entry["write_uid"][0])])
                    activity_entry["creator_name"] = "{} ({})".format(creating_user.login, creating_user.name)

                application["activity_log"] = activity_log_entries

                employer_id = application["employer_id"][0]
                employer = http.request.env["casemanagement.employer"].search([
                    ("id", "=", employer_id)]
                ).read()[0]

                application["employer"] = employer

            pagination = dict(current_page=page, total_items=total, per_page=per_page,
                              total_pages=math.ceil(total / per_page))
            json_result = json.dumps(dict(data=applications, pagination=pagination),
                                     indent=4,
                                     default=date_utils.json_default)

            response = Response(json_result, content_type="application/json")

        except Exception as e:
            traceback.print_exc()
            _logger.error("Error '%s' when fetching applications" % e)
            response = Response("[]", content_type="application/json")

        return response
