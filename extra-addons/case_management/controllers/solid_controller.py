# -*- coding: utf-8 -*-
import base64
import logging
import os

import requests
from werkzeug import Response

from odoo import http
from odoo.http import request

import json
import jwcrypto.jwk

from solidclient.solid_client import SolidSession, SolidAPIWrapper

from ..util.rdf import collect_rdf_graph_data
from ..util.serialize_exception import serialize_exception
from ..util.solid import get_digest, get_key_and_access_token_from_params, \
    write_key_and_access_token_to_params, solid_client_credentials_login, get_authenticated_solid_session

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)

APP_SERVER_URL = os.environ.get("APP_SERVER_URL", "http://localhost:8069/")


class SolidController(http.Controller, CaseManagementHelper):
    @http.route('/casemanagement/solid-notification-webhook',
                methods=['POST'],
                auth='public',
                type='http',
                csrf=False)
    @serialize_exception
    def solid_notification_webhook(self):
        try:
            # TODO: logic for unsubscribing
            # TODO: trigger update of Applicant and Application update
            # TODO: update Subscription model
            headers = http.request.httprequest.headers

            _logger.info('====================== Request stream.read() ======================')
            json_data = http.request.httprequest.stream.read().decode().replace('\'', '\"')
            _logger.info(json_data)

            _logger.info('====================== Request stream.read() JSON ======================')
            webhook_json = json.loads(json_data)
            _logger.info(json.dumps(webhook_json, indent=4))

            if webhook_json and 'object' in webhook_json and 'unsubscribe_endpoint' in webhook_json:
                _logger.info("=================================================================================")
                _logger.info(webhook_json)
                _logger.info("=================================================================================")
                _logger.info("Object: '{}', unsubscribe_endpoint: '{}'".format(webhook_json['object'],
                                                                               webhook_json['unsubscribe_endpoint']))

                # Verify signature from Pod server
                signed_jwt = headers.get('Authorization')
                if not http.request.env['casemanagement.validation'].validate_notification_signature(signed_jwt):
                    message = 'Solid Pod Server notification signature validation failed'
                    _logger.error(message)
                    _logger.error(f'Signed JWT: {signed_jwt}')
                    _logger.error(f'Headers: {headers}')
                    raise ValueError(message)

                # Save Notification
                http.request.env['casemanagement.notification'].create_from_notification_json(
                    webhook_json
                )

                # TODO: do something with notification object
                key, access_token, raw_token, client_id, client_secret, valid_until = \
                    get_key_and_access_token_from_params(http.request.env)
                url = webhook_json['object']['id']

                solid_session = SolidAPIWrapper(client_id=client_id,
                                                client_secret=client_secret,
                                                access_token=raw_token,
                                                keypair=key,
                                                logger=_logger)
                resp = solid_session.get(url)
                _logger.info("=================================================================================")
                _logger.info(resp.text)
                _logger.info("=================================================================================")
                _logger.info("Fetched resource URL '{}', status code: {}".format(url, resp.status_code))
                _logger.info("=================================================================================")

                # Demo example
                update_types = [t.split('#')[1] for t in webhook_json['type']]
                if 'Create' in update_types:
                    _logger.info("============================ DEMO 1 ============================")
                    _logger.info(resp.text)

                    collected_data = dict()
                    collect_rdf_graph_data(collected_data, resp.text)
                    if 'data_request' in collected_data:
                        # Fetch data request, get subject personal number
                        data_request_resp = solid_session.get(collected_data.get('data_request'))
                        collect_rdf_graph_data(collected_data, data_request_resp.text)
                        if 'first_name' not in collected_data:
                            collected_data['first_name'] = 'DEMO'
                            collected_data['last_name'] = collected_data.get('uuid')

                    _logger.info("============================ DEMO 2 ============================")
                    _logger.info(collected_data)

                    # Extract data used when creating Applicant
                    personal_number = collected_data.get('personal_number')
                    solid_guid = collected_data.get('uuid')

                    # Create Applicant (if not exists) based on personal number in Notification, save solid_guid
                    applicant = http.request.env["casemanagement.applicant"].search(
                        [['personal_number', '=', personal_number]]
                    )
                    if applicant:
                        # Update existing applicant
                        applicant[0].update({
                            'personal_number': personal_number,
                            'first_name': collected_data.get('first_name'),
                            'last_name': collected_data.get('last_name'),
                            'solid_guid': solid_guid
                        })
                        _logger.info(f"Updated applicant with data '{collected_data}'")
                    else:
                        # Check if personal details are not secret
                        if not http.request.env["casemanagement.applicant"].has_secret_details(personal_number):
                            _logger.info('Create Applicant - no secret personal details')
                            # Create new applicant
                            created_applicant = http.request.env["casemanagement.applicant"].create({
                                'personal_number': personal_number,
                                'first_name': collected_data.get('first_name'),
                                'last_name': collected_data.get('last_name'),
                                'solid_guid': solid_guid
                            })
                            # Fetch all data from API
                            created_applicant.fetch_all_details()

                            # Demo workflow
                            if collected_data.get('first_name') == 'DEMO':
                                # Update demo applicant with good values
                                created_applicant.update({
                                    'phone': '0701234567',
                                    'email': 'm@gmail.com',
                                    'residence_permit_type': 'put',
                                    'residence_permit_type_good': True,
                                    'origin_country': 'Ukraine',
                                })
                                # Create Application with existing Employer and created applicant above
                                http.request.env["casemanagement.application"].create({
                                    'name': 'DEMO Application',
                                    'start_date': '2022-06-01',
                                    'employer_id': 1,
                                    'applicant_id': created_applicant.id
                                })
                        else:
                            # TODO: save appropriate response back to Solid Pod, secret personal details
                            _logger.info('Failed to create Applicant - secret personal details (SPU)')
                    # TODO: save response back to Solid Pod
                    # put_response_to_solid(personal_number, solid_guid)

            else:
                error = "'object' or 'unsubscribe_endpoint' not found in request JSON"
                _logger.error(f"Error processing notification webhook: {error}")
                return Response(status=400)
        except Exception as e:
            _logger.error(f"Error processing notification webhook: '{type(e)}' - '{e}'", exc_info=e)
            return Response(status=400)

        # Solid server is not smart enough at the moment, but return status 200 OK if went well and 400 if error
        return Response(status=200)

    @http.route("/casemanagement/subscribe-to-solid-resource/",
                auth="user",
                website=True)
    def subscribe_to_solid_resource(self, solid_resource_uri=None, **post):
        """
        Subscribe to a Solid resource for WebHookSubscription2021 notifications
        :param solid_resource_uri: Solid resource URI to receive notifications for
        :type solid_resource_uri: str
        """

        solid_resource_uri = post.get('solid_resource_uri') or solid_resource_uri
        subscription_resource = solid_resource_uri

        try:
            solid_session = get_authenticated_solid_session(http.request.env, request.session)

            # Subscribe to a resource on Solid server
            if request.session.get('solid_subscription_resource_uri'):
                subscription_resource = request.session.pop('solid_subscription_resource_uri')

            if subscription_resource:
                _logger.info("==============================================")
                _logger.info(f"Subscribing to notifications on resource '{subscription_resource}'")

                # Negotiate for WebHook protocol
                gateway_data = {
                    "@context": ["https://www.w3.org/ns/solid/notification/v1"],
                    "type": ["WebHookSubscription2021"],
                    "features": ["state", "webhook-auth"],
                }
                # Usually /gateway
                notification_endpoint = solid_session.solid_metadata.get('notification_endpoint')
                gateway_response = solid_session.post(notification_endpoint,
                                                      json.dumps(gateway_data)).json()
                _logger.info("====================== Gateway response ========================")
                _logger.info(gateway_response)

                # Usually /subscription
                subscription_endpoint = gateway_response.get('endpoint')

                # Request new WebHook subscription
                subscription_data = {
                    "@context": ["https://www.w3.org/ns/solid/notification/v1"],
                    "type": "WebHookSubscription2021",
                    "topic": subscription_resource,
                    "target": f"{APP_SERVER_URL}casemanagement/solid-notification-webhook",
                }

                # Prepare data for Subscription object
                sub_data = dict(digest=get_digest(subscription_data))
                sub_data.update(subscription_data)
                found_subscription = http.request.env['casemanagement.subscription'] \
                    .search([['digest', '=', sub_data.get('digest')]], count=True)
                if not found_subscription:
                    subscription_response = solid_session.post(subscription_endpoint,
                                                               json.dumps(subscription_data))
                    _logger.info("====================== Subscription response ========================")
                    _logger.info(subscription_response.raw_text)

                    if subscription_response.status_code != 200 and "No WebId present" in subscription_response.raw_text:
                        # Key and access_token most likely expired, start auth and redirect back to this endpoint
                        request.session['solid_subscription_resource_uri'] = subscription_resource
                        # Forget expired key and access_token
                        request.session.pop('solid_key')
                        request.session.pop('solid_access_token')

                        # Perform login to Solid Pod server, extract and save key and access token
                        solid_client_credentials_login(http.request.env, request.session)
                        return self.subscribe_to_solid_resource(subscription_resource, **post)

                    data = {
                        'digest': sub_data.get('digest'),
                        'type': sub_data.get('type'),
                        'subscription_target': sub_data.get('topic'),
                        'unsubscribe_endpoint': subscription_response.json().get('unsubscribe_endpoint'),
                        'raw_json': json.dumps(subscription_response.json()),
                    }
                    http.request.env['casemanagement.subscription'].create(data)

                    modal_message = "Subscribed to resource '{}'; response: '{}'".format(subscription_resource,
                                                                                         subscription_response.raw_text)
                else:
                    modal_message = "Tried to subscribe to resource '{}', but Subscription " \
                                    "with identical digest already exists".format(subscription_resource)
                    _logger.info("==============================================")
                    _logger.info(modal_message)
            else:
                modal_message = "No notification subscription resource found"
                _logger.info(modal_message)
        except Exception as e:
            modal_message = f"Error when subscribing to Solid resource '{subscription_resource}': '{e}'"
            _logger.error(modal_message)

        self.set_flash_in_session(modal_message)

        return self.index()

    @http.route("/casemanagement/oauth/callback",
                methods=["GET"],
                auth="user",
                website=True)
    def oauth_callback(self, **kwargs):
        """
        This was used before client credentials authentication flow was available in Solid IdP
        """
        auth_code = kwargs.get('code')
        state = kwargs.get('state')
        assert state in request.session, f"state {state} not in request.session?"

        # Generate a key-pair.
        keypair = jwcrypto.jwk.JWK.generate(kty='EC', crv='P-256')
        solid_session = SolidSession(keypair, state_storage=request.session)

        code_verifier = request.session[state].pop('solid_code_verifier')

        basic_secret = base64.b64encode(
            (request.session['solid_client_id'] + ':' + request.session['solid_client_secret']).encode('utf-8'))

        # Exchange auth code for access token
        resp = requests.post(url=solid_session.provider_info['token_endpoint'],
                             data={
                                 "grant_type": "authorization_code",
                                 "client_id": request.session['solid_client_id'],
                                 "redirect_uri": solid_session.get_callback_url(),
                                 "code": auth_code,
                                 "code_verifier": code_verifier,
                             },
                             headers={
                                 'Authorization': 'Basic ' + basic_secret.decode('utf-8'),
                                 'DPoP': solid_session.make_token(keypair,
                                                                  solid_session.provider_info['token_endpoint'],
                                                                  'POST')
                             },
                             allow_redirects=True)
        result = resp.json()

        _logger.info('++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        _logger.info('Access token response:')
        _logger.info(json.dumps(result, indent=4))

        request.session['solid_key'] = keypair.export()
        request.session['solid_access_token'] = result['access_token']
        request.session['solid_access_token_response'] = result

        write_key_and_access_token_to_params(http.request.env, keypair.export(), result["access_token"], result,
                                             request.session['solid_client_id'],
                                             request.session['solid_client_secret'])

        self.set_flash_in_session("Authentication with OIDC successful! You can now access Solid resources.")

        state = request.session.pop(state)

        return request.redirect(state.pop('solid_redirect_url'))
