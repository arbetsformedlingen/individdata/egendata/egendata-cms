# -*- coding: utf-8 -*-
import logging

from werkzeug import Response

from odoo.tools import date_utils

from odoo import http

import json

from ..util.serialize_exception import serialize_exception
from ..util.email import send_email, extract_attachment_data, JS_EMAIL, send_email_with_attachment

from .case_management_helper_controller import CaseManagementHelper

_logger = logging.getLogger(__name__)


class Employer(http.Controller, CaseManagementHelper):
    @http.route('/casemanagement/employer/<action>/<int:employer_id>',
                methods=['GET', 'POST'],
                auth='user',
                website=True)
    @serialize_exception
    def approve_employer(self, action, employer_id, **post):
        """
        Approve/Examine a given Employer, save action as comment, email Employer and JobbSprånget
        :param action: 'approve' or 'examine'
        :type action: str
        :param employer_id: Employer ID to approve/examine
        :type employer_id: int
        """
        assert action in ['approve', 'examine']
        try:
            fetched_employer = http.request.env['casemanagement.employer'].search([['id', '=', employer_id]])
            modal_message = ''
            if fetched_employer:
                fetched_employer = fetched_employer[0]
                if post:
                    bar_number = post.get('bar_number')
                    if bar_number:
                        # TODO: possible to validate bar_number with some API?
                        # Save action/attachment as comment
                        attachment_data = extract_attachment_data(post)
                        http.request.env["attachment.helper"].attach_document(fetched_employer,
                                                                              attachment_data.get('name'),
                                                                              attachment_data.get('binary'),
                                                                              attachment_data.get('comment_text'))
                        # Approve/Examine Employer
                        approval_state = 'approved' if action == 'approve' else 'examine'
                        fetched_employer.update({'approval': approval_state, 'bar_number': bar_number})

                        modal_message = f'Employer "{fetched_employer.name}" marked "{approval_state}"!'

                        # Email Employer
                        if approval_state == 'approved':
                            email_content = f'Congratulations, {fetched_employer.name}!\n\n' \
                                            f'You have been approved as en employer for internships given via ' \
                                            f'JobbSprånget.\n\n' \
                                            f'This is only for your information and does not require any action.'
                        else:
                            email_content = attachment_data.get('comment_text') or 'no message present'

                        email_subject = 'Employer Approved' if approval_state == 'approved' else 'Employer - Examine'

                        # TODO: email the JobbSprånget team - is it OK to just send a CC here?
                        email_sent = send_email(fetched_employer, email_content, http.request.env, email_subject,
                                                cc=JS_EMAIL)
                        if email_sent:
                            modal_message += ' Emails have been sent to Employer and JobbSprånget.'
                        else:
                            modal_message += ' Emails could not be sent!'
                    else:
                        modal_message = f'Employer "{fetched_employer.name}" cannot be approved without BÄR number!'

            return http.request.render('case_management.approve-employer',
                                       {'employer': fetched_employer,
                                        'modal_message': modal_message,
                                        'employer_id': employer_id,
                                        'action': action})
        except Exception as e:
            _logger.error(f"Error when emailing Employer ID '{employer_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route("/casemanagement/employer/<int:employer_id>",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_employer_by_id(self, employer_id):
        """
        Get Employer by ID
        :param employer_id: Employer ID
        :type employer_id: int
        """
        try:
            fetched_employer = http.request.env['casemanagement.employer'].browse(
                [employer_id]
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_employer, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error("Error '%s' when fetching Employer by ID %s" % (e, employer_id))
            response = Response("[]", content_type="application/json")

        return response

    @http.route("/casemanagement/employer/",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_employers(self):
        """
        Get all Employers
        """
        try:
            fetched_employers = http.request.env['casemanagement.employer'].search(
                []
            ).read()

            for employer in fetched_employers:
                # Include comments made on Employer
                activity_log_entries = http.request.env["casemanagement.comment"].search([
                    ("employer_id", "=", employer["id"])]
                ).read()

                # Join in attachments for comments, add creator_name
                for activity_entry in activity_log_entries:
                    activity_entry["attachment"] = http.request.env["casemanagement.attachment"].search(
                        [("comment_id", "=", activity_entry["id"])]).read()
                    creating_user = http.request.env["res.users"].search([("id", "=", activity_entry["write_uid"][0])])
                    activity_entry["creator_name"] = "{} ({})".format(creating_user.login, creating_user.name)

                employer["activity_log"] = activity_log_entries

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_employers, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error("Error '%s' when fetching all Employers" % e)
            response = Response("[]", content_type="application/json")

        return response

    @http.route("/casemanagement/employer/<int:employer_id>/approve",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def approve_employer(self, employer_id):
        """
        Approve Employer by ID
        :param employer_id: Employer ID
        :type employer_id: int
        """
        try:
            json_data = http.request.jsonrequest

            employer = http.request.env['casemanagement.employer'].browse([employer_id])
            employer.write({"approval": "approved", "bar_number": json_data["barValue"]})
            http.request.env["attachment.helper"].attach_document(employer, None, None,
                                                                  "Godkände företag, BÄR ärendenummer '%s'" %
                                                                  json_data["barValue"])

            # Send email to Employer and CC JobbSprånget
            send_email_with_attachment(employer,
                                       {"subject": "Employer approved for internships via JobbSprånget",
                                        "content": json_data["note"]},
                                       http.request.env,
                                       cc=JS_EMAIL)
            return employer.read()
        except Exception as e:
            _logger.exception(e)
            return Response(status=500, content_type="application/json")

    @http.route("/casemanagement/employer/<int:employer_id>/set_approval",
                methods=["PUT"],
                auth="user",
                type="json",
                csrf=False)
    def set_employer_approval(self, employer_id):
        """
        Set Employer approval by ID
        :param employer_id: Employer ID
        :type employer_id: int
        """
        try:
            json_data = http.request.jsonrequest

            employer = http.request.env['casemanagement.employer'].browse([employer_id])
            if json_data.get("approval"):
                employer.write({"approval": json_data["approval"]})
                http.request.env["attachment.helper"].attach_document(employer, None, None,
                                                                      "Ändrade godkännande till '%s'" %
                                                                      (json_data["approval"]))

            return employer.read()
        except Exception as e:
            _logger.exception(e)
            return Response(status=500, content_type="application/json")

    @http.route("/casemanagement/employer/<int:employer_id>/note",
                methods=["PUT"],
                auth="user",
                type="http",
                csrf=False)
    def save_employer_note(self, employer_id, **post):

        if "note" not in post:
            return Response("", status=400)

        # Get employer by id
        employer = http.request.env["casemanagement.employer"].browse(employer_id)

        if not employer:
            response = Response("", status=404)
            return response

        file_name = None
        file_data = None

        if "attachment" in post:
            attachment = post.get("attachment")
            file_name = attachment.filename
            file_data = attachment.read()

        try:
            http.request.env["attachment.helper"].attach_document(employer,
                                                                  file_name,
                                                                  file_data,
                                                                  "Anteckning: '%s'" % post["note"])
            return Response("", status=200)
        except Exception as e:
            _logger.exception(e)
            return Response(status=500)
