from odoo import models, fields


class ClassificationCode(models.Model):
    _name = 'casemanagement.code'
    _description = 'Classification code from Swedish Migration Board'
    _order = 'id desc'
    _translate = True
    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name)',
         'You can not have two codes with the same name!'),
    ]

    name = fields.Char(help='Classification code', required=True)
    type = fields.Selection(
        selection=[
            ('active', 'Active'),
            ('passive', 'Passive'),
        ],
        default='passive',
        help='Passive - not issued any longer, Active - can still be issued'
    )
    description = fields.Char(help='Description')
    note = fields.Char(help='Note')
    legal_text = fields.Char(help='Legal text')
    eligible = fields.Selection(
        selection=[
            ('true', 'True'),
            ('false', 'False'),
            ('undecided', 'Undecided'),
        ],
        default='undecided',
        help='Eligible or not for JobbSprånget case'
    )

    applicant_ids = fields.One2many('casemanagement.applicant', 'classification_code_id', string='Applicants')
