import logging
import uuid

from datetime import datetime, timedelta

from odoo.exceptions import ValidationError
from ..util.email import send_emails_to_unions
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Campaign(models.Model):
    _name = 'casemanagement.campaign'
    _description = 'Campaign holding Import and valid until date_end'
    _order = 'id desc'
    _translate = True
    _sql_constraints = [
        ('date_end_uniq', 'UNIQUE (date_end)', 'You can not have two Campaigns with the same date_end!'),
    ]

    name = fields.Char(help='Campaign name')
    date_end = fields.Date(help='Campaign valid until')

    import_ids = fields.One2many('casemanagement.import', 'campaign_id', string='Imports')
    employer_ids = fields.One2many('casemanagement.employer', 'campaign_id', string='Employers')

    @api.model
    def create_union_approvals(self, today=None):
        """
        Create UnionApproval records for every employer_ids record in this Campaign
        :param today: Datetime to use as create_date, used in testing
        :type today: datetime
        """
        if len(self):
            # Create UUID per Union record
            trade_unions = self.env['casemanagement.union'].search([])
            guid_mapping = [{'guid': str(uuid.uuid4().hex),
                             'id': u.id,
                             'union': u} for u in trade_unions]
            _logger.info(guid_mapping)
        else:
            guid_mapping = []

        for record in self:
            # Connected Employer records
            for employer in record.employer_ids:
                for u in guid_mapping:
                    created_approval = self.env['casemanagement.approval'].create({
                        'guid': u.get('guid'),
                        'union_id': u.get('id'),
                        'employer_id': employer.id
                    })
                    if today:
                        created_approval.write({'create_date': today})

        return guid_mapping

    @api.constrains('date_end')
    def _check_date_end(self):
        """
        Date end should not be in the past
        :return:
        """
        for record in self:
            if record.date_end < fields.Date.today():
                raise ValidationError("Field 'date_end' cannot be a date in the past")

    @api.model
    def remove_old_data(self, today=None, domain=None):
        """
        Remove old Applicant and Application records after 2 months (62 days) have passes since latest Campaign ended
        :param today: Date object to use as "today" for testing that correct records are deleted
        :type today: datetime | date
        :param domain: Additional filter used on records to be deleted
        :type domain: list
        :rtype: tuple(int, int)
        """
        # Campaign is used to get the timespan indicating what Applicant and Application records will be deleted
        today = today or fields.Date.today()
        two_months_back = today - timedelta(days=62)
        expired_campaign = self.env[self._name].search([['date_end', '<', two_months_back]])
        removed_applicants = 0
        removed_applications = 0

        for campaign in expired_campaign:
            if domain:
                combined_domain = [domain,
                                   ['create_date', '<', campaign.date_end]]
            else:
                combined_domain = [['create_date', '<', campaign.date_end]]

            # Remove Applicants
            old_applicants = self.env['casemanagement.applicant'].search(combined_domain)
            if old_applicants:
                _logger.info(f'Removing {len(old_applicants)} Applicant records created before {campaign.date_end}')
                old_applicants.unlink()
                removed_applicants += len(old_applicants)

            # Remove Applications created before campaign date_end
            old_applications = self.env['casemanagement.application'].search(combined_domain)
            if old_applications:
                _logger.info(f'Removing {len(old_applications)} Application records created before {campaign.date_end}')
                old_applications.unlink()
                removed_applications += len(old_applications)

        if not expired_campaign:
            _logger.info(f'No Campaign with date_end older than "{two_months_back}" was found')

        return removed_applicants, removed_applications

    @api.model
    def update_approvals_for_soon_invalid_employers(self, today=None, expire_before_date=None, ignore_recent=False):
        """
        Creates new UnionApproval records for Employers whose valid status expires sooner than 30 days
        :param today: Date object to use as "today" for testing that correct records are selected
        :type today: datetime
        :param expire_before_date: Datetime object to use to find Employer records whose validity expires before
        :type expire_before_date: datetime
        :param ignore_recent: If True, ignores recently create UnionApproval records and creates new ones
        :type ignore_recent: bool
        :returns: Number of unique emails sent to Union representatives
        """
        # TODO: discuss logic and hard-coded 30 days
        today = today or fields.Datetime.now()
        one_month_forward = expire_before_date or today + timedelta(days=30)
        num_emails = 0

        # Select active Campaign(s)
        campaigns = self.env[self._name].search([['date_end', '>=', today]])

        # For every Campaign, check if validity expires in less than 30 days
        for c in campaigns:
            # For all Employer records in Campaign, create new UnionApproval records,
            # if at least one Employer validity expires within 30 days
            if any([e.union_approval_expiration_date and e.union_approval_expiration_date < one_month_forward
                    for e in c.employer_ids]):
                today_back_days = today - timedelta(days=30)
                # Avoid creating UnionApproval records and sending emails more often than once per 30 days
                approvals_old = [e.union_approval_latest_date and e.union_approval_latest_date < today_back_days
                                 for e in c.employer_ids]
                if ignore_recent or all(approvals_old):
                    guid_mapping = c.create_union_approvals(today=today)
                    # Send emails to all Unions
                    send_emails_to_unions(guid_mapping, self.env)
                    num_emails += len(guid_mapping)

        if not num_emails:
            _logger.info('No UnionApproval records with validity expiring soon found')

        return num_emails
