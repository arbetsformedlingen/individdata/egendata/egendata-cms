from odoo import models, fields


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    solid_username = fields.Char("Solid username", config_parameter="casemanagement.solid_username")
    solid_key = fields.Char("Solid key", config_parameter="casemanagement.solid_key")
    solid_access_token = fields.Char("Solid access token", config_parameter="casemanagement.solid_access_token")
    solid_raw_token = fields.Char("Solid raw token", config_parameter="casemanagement.solid_raw_token")
    solid_client_id = fields.Char("Solid client id", config_parameter="casemanagement.solid_client_id")
    solid_client_secret = fields.Char("Solid client secret", config_parameter="casemanagement.solid_client_secret")
    solid_token_valid_until = fields.Char("Solid token valid until",
                                          config_parameter="casemanagement.solid_token_valid_until")

    ipf_client_id = fields.Char("IPF client id", config_parameter="casemanagement.ipf_client_id")
    ipf_client_secret = fields.Char("IPF client secret", config_parameter="casemanagement.ipf_client_secret")
