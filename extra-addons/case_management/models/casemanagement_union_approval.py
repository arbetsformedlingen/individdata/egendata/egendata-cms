from odoo import models, fields, api
from odoo.exceptions import ValidationError


class UnionApproval(models.Model):
    _name = 'casemanagement.approval'
    _description = 'Trade union approval valid for some time issued for a specific Employer'
    _order = 'id desc'
    _translate = True

    signed_by = fields.Char(help='Personal number of a person signing/approving Employer', readonly=True)
    signed_date = fields.Datetime(help='Signed/approved by Union representative on', readonly=True)
    valid_to_date = fields.Datetime(help='Approval valid to this date', readonly=True)
    guid = fields.Char(help='Unique identifier used in URL for Unions', readonly=True)
    status = fields.Selection(
        selection=[
            ('waiting', 'Waiting'),
            ('approved', 'Approved'),
        ],
        default='waiting',
        help='Waiting/Approved - waiting for Union to approve, approved by Union representative'
    )

    union_id = fields.Many2one('casemanagement.union', 'Union', readonly=True)
    employer_id = fields.Many2one('casemanagement.employer', 'Employer', readonly=True)

    @api.constrains('signed_by')
    def _check_signed_by(self):
        for record in self:
            if not self.env['casemanagement.validation'].valid_personal_number(record.signed_by):
                raise ValidationError("Field 'signed_by' has to be a valid Swedish personal number")

    def sign_approval(self, personal_number):
        signed_date = fields.Datetime.now()
        valid_to_date = signed_date.replace(year=signed_date.year + 1)
        for record in self:
            record.signed_by = personal_number
            record.signed_date = signed_date
            record.valid_to_date = valid_to_date
            record.status = 'approved'
