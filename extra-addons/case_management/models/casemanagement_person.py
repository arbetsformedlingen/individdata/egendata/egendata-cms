from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class CaseManagementPerson(models.Model):
    _name = 'casemanagement.person'
    _description = 'Simple representation of a person'

    first_name = fields.Char(
        help='First name',
        readonly=True,
        required=True)
    last_name = fields.Char(
        help='Last name',
        readonly=True,
        required=True)
    phone_home = fields.Char(string="Home Phone")
    phone_mobile = fields.Char(string="Mobile Phone")
    phone_work = fields.Char(string="Work Phone")

    @api.constrains('phone_home', 'phone_mobile', 'phone_work')
    def _constrain_phone(self):
        for rec in self:
            if rec.phone_home:
                if len(rec.phone_home) > 16:
                    raise ValidationError(_("Home Phone number is too long"))
            if rec.phone_mobile:
                if len(rec.phone_mobile) > 16:
                    raise ValidationError(_("Mobile Phone number is too long"))
            if rec.phone_work:
                if len(rec.phone_work) > 16:
                    raise ValidationError(_("Work Phone number is too long"))
