import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    office_code = fields.Char(string="Office code", help="AF office code", readonly=True)

    @api.constrains('office_code')
    def _constrain_office_code(self):
        for rec in self:
            if rec.office_code:
                if not self.env['casemanagement.validation'].valid_office_code(rec.office_code):
                    raise ValidationError(_("Invalid Office code"))

    @api.model
    def _deactivate_default_admin_user(self):
        """
        Deactivates default super admin 'admin' user if any other admin user able to create new users is found
        """
        admin_group_id = self.env.ref('base.group_system').id
        contact_creation_group_id = self.env.ref('base.group_partner_manager').id
        admin_user = self.env.ref('base.user_admin', raise_if_not_found=False)

        if admin_user:
            query = """
SELECT res_users.id
FROM res_users
WHERE res_users.active IS TRUE AND res_users.id != %s AND EXISTS (
    SELECT 1 FROM res_groups_users_rel WHERE res_groups_users_rel.gid = %s AND res_groups_users_rel.uid = res_users.id
) AND EXISTS (
    SELECT 1 FROM res_groups_users_rel WHERE res_groups_users_rel.gid = %s AND res_groups_users_rel.uid = res_users.id
)
GROUP BY id"""

            self.env.cr.execute(query, (admin_user.id, admin_group_id, contact_creation_group_id,))
            uids = [res[0] for res in self.env.cr.fetchall()]

            _logger.debug("{} users found in both base.group_system and base.group_partner_manager".format(len(uids)))

            if uids:
                _logger.debug("Deactivate user ID '{}'".format(admin_user.id))
                # Deactivate default Super Admin
                self.browse([admin_user.id]).write({'active': False})
                _logger.debug("User with ID '{}' deactivated".format(admin_user.id))
            else:
                _logger.debug("Super admin user 'admin' cannot be deactivated")
        else:
            _logger.debug("Super admin user 'admin' not found")
