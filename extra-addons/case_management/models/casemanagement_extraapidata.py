import json
import logging
import os
from datetime import datetime

import jwcrypto
import requests as requests
from solidclient.utils.utils import make_random_string

from odoo import models, fields

SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")

_logger = logging.getLogger(__name__)


class ExtraApiData(models.Model):
    _name = 'casemanagement.extraapidata'
    _description = 'Extra data for case management'

    testing = fields.Char()

    def make_token_for(self, keypair, uri, method):
        print(keypair)
        jwt = jwcrypto.jwt.JWT(header={
            "typ":
                "dpop+jwt",
            "alg":
                "ES256",
            "jwk":
                keypair.export(private_key=False, as_dict=True)
        },
            claims={
                "jti": make_random_string(),
                "htm": method,
                "htu": uri,
                "iat": int(datetime.now().timestamp())
            })
        jwt.make_signed_token(keypair)
        return jwt.serialize()

    def make_headers(self, url, method):
        with open('sensitivedata.dat', 'r') as stored_keys:
            json_loaded = json.loads(stored_keys.read())
            keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
            access_token = json_loaded["access_token"]
            headers = {
                "Authorization": ("DPoP " + access_token),
                "DPoP": self.make_token_for(keypair, url, method)
            }
            return headers

    def construct_access_headers(self, url, method):
        # We need a 'key' and an 'access_token'
        # and construct headers like this:

        with open('sensitivedata.dat', 'r') as stored_keys:
            json_loaded = json.loads(stored_keys.read())
            keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
            access_token = json_loaded["access_token"]
            headers = {
                "Authorization": ("DPoP " + access_token),
                "DPoP": self.make_token_for(keypair, url, method)
            }

        decoded_access_token = jwcrypto.jwt.JWT(jwt=access_token)

        web_id = json.loads(
            decoded_access_token.token.objects["payload"]
        )["sub"]

        return headers

    def send_to_log(self, data):
        url = "http://localhost:5555/log"

        req = requests.post(url, json=data)

        return req.status_code == 200

    def fetch_some_data(self, cr, uid, context=None):
        url = f"{SOLID_SERVER_URL}/source-test-se/profile/"
        _logger.info("fetch_some_data() URL: '{}'".format(url))
        self.send_to_log({"status": "RUNNING"})

        headers = self.construct_access_headers(url, 'GET')
        _logger.info("fetch_some_data() Headers: '{}'".format(url))

        resp = requests.get(url, headers)

        _logger.info("fetch_some_data() Response status code: {}".format(resp.status_code))
        _logger.info("fetch_some_data() Response body: {}".format(resp.text))
        _logger.info("fetch_some_data() Fetching some data finished")
