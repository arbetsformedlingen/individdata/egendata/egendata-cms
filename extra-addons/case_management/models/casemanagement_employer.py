import io
import logging
from datetime import datetime

from odoo.exceptions import ValidationError
from odoo import models, fields, api, _
from odoo.tools import pycompat

_logger = logging.getLogger(__name__)


class Employer(models.Model):
    _name = 'casemanagement.employer'
    _description = 'An employer offering internship'
    _inherit = ['mail.thread']
    _order = "id desc"
    _sql_constraints = [
        # ('name_uniq', 'UNIQUE (name)',
        #  'You can not have two employers with the same name!'),
        ('org_number_uniq', 'UNIQUE (org_number)',
         'You can not have two employers with the same organization number!'),
    ]
    _translate = True

    name = fields.Char(help='Employer name', required=True, readonly=True)
    org_number = fields.Char(help='Organization number', required=True, readonly=True)
    bar_number = fields.Char(help='Case number from BÄR', readonly=True)
    email = fields.Char(help='Email address to contact employer', required=True, readonly=True)

    no_payment_problem = fields.Boolean(help='"checked" - no payment problems, "unchecked" - has payment problems',
                                        readonly=True)
    no_tax_debt = fields.Boolean(help='"checked" - no debt, "unchecked" - has debt', readonly=True)
    no_bankruptcy = fields.Boolean(help='"checked" - not bankrupt, "unchecked" - bankrupt', readonly=True)

    union_approved = fields.Selection(
        selection=[
            ('ok', 'OK'),
            ('waiting', 'Waiting'),
            ('--', '--'),
        ],
        default='--',
        compute="_compute_union_approved",
        help='"OK" - approved by union, "Waiting" - request of signature sent to unions, '
             '"--" - nothing sent to unions, or something is wrong',
        readonly=True)

    union_approval_expiration_date = fields.Datetime(
        compute="_compute_union_approval_expiration_date",
        help='Datetime after which Employer approval by Union is no longer valid',
        readonly=True)

    union_approval_latest_date = fields.Datetime(
        compute="_compute_union_approval_latest_date",
        help='Datetime when latest Approval record was created',
        readonly=True)

    auto_suggestion_approval = fields.Selection(
        compute="_suggest_approval",
        selection=[
            ('approved', 'Approved'),
            ('examine', 'Examine'),
            ('unprocessed', 'Unprocessed'),
        ],
        default='unprocessed',
        help='Approved/Examine/Unprocessed - automatically generated value based on Employer property values',
        readonly=True)

    approval = fields.Selection(
        selection=[
            ('approved', 'Approved'),
            ('examine', 'Examine'),
            ('unprocessed', 'Unprocessed'),
        ],
        default='unprocessed',
        help='Approved/Examine/Unprocessed - depending on whether employer was handled by case worker')

    customer_number = fields.Integer("Customer number", help="Customer number used in APR API (Kundnummer)")

    application_ids = fields.One2many('casemanagement.application', 'employer_id', string='Applications')

    comment_ids = fields.One2many('casemanagement.comment', 'employer_id', string='Comments')

    # Has potentially many Approvals by Unions
    approval_ids = fields.One2many('casemanagement.approval', 'employer_id', string='Approvals')

    # Belongs to Campaign
    campaign_id = fields.Many2one('casemanagement.campaign', 'Campaign', readonly=True)

    address_id = fields.Many2one(string="Address", comodel_name="casemanagement.address")

    contact_person = fields.Many2one(string="Contact person", comodel_name="casemanagement.person")

    @api.depends('approval_ids.status', 'approval_ids.valid_to_date')
    def _compute_union_approved(self):
        """
        Computes automatic union approval suggestion based on property values supplied in @api.depends decorator
        """
        today = datetime.now()
        for record in self:
            if record.approval_ids:
                # Approval from any Union makes Employer approved for 1 year
                if any([r.status == 'approved' and today < r.valid_to_date for r in record.approval_ids]):
                    record.union_approved = 'ok'
                elif any([[r.status == 'waiting' for r in record.approval_ids]]):
                    record.union_approved = 'waiting'
            else:
                record.union_approved = '--'

    @api.depends('approval_ids.status', 'approval_ids.valid_to_date')
    def _compute_union_approval_expiration_date(self):
        """
        Computes datetime when the latest Approval record was created
        """
        today = datetime.now()
        for record in self:
            if record.approval_ids:
                if any([r.status == 'approved' and today < r.valid_to_date for r in record.approval_ids]):
                    valid_to_dates = sorted([r.valid_to_date for r in record.approval_ids if r.valid_to_date])
                    record.union_approval_expiration_date = valid_to_dates[-1]
                else:
                    record.union_approval_expiration_date = None
            else:
                record.union_approval_expiration_date = None

    @api.depends('approval_ids.create_date')
    def _compute_union_approval_latest_date(self):
        """
        Computes date after which Union approval of Employer is no longer valid
        """
        for record in self:
            if record.approval_ids:
                create_dates = sorted([r.create_date for r in record.approval_ids])
                record.union_approval_latest_date = create_dates[-1]
            else:
                record.union_approval_latest_date = None

    @api.depends('name', 'org_number', 'email', 'no_payment_problem', 'no_tax_debt', 'no_bankruptcy', 'union_approved',
                 'bar_number')
    def _suggest_approval(self):
        """
        Computes automatic approval suggestion based on property values supplied in @api.depends decorator
        """
        for record in self:
            satisfied = all(
                [record.name, record.org_number, record.email, record.no_payment_problem, record.no_tax_debt,
                 record.no_bankruptcy, record.bar_number, record.union_approved == 'ok'])
            record.auto_suggestion_approval = 'approved' if satisfied else 'examine'

    @api.constrains('email')
    def _check_email(self):
        for record in self:
            if not self.env['casemanagement.validation'].valid_email(record.email):
                raise ValidationError("Field 'email' has to be a valid email address")

    @api.constrains('org_number')
    def _check_org_number(self):
        for record in self:
            if not self.env['casemanagement.validation'].valid_org_number(record.org_number):
                raise ValidationError("Field 'org_number' has to be a valid Swedish Organization Number")

    @api.model
    def employers_to_csv(self, employers):
        """
        Returns binary contents of CSV file with rows corresponding to Employer records
        :param employers: Employer records
        :type employers: list[Employer]
        :rtype: bytearray
        """
        rows = [[e.name, e.org_number, e.email] for e in employers]

        fp = io.BytesIO()
        writer = pycompat.csv_writer(fp, quoting=1)

        # Header - only these columns are present and allowed to be updated with CSV import
        writer.writerow(['name', 'org_number', 'email'])

        for data in rows:
            row = []
            for d in data:
                # Spreadsheet apps tend to detect formulas on leading =, + and -
                if isinstance(d, str) and d.startswith(('=', '-', '+')):
                    d = "'" + d

                row.append(pycompat.to_text(d))
            writer.writerow(row)

        return fp.getvalue()

    def write(self, values):
        to_return = list()
        for rec in self:
            old_values = {}
            for key in values:
                old_values[key] = getattr(rec, key)
            res = super(Employer, rec).write(values)
            to_return.append(res)
            body = []
            for key in values:
                field_name = self.env['ir.translation'].get_field_string(rec._name)[key]
                body.append(_("'%(field_name)s' field was changed from '%(old_value)s' to '%(new_value)s'",
                              field_name=field_name,
                              old_value=old_values[key],
                              new_value=values[key]))
            body = "\n".join(body)
            rec.message_post(body=_(body))
            _logger.info(f"{body} for {rec._name} {rec.id}")
        return to_return
