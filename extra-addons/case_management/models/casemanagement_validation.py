import re
import datetime
import logging
import os

import json
import jwcrypto.jwk
import jwcrypto.jwt
import requests
from personnummer import personnummer

from odoo import models

_logger = logging.getLogger(__name__)

EMAIL_REGEX = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3002")
POD_PROVIDER_BASE_URL = os.environ.get("POD_PROVIDER_BASE_URL", "http://localhost:3001")


class CasemanagementValidation(models.Model):
    _name = "casemanagement.validation"
    _description = 'Used to validate different input fields'
    _auto = False

    def valid_office_code(self, code):
        if len(code) > 4:
            return False
        return self.valid_number(code)

    def valid_posting_code(self, code):
        if code is False:
            return True
        if len(code) > 10 or not code.strip():
            return False
        return self.valid_number(code)

    def valid_zip(self, zip_code):
        if len(zip_code) > 5:
            return False
        return self.valid_number(zip_code)

    @staticmethod
    def valid_org_number(org_number):
        """
        Checks if org_number does not contain invalid characters, only digits and "-" allowed
        :param org_number: org number string
        :type org_number: str
        :rtype: bool
        """
        return re.fullmatch(r'[\d-]+', str(org_number))

    @staticmethod
    def valid_personal_number(personal_number):
        """
        Checks if personal_number does not contain invalid characters, only digits and "-+" allowed
        :param personal_number: personal number string
        :type personal_number: str
        :rtype: bool
        """
        return personnummer.valid(personal_number)

    @staticmethod
    def valid_number(number):
        """
        Checks if number does not contain invalid characters, only digits allowed
        :param number: number string
        :type number: str
        :rtype: bool
        """
        return re.fullmatch(r'\d+', str(number))

    @staticmethod
    def valid_email(email_address):
        """
        Checks if email_address is a syntactically valid email address
        :param email_address: Email address string
        :type email_address: str
        :rtype: bool
        """
        return re.fullmatch(EMAIL_REGEX, str(email_address))

    @staticmethod
    def validate_campaign_date_end(campaign_end):
        """
        Validates campaign end date - must parse and be in the future
        :param campaign_end: Date string in format YYYY-MM-DD
        :type campaign_end: str
        :rtype: datetime.datetime | bool
        """
        parsing_format = '%Y-%m-%d'
        try:
            parsed_datetime = datetime.datetime.strptime(campaign_end, parsing_format)
        except Exception as e:
            _logger.error(f'Cannot parse string "{campaign_end}" as datetime using format "{parsing_format}" '
                          f'with error: "{e}"')
            return False

        if datetime.datetime.now() > parsed_datetime:
            return False

        return parsed_datetime

    @staticmethod
    def validate_notification_signature(signed_token):
        """
        Validate signed_token delivered along with resource notification from Solid Pod server
        :param signed_token: Serialized JWT, signed by Solid Pod server public key
        :type signed_token: str
        :rtype: bool
        """
        pod_provider_url = POD_PROVIDER_BASE_URL.rstrip('/')

        try:
            pod_server_metadata = requests.get(pod_provider_url + "/.well-known/solid").json()

            _logger.info("================= pod_server_metadata =================")
            _logger.info(pod_server_metadata)

            jwks_endpoint = pod_server_metadata.get('jwks_endpoint')
            pod_server_keys = requests.get(jwks_endpoint).text

            _logger.info("================= pod_server_keys =================")
            _logger.info(pod_server_keys)

            # Use the whole JWKS when verifying signed JWT
            pod_server_key_set = jwcrypto.jwk.JWKSet.from_json(pod_server_keys)

            _logger.info("================= pod_server_key_set =================")
            _logger.info(pod_server_key_set)

            # Claims can be checked directly when deserializing JWT
            deserialized_token = jwcrypto.jwt.JWT(check_claims=dict(iss=pod_provider_url))
            deserialized_token.deserialize(signed_token, key=pod_server_key_set)

            _logger.info("================= authorization_jwt =================")
            _logger.info(signed_token)
            _logger.info("================= deserialized_token claims =================")
            _logger.info(deserialized_token.claims)

            # Value of "iss" has to match SOLID_SERVER_URL according to Solid Notifications spec:
            # https://github.com/solid/notifications/blob/main/webhook-subscription.md#10-webhook-request-with-token-signed-by-the-pod-key
            claim_to_check = json.loads(deserialized_token.claims).get('iss')

        except Exception as e:
            claim_to_check = None
            _logger.error(f"Could not validate signed token '{signed_token}', error: '{e}'", exc_info=e)

        return claim_to_check and claim_to_check == pod_provider_url
