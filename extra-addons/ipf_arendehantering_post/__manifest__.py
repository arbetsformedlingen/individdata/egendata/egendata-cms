# -*- coding: utf-8 -*-
{
    'name': "IPF Arendehantering POST",
    'summary': "POST ärendehantering data to TRASK from CMS",
    'description': "",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',
    'depends': [
        'af_ipf',
        'case_management_apr_case'
        ],
    'data': [
        'security/ir.model.access.csv'
    ],
}
