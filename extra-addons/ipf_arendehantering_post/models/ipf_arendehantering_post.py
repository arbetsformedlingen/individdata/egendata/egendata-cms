import logging

from odoo import models, api

_logger = logging.getLogger(__name__)


class IpfArendehanteringPost(models.Model):
    _name = 'ipf.case_post'
    _description = 'Model without table used to POST APR to AIS Arendehantering API'
    _auto = False

    @api.model
    def post_arendehantering(self, apr_case, **kw):
        """
        POSTs APR to AIS Arendehantering API:
        https://eu1.anypoint.mulesoft.com/exchange/1dae6e32-bf25-4b52-aa03-8f32e1f5fc08/ais-arendehantering/minor/2.0/console/method/%23414/
        """

        arendehantering = self.env.ref("af_ipf.ipf_endpoint_arendehantering").sudo()

        arende_dict = {
            "Deltidsorsak": apr_case.part_time_reason,

            "Praktikplats": {

                "Besoksadress": {
                    "Gatuadress": apr_case.internship.address_id.street,
                    "Ort": apr_case.internship.address_id.city,
                    "Postnummer": apr_case.internship.address_id.address_zip
                },

                "Plats": apr_case.internship.place,

                "Kontaktperson": {
                    "Efternamn": apr_case.internship.contact_person.first_name,
                    "Fornamn": apr_case.internship.contact_person.last_name,
                    "Telefonnummer": apr_case.internship.contact_person.phone_mobile
                },

                "Arbetstidsbeskrivning": apr_case.internship.working_hours_description,
                "Syfte": apr_case.internship.purpose,
                "Uppgifter": apr_case.internship.information,
                "Yrkesgruppskod": apr_case.internship.work_group_code.code
            },

            "Praktiktyp": apr_case.type_of_internship,
            # "RamprogramArendenummer": apr_case.case_number,

            "Anordnare": {

                "AdministrativaKontaktuppgifter": {

                    "Adress": {
                        "Gatuadress": apr_case.application.employer_id.address_id.street,
                        "Ort": apr_case.application.employer_id.address_id.city,
                        "Postnummer": apr_case.application.employer_id.address_id.address_zip
                    },

                    "Kontaktperson": {
                        "Efternamn": apr_case.application.employer_id.contact_person.first_name,
                        "Fornamn": apr_case.application.employer_id.contact_person.last_name,
                        "Telefonnummer": apr_case.application.employer_id.contact_person.phone_mobile
                    }

                },
                "FinnsArbetstagarorganisation": apr_case.union_exists,
                "FinnsYttrandeFranArbetstagarorganisation": apr_case.union_approval_exists,
                "Kundnummer": apr_case.application.employer_id.customer_number
            },

            "Sokande": {
                "FullfoljdGymnasieutbildning": apr_case.application.applicant_id.completed_secondary_education,
                "Personnummer": apr_case.application.applicant_id.personal_number,
                "SamordningAvErsattning": apr_case.application.applicant_id.samordning_av_ersattning
            },

            "AnsvarigHandlaggareSignatur": apr_case.responsible_officer.login,
            "AnsvarigtKontorKod": apr_case.responsible_officer.office_code,
            "BeslutandeHandlaggareSignatur": apr_case.deciding_officer.login,
            "BeslutandeKontorKod": apr_case.deciding_officer.office_code,
            "Beslutsdatum": apr_case.decision_date,

            "Beslutsperiod": {
                "Startdatum": apr_case.decision_period_start_date,
                "Slutdatum": apr_case.decision_period_end_date
            },

            "Omfattning": int(apr_case.work_hours_percentage),
            "Konteringar": []
        }

        if apr_case.planned_break_start_date and apr_case.planned_break_end_date:
            arende_dict["PlaneratUppehall"] = {
                "Startdatum": apr_case.planned_break_start_date,
                "Slutdatum": apr_case.planned_break_end_date
            }

        if apr_case.previous_case_number:
            arende_dict["ForegaendeArendenummer"] = apr_case.previous_case_number

        for posting in apr_case.postings:
            p = {
                "Kontokod": posting.account_code,
                "Verksamhetskod": posting.business_code,
                "Finansieringskod": posting.financing_code,
                "Kostnadsstallekod": posting.cost_center_code,
            }
            if posting.project_code:
                p["Projectkod"] = posting.project_code
            if posting.free_classification_code:
                p["FriKlassificeringskod"] = posting.free_classification_code

            arende_dict["Konteringar"].append(p)

        # API will complain about any included key 
        # with a value that takes a field that isn't set
        # when it expects string, integer etc. since they're sent as the boolean false
        arende_dict = self._clean_false_values_from_dict(
            arende_dict,
            (
                "FinnsArbetstagarorganisation",
                "FinnsYttrandeFranArbetstagarorganisation"
            )
        )

        # TODO: additional error handling when this POST fails
        return arendehantering.call(body=arende_dict, **kw)

    @staticmethod
    def _clean_false_values_from_dict(d, keys_to_ignore):
        """
        Removes any key from the provided dict that has the value of False
        Use keys_to_ignore to skip any keys you want to keep even when their value is False
        """
        keys_to_remove = []
        for key in d:
            if not d[key] and key not in keys_to_ignore:
                keys_to_remove.append(key)

        for key in keys_to_remove:
            d.pop(key)
        return d
