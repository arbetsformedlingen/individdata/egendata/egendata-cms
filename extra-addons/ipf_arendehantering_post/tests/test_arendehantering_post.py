import logging
import os

from datetime import datetime

from odoo.tests import common, tagged

_logger = logging.getLogger(__name__)

API_AIS_CLIENT_ID = os.environ.get("API_AIS_CLIENT_ID", "client_id")
API_AIS_CLIENT_SECRET = os.environ.get("API_AIS_CLIENT_SECRET", "client_secret")


@tagged('ipf')
class TestsCaseManagementModels(common.TransactionCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        super(TestsCaseManagementModels, self).setUp()

    def test_arendehantering_post(self):
        # Employers
        created_employers = self.env['casemanagement.employer'].create([
            {
                'name': 'TEST employer 1',
                'org_number': '898989-1234',
                'email': 'test@testemployer1.org',
                'address_id': self.env['casemanagement.address'].create(
                    {
                        'street': 'test',
                        'address_zip': '12345',
                        'city': 'testtown'
                    }).id,
                'contact_person': self.env['casemanagement.person'].create(
                    {
                        'first_name': 'testperson',
                        'last_name': 'abcde',
                        'phone_mobile': '0701234567'
                    }).id,
                'customer_number': '10145352'
            },
            {
                'name': 'TEST employer 2',
                'org_number': '898989-1235',
                'email': 'test@testemployer2.org',
                'address_id': self.env['casemanagement.address'].create(
                    {
                        'street': 'test2',
                        'address_zip': '23456',
                        'city': 'testtown'
                    }).id,
                'contact_person': self.env['casemanagement.person'].create(
                    {
                        'first_name': 'testperson2',
                        'last_name': 'abcde',
                        'phone_mobile': '0701234567'
                    }).id
            },
            {
                'name': 'TEST employer 3',
                'org_number': '898989-1236',
                'email': 'test@testemployer3.org',
                'address_id': self.env['casemanagement.address'].create(
                    {
                        'street': 'test3',
                        'address_zip': '34567',
                        'city': 'testtown'
                    }).id,
                'contact_person': self.env['casemanagement.person'].create(
                    {
                        'first_name': 'testperson3',
                        'last_name': 'abcde',
                        'phone_mobile': '0701234567'
                    }).id
            },
        ])
        # Campaign
        created_campaign = self.env['casemanagement.campaign'].create(
            {
                'date_end': datetime.now().date(),
                'employer_ids': [(6, 0, [e.id for e in created_employers])]
            })

        # Create Unions if no existing
        existing_unions = self.env['casemanagement.union'].search([])
        if not existing_unions:
            existing_unions = self.env['casemanagement.union'].create([
                {
                    'name': 'Akavia',
                    'email': 'noreply@jobtech-akavia.se'
                },
                {
                    'name': 'Jusek',
                    'email': 'noreply@jobtech-jusek.se'
                },
                {
                    'name': 'Sveriges Ingenjörer',
                    'email': 'noreply@jobtech-sako.se'
                },
                {
                    'name': 'Sveriges Arkitekter',
                    'email': 'noreply@jobtech-sveark.se'
                },
                {
                    'name': 'Naturvetarna',
                    'email': 'noreply@jobtech-naturvetarna.se'
                }
            ])

        # Applicants
        created_applicants = self.env['casemanagement.applicant'].create([
            {
                'first_name': 'TEST',
                'last_name': "testson",
                'personal_number': '200101010122',
                'email': 'test@testson.org',
                'completed_secondary_education': True,
                'samordning_av_ersattning': True,
                'address_id': self.env['casemanagement.address'].create(
                    {
                        'street': 'applicantstreet',
                        'address_zip': '12345',
                        'city': 'testtown'
                    }).id
            },
        ])

        # Application
        created_application = self.env['casemanagement.application'].create(
            {
                'name': 'TEST internship 1',
                'employer_id': created_employers[0].id,
                'applicant_id': created_applicants[0].id
            })

        # Handläggare
        user = self.env["res.users"].create(
            {
                "login": "xxaff",
                "name": "xxaff",
                "office_code": "1208"
            }
        )

        existing_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])
        self.assertFalse(existing_approvals)

        # Create Approval records for all Campaign.employer_ids and all existing Unions
        guid_mapping = created_campaign.create_union_approvals()
        self.assertIsInstance(guid_mapping, list)
        self.assertTrue(len(guid_mapping) == len(existing_unions))

        created_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])

        _logger.info('=============================== CREATED APPROVALS ==============================')
        _logger.info(created_approvals)
        _logger.info('================================================================================')

        self.assertTrue(len(created_approvals) == len(created_employers) * len(existing_unions))

        # Sign Approvals, now Employers should be approved
        created_approvals.sign_approval('199004012382')

        arendehantering_ipf = self.env.ref("af_ipf.ipf_endpoint_arendehantering")

        arendehantering_ipf.ipf_id.write({
            "client_id": API_AIS_CLIENT_ID,
            "client_secret": API_AIS_CLIENT_SECRET
        })

        full_test_case = self.env['casemanagement.apr_case'].create({
            "internship": self.env["casemanagement.internship"].create(
                {
                    "address_id": self.env['casemanagement.address'].create(
                        {
                            'street': 'internshipstreet',
                            'address_zip': '12345',
                            'city': 'testtown'
                        }).id,
                    "place": "testplace",
                    "contact_person": self.env["casemanagement.person"].create(
                        {
                            'first_name': 'testperson4',
                            'last_name': 'abcde',
                            'phone_mobile': '0701234567'
                        }).id,
                    "working_hours_description": "08-16",
                    "purpose": "test",
                    "information": "test",
                    "work_group_code": self.env["res.ssyk"].search([("code", "=", "2512")])[0].id, # Not required
                }).id,
            "type_of_internship": "Praktikantprogrammet",  # Not required
            "case_number": "12345",  # Not required
            "union_exists": True,
            "planned_break_start_date": "2022-10-10",  # Not required
            "planned_break_end_date": "2022-10-17",  # Not required
            "application": created_application.id,
            "responsible_officer": user.id,
            "deciding_officer": user.id,
            "decision_date": "2022-09-20",
            "decision_period_start_date": "2022-10-01",
            "decision_period_end_date": "2022-11-01",
            "postings": [
                (0, 0, {
                    "account_code": "7931",
                    "business_code": "151001",
                    "financing_code": "70210",
                    "cost_center_code": "30002"
                })],  # Required if no RamprogramArendenummer is given
        })

        basic_test_case = self.env['casemanagement.apr_case'].create({
            "internship": self.env["casemanagement.internship"].create(
                {
                    "address_id": self.env['casemanagement.address'].create(
                        {
                            'street': 'internshipstreet',
                            'address_zip': '12345',
                            'city': 'testtown'
                        }).id,
                    "place": "testplace",
                    "contact_person": self.env["casemanagement.person"].create(
                        {
                            'first_name': 'testperson4',
                            'last_name': 'abcde',
                            'phone_mobile': '0701234567'
                        }).id,
                    "working_hours_description": "08-16",
                    "purpose": "test",
                    "information": "test",
                    "work_group_code": self.env["res.ssyk"].search([("code", "=", "2512")])[0].id,
                }).id,
            "union_exists": True,
            "application": created_application.id,
            "responsible_officer": user.id,
            "deciding_officer": user.id,
            "decision_date": "2022-09-20",
            "decision_period_start_date": "2022-10-01",
            "decision_period_end_date": "2022-11-01",
            "postings": [
                (0, 0, {
                    "account_code": "7931",
                    "business_code": "151001",
                    "financing_code": "70210",
                    "cost_center_code": "30002"
                })],  # Required if no RamprogramArendenummer is given
        })
        res = self.env['ipf.case_post'].post_arendehantering(basic_test_case, calling_user_login=user.login)
        _logger.info("============================ RESULT BASIC =============================")
        _logger.info(res)
        self.assertTrue("Arendenummer" in res)

        res = self.env['ipf.case_post'].post_arendehantering(full_test_case, calling_user_login=user.login)
        _logger.info("============================ RESULT FULL =============================")
        _logger.info(res)
        self.assertTrue("Arendenummer" in res)

        # TODO: change some value(s) to ones that causes the api call to fail
        failed_test_apr = self.env['casemanagement.apr_case'].create({
            "internship": self.env["casemanagement.internship"].create(
                {
                    "address_id": self.env['casemanagement.address'].create(
                        {
                            'street': 'internshipstreet',
                            'address_zip': '12345',
                            'city': 'testtown'
                        }).id,
                    "place": "testplace",
                    "contact_person": self.env["casemanagement.person"].create(
                        {
                            'first_name': 'testperson4',
                            'last_name': 'abcde',
                            'phone_mobile': '0701234567'
                        }).id,
                    "working_hours_description": "08-16",
                    "purpose": "test",
                    "information": "test",
                    "work_group_code": self.env["res.ssyk"].search([("code", "=", "2512")])[0].id,
                }).id,
            "union_exists": True,
            "application": created_application.id,
            "responsible_officer": user.id,
            "deciding_officer": user.id,
            "decision_date": "2022-05-10",
            "decision_period_start_date": "2022-05-15",
            "decision_period_end_date": "2022-06-15",
            "planned_break_start_date": "2022-06-10",  # Not required
            "planned_break_end_date": "2022-06-17",  # Not required
        })

        expected_result = {'Errors': [{'ErrorMessage': 'Det planerade uppehållets slutdatum måste inträffa '
                                                       'inom beslutsperioden.',
                                       'PropertyName': 'PlaneratUppehall.Slutdatum'}]}
        res = self.env['ipf.case_post'].post_arendehantering(failed_test_apr, calling_user_login=user.login)
        self.assertEqual(res, expected_result, "Result from API and expected result dicts are not the same")

        _logger.info('========================== TEST test_arendehantering_post PASSED ==========================')
