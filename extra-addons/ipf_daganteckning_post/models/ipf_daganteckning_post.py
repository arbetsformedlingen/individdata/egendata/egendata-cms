import logging

from odoo import models, api
from odoo.tools.safe_eval import datetime

_logger = logging.getLogger(__name__)


class IpfDaganteckningPost(models.Model):
    _name = 'ipf.daganteckning_post'
    _description = 'Model without table used to POST Daganteckning to AIS-F Daganteckning API'
    _auto = False

    @api.model
    def post_note(self, application, user, note_subject=False, note_text=False):
        """
        POSTs Daganteckning to AIS-F API:
        https://eu1.anypoint.mulesoft.com/exchange/1dae6e32-bf25-4b52-aa03-8f32e1f5fc08/ais-f-daganteckningar/
        :param application: Application which is receiving this note (casemanagement.application record)
        :param user: Case officer leaving a note, usually currently logged in user (res.users record)
        :param note_subject: Note subject
        :type note_subject: str|bool
        :param note_text: Note text, may include new line character '\n'
        :type note_text: str|bool
        """
        note = self.env.ref("af_ipf.ipf_endpoint_daganteckning").sudo()

        now = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d')

        # TODO: Verify params, these in particular: avsandandeSystem, anteckningtypId
        note_dict = {
            "sekretess": False,
            "ansvarKontor": user.office_code,  # required
            "ovrigInfo": False,
            "rubrik": note_subject,
            "ansvarSignatur": user.login or "*sys*",  # required
            "anteckningtypId": 1,  # required
            "redigerbar": "A",
            "entitetsId": application.applicant_id.job_seeker_id,  # required
            "avsandandeSystem": "AISF",  # required
            "text": note_text,
            "samtycke": True,
            "avsandandeSystemReferens": False,
            "handelsetidpunkt": now,  # required
            "lank": False
        }

        # API will complain about any included key
        # with a value that takes a field that isn't set
        # when it expects string, integer etc. since they're sent as the boolean false
        note_dict = self._clean_false_values_from_dict(
            note_dict,
            (
                "sekretess",
                "samtycke",
                "ansvarKontor",
                "ansvarSignatur",
                "anteckningtypId",
                "entitetsId",
                "avsandandeSystem",
                "handelsetidpunkt"
            )
        )

        # TODO: additional error handling when this POST fails
        return note.call(body=note_dict)

    @staticmethod
    def _clean_false_values_from_dict(d, keys_to_ignore):
        """
        Removes any key from the provided dict that has the value of False
        Use keys_to_ignore to skip any keys you want to keep even when their value is False
        """
        keys_to_remove = []
        for key in d:
            if not d[key] and key not in keys_to_ignore:
                keys_to_remove.append(key)

        for key in keys_to_remove:
            d.pop(key)
        return d
