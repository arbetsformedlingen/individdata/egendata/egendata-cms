import logging
import os

from datetime import datetime

from odoo.tests import common, tagged
from odoo.tools.safe_eval import json

_logger = logging.getLogger(__name__)

API_AIS_CLIENT_ID = os.environ.get("API_AIS_CLIENT_ID", "client_id")
API_AIS_CLIENT_SECRET = os.environ.get("API_AIS_CLIENT_SECRET", "client_secret")


@tagged('ipf')
class TestsCaseManagementModels(common.TransactionCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        super(TestsCaseManagementModels, self).setUp()

    def test_daganteckning_post(self):
        # Employers
        created_employers = self.env['casemanagement.employer'].create([
            {
                'name': 'TEST employer 1',
                'org_number': '898989-1234',
                'email': 'test@testemployer1.org',
                'address_id': self.env['casemanagement.address'].create(
                    {
                        'street': 'test',
                        'address_zip': '12345',
                        'city': 'testtown'
                    }).id,
                'contact_person': self.env['casemanagement.person'].create(
                    {
                        'first_name': 'testperson',
                        'last_name': 'abcde',
                        'phone_mobile': '0701234567'
                    }).id
            }
        ])
        # Campaign
        created_campaign = self.env['casemanagement.campaign'].create(
            {
                'date_end': datetime.now().date(),
                'employer_ids': [(6, 0, [e.id for e in created_employers])]
            })

        # Create Unions if no existing
        existing_unions = self.env['casemanagement.union'].search([])
        if not existing_unions:
            existing_unions = self.env['casemanagement.union'].create([
                {
                    'name': 'Akavia',
                    'email': 'noreply@jobtech-akavia.se'
                },
                {
                    'name': 'Jusek',
                    'email': 'noreply@jobtech-jusek.se'
                },
                {
                    'name': 'Sveriges Ingenjörer',
                    'email': 'noreply@jobtech-sako.se'
                },
                {
                    'name': 'Sveriges Arkitekter',
                    'email': 'noreply@jobtech-sveark.se'
                },
                {
                    'name': 'Naturvetarna',
                    'email': 'noreply@jobtech-naturvetarna.se'
                }
            ])

        # Applicants
        created_applicants = self.env['casemanagement.applicant'].create([
            {
                'first_name': 'TEST',
                'last_name': "testson",
                'personal_number': '199004012382',
                'job_seeker_id': 1342353,
                'email': 'test@testson.org',
                'completed_secondary_education': True,
                'samordning_av_ersattning': True,
                'address_id': self.env['casemanagement.address'].create(
                    {
                        'street': 'applicantstreet',
                        'address_zip': '12345',
                        'city': 'testtown'
                    }).id
            },
        ])

        # Application
        created_application = self.env['casemanagement.application'].create(
            {
                'name': 'TEST internship 1',
                'employer_id': created_employers[0].id,
                'applicant_id': created_applicants[0].id
            })

        # Handläggare
        # TODO: API side data validation might not accept incorrect office code for xxint
        user = self.env["res.users"].create(
            {
                "login": "xxint",
                "name": "xxint",
                "office_code": "0521",
            }
        )

        existing_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])
        self.assertFalse(existing_approvals)

        # Create Approval records for all Campaign.employer_ids and all existing Unions
        guid_mapping = created_campaign.create_union_approvals()
        self.assertIsInstance(guid_mapping, list)
        self.assertTrue(len(guid_mapping) == len(existing_unions))

        created_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])

        # Sign Approvals, now Employers should be approved
        created_approvals.sign_approval('199004012382')

        test_note_subject = 'Test note subject'
        test_note_text = """Test note longer text, it can even contain
new line characters
as you can see
here."""

        res = self.env['ipf.daganteckning_post'].post_note(created_application,
                                                           user,
                                                           test_note_subject,
                                                           test_note_text)
        _logger.info('==================== Response Object ======================')
        _logger.info(res)

        # self.assertTrue(res == dict(), "Empty dict is returned because successful POST does not return JSON back")

        note_endpoint = self.env.ref("af_ipf.ipf_endpoint_daganteckning").sudo()
        request_history = self.env['af.ipf.request.history'].search([('config_id', '=', note_endpoint.ipf_id.id)])

        payload_json = json.loads(request_history[0].payload)

        # Check IPF request history to see that response code was 200 and saved payload equals returned value
        self.assertTrue(len(request_history) == 1)
        self.assertTrue(request_history[0].response_code == '200')
        self.assertTrue(payload_json == res)

        self.assertTrue("rubrik" in payload_json)
        self.assertTrue(payload_json["rubrik"] == test_note_subject)
        self.assertTrue("text" in payload_json)
        self.assertTrue(payload_json["text"] == test_note_text)
        self.assertTrue("ansvarSignatur" in payload_json)
        self.assertTrue(payload_json["ansvarSignatur"] == user.login)
        self.assertTrue("ansvarKontor" in payload_json)
        self.assertTrue(payload_json["ansvarKontor"] == user.office_code)
        self.assertTrue("entitetsId" in payload_json)
        self.assertTrue(payload_json["entitetsId"] == created_applicants[0].job_seeker_id)

        _logger.info('================= TEST test_daganteckning_post PASSED =================')
