import csv

BASE_TEMPLATE = """<?xml version="1.0" encoding="utf-8"?>

<odoo>
    {records}
</odoo>
"""

DATA_TEMPLATE = """
    <data noupdate="1">
        <record id="{record_id}" model="res.ssyk">
{fields}
        </record>
    </data>
"""

FIELD_TEMPLATE = """            <field name="{field_name}">{field_value}</field>
"""

FIELD_REF_TEMPLATE = """            <field name="parent_id" ref="{parent_id}"/>
"""


def generate_data():
    with open('res.ssyk.csv') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        records = ""

        for row in csv_reader:
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            records += generate_row_data(row)
            line_count += 1
        print(f'Processed {line_count} lines.')

    final_data = BASE_TEMPLATE.format(records=records)

    with open('res_ssyk.xml', 'w') as f:
        f.write(final_data)
        print('Written data to XML file "{}"'.format(f.name))


def generate_row_data(row):
    """
    Generate string representation of all fields belonging to a row
    :param row: Dictionary with values
    :type row: dict
    :rtype: str
    """
    fields = ""

    for f in row:
        if f == 'id':
            continue
        if 'parent_id' in f:
            fields += FIELD_REF_TEMPLATE.format(parent_id=row[f].strip())
        else:
            fields += FIELD_TEMPLATE.format(field_name=f, field_value=row[f].strip())

    return DATA_TEMPLATE.format(record_id=row["id"], fields=fields.rstrip())


if __name__ == "__main__":
    generate_data()
