# -*- coding: utf-8 -*-
# Copyright 2017 Camptocamp (http://www.camptocamp.com).
# @author Guewen Baconnier <guewen.baconnier@camptocamp.com>
# Copyright 2017 Akretion (http://www.akretion.com).
# @author Sébastien BEAU <sebastien.beau@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import logging
import os
import threading
import re
import json

from distutils.util import strtobool
from odoo.netsvc import ColoredFormatter
from datetime import datetime

_logger = logging.getLogger(__name__)

try:
    from pythonjsonlogger.jsonlogger import JsonFormatter, RESERVED_ATTRS
except ImportError:
    JsonFormatter = object
    _logger.debug("Cannot 'import pythonjsonlogger'.")


def is_true(strval):
    return bool(strtobool(strval or '0'.lower()))


# The following regex are used to extract information from native odoo log
# We structure it the following way
# {name_of_the_log : [regex, formatter, extra_vals]}
# The extra_vals "dict" will be merged into the jsonlogger if the regex match
# In order to make the regex understandable please add a string example that
# matches
REGEX = {
    'odoo.addons.base.models.ir_cron': [
        # cron.object.execute('odooapp', 1, '*', 'Subscribe to Solid Pod server inbox', 127)
        (r"cron\.object\.execute\('[\w]+', \d, '\*', '(?P<cron_job_name>[^']+)', \d+\)",
         {}, {'cron_running': True}),

        # 0.003s (cron Subscribe to Solid Pod server inbox, server action 127 with uid 1)
        (r"(?P<cron_duration_second>[\d.]+)s \(cron (?P<cron_job_name>[^\,]+), .+",
         {'cron_duration_second': float},
         {'cron_running': False, 'cron_status': 'succeeded'}),

        # Call from cron Update res.users with office_code value from X500 API for server action #144 failed in Job #11
        (r"Call from cron (?P<cron_job_name>[^#]+) .*",
         {}, {'cron_running': False, 'cron_status': 'failed'}),
    ],
}


class OdooJsonFormatter(JsonFormatter):

    def add_fields(self, log_record, record, message_dict):
        record.pid = os.getpid()
        record.dbname = getattr(threading.currentThread(), 'dbname', '?')
        _super = super(OdooJsonFormatter, self)
        res = _super.add_fields(log_record, record, message_dict)
        if log_record['name'] in REGEX:
            for regex, formatters, extra_vals in REGEX[log_record['name']]:
                match = re.match(regex, log_record['message'])
                if match:
                    vals = match.groupdict()
                    for key, func in formatters.items():
                        vals[key] = func(vals[key])
                    log_record.update(vals)
                    if extra_vals:
                        log_record.update(extra_vals)
        return res

    def process_log_record(self, log_record):
        """
        Add some extra fields based on existing fields
        """
        added_fields = dict()

        try:
            parsed_timestamp = datetime.strptime(log_record['asctime'], '%Y-%m-%d %H:%M:%S,%f')
            added_fields['timestamp'] = datetime.strftime(parsed_timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')
        except Exception:
            pass

        if added_fields:
            log_record.update(added_fields)

        return log_record

    def jsonify_log_record(self, log_record):
        """
        Returns a json string of the log record.
        """
        return self.json_serializer(log_record,
                                    default=self.json_default,
                                    cls=self.json_encoder,
                                    indent=self.json_indent,
                                    separators=(',', ':'),  # Compact JSON without whitespaces
                                    ensure_ascii=self.json_ensure_ascii)


class OdooJsonDevFormatter(ColoredFormatter):

    def format(self, record):
        response = super(OdooJsonDevFormatter, self).format(record)
        extra = {}
        reserved_attrs = list(RESERVED_ATTRS) + ["dbname", "pid"]
        for key, value in record.__dict__.items():
            if (key not in reserved_attrs and not
                    (hasattr(key, "startswith") and key.startswith('_'))):
                extra[key] = value
        if extra:
            response += " extra: " + json.dumps(
                extra, indent=4, sort_keys=True)
        return response


def json_serializer(data):
    """
    Serialize data using this wrapper around json.dumps()
    :param data: Data to serialize
    :rtype: str
    """
    return json.dumps(data, separators=(',', ':'))


if is_true(os.environ.get('ODOO_LOGGING_JSON')):
    format = ('%(asctime)s %(pid)s %(levelname)s'
              '%(dbname)s %(name)s: %(message)s')
    # Most compact JSON without whitespaces
    # formatter = OdooJsonFormatter(format, json_indent=0, json_serializer=json_serializer)
    # formatter = OdooJsonFormatter(format, json_indent=0)
    formatter = OdooJsonFormatter(format)
    logging.getLogger().handlers[0].formatter = formatter
elif is_true(os.environ.get('ODOO_LOGGING_JSON_DEV')):
    format = ('%(asctime)s %(pid)s %(levelname)s'
              '%(dbname)s %(name)s: %(message)s')
    formatter = OdooJsonDevFormatter(format)
    logging.getLogger().handlers[0].formatter = formatter
