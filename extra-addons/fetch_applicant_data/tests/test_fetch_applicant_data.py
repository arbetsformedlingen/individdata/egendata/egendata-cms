import logging

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('ipf')
class TestFetchApplicantData(common.TransactionCase):

    def test_fetch_job_seeker_id(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Job Seeker ID',
            'personal_number': '192001059266'
        })
        self.assertFalse(new_user.job_seeker_id)

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_job_seeker_details()

        self.assertTrue(new_user.job_seeker_id == 1)
        self.assertTrue(new_user.first_name == 'Marie')
        self.assertTrue(new_user.last_name == 'Testare')

        _logger.info("========================================="
                     " IPF TEST fetch_job_seeker_id PASSED "
                     "=========================================")

    def test_fetch_education_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Education Details',
            'personal_number': '192001059266',
            'job_seeker_id': 1
        })
        new_user_higher = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User Higher',
            'last_name': 'Education Details',
            'personal_number': '193001110018',
            'job_seeker_id': 10
        })
        self.assertFalse(new_user.completed_secondary_education)
        self.assertTrue(new_user.education_level == '--')
        self.assertFalse(new_user_higher.completed_secondary_education)
        self.assertTrue(new_user_higher.education_level == '--')

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)
        self.assertIn(new_user_higher, all_applicants)

        all_applicants.fetch_education_details()

        self.assertFalse(new_user.completed_secondary_education)
        self.assertTrue(new_user.education_level == '1')

        self.assertTrue(new_user_higher.completed_secondary_education)
        self.assertTrue(new_user_higher.education_level == '4')

        _logger.info("========================================="
                     " IPF TEST fetch_education_details PASSED "
                     "=========================================")

    def test_fetch_own_business_details(self):
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User',
            'last_name': 'Own Business Details',
            'personal_number': '198103314731'
        })
        self.assertTrue(new_user.no_own_business == '--')

        all_applicants = self.env['casemanagement.applicant'].search([])
        self.assertIn(new_user, all_applicants)

        all_applicants.fetch_own_business_details()

        self.assertTrue(new_user.no_own_business == 'examine')

        _logger.info("============================================"
                     " IPF TEST fetch_own_business_details PASSED "
                     "============================================")

    def test_fetch_registered_as_unemployed_details(self):
        # Test data in T2:
        #
        # Testpnr, förnamn, efternamn, inskrivningsstatus
        # 199010102383, Inskrivning, Test One, Inskriven
        # 199008272396, Anders, Abrahamsson, Lagrad
        # 199812062397, Tara, Thornton, Utskriven avliden
        # 198812192923, Rita, Ryding, Utskriven

        # 199010102383, Inskriven
        new_user = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User 1',
            'last_name': 'Unemployed Details',
            'personal_number': '199010102383'
        })

        self.assertTrue(new_user.registration_date is False)

        new_user.fetch_registered_as_unemployed_details()

        self.assertTrue(new_user.registration_date)

        # 199008272396, Lagrad
        new_user_2 = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User 2',
            'last_name': 'Unemployed Details',
            'personal_number': '199008272396'
        })

        self.assertTrue(new_user_2.registration_date is False)

        new_user_2.fetch_registered_as_unemployed_details()

        self.assertTrue(new_user_2.registration_date is False)

        # 199812062397, Utskriven avliden
        new_user_3 = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User 3',
            'last_name': 'Unemployed Details',
            'personal_number': '199812062397'
        })

        self.assertTrue(new_user_3.registration_date is False)

        new_user_3.fetch_registered_as_unemployed_details()

        self.assertTrue(new_user_3.registration_date is False)

        # 198812192923, Utskriven
        new_user_4 = self.env['casemanagement.applicant'].create({
            'first_name': 'Test User 4',
            'last_name': 'Unemployed Details',
            'personal_number': '198812192923'
        })

        self.assertTrue(new_user_4.registration_date is False)

        new_user_4.fetch_registered_as_unemployed_details()

        self.assertTrue(new_user_4.registration_date is False)

        _logger.info("========================================================"
                     " IPF TEST fetch_registered_as_unemployed_details PASSED "
                     "========================================================")

    def test_has_secret_details(self):
        all_applicants = self.env['casemanagement.applicant']

        secrecy_1 = all_applicants.has_secret_details('195401722391')

        self.assertFalse(secrecy_1)

        # Protected/secret personal details
        secrecy_2 = all_applicants.has_secret_details('19761222-2385')

        self.assertTrue(secrecy_2)

        # Protected/secret personal details
        secrecy_3 = all_applicants.has_secret_details('770122-2395')

        self.assertTrue(secrecy_3)

        # Protected/secret personal details
        secrecy_4 = all_applicants.has_secret_details('8602092382')

        self.assertTrue(secrecy_4)

        # Not found in registry
        self.assertRaises(ValueError, all_applicants.has_secret_details, '192001059266')

        _logger.info("==========================================="
                     " IPF TEST has_secret_details PASSED "
                     "===========================================")
