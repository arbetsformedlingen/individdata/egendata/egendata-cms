from datetime import datetime
import logging

from personnummer.personnummer import Personnummer

from odoo import models, api, _

_logger = logging.getLogger(__name__)


class Applicant(models.Model):
    _inherit = 'casemanagement.applicant'

    @api.model
    def fetch_job_seeker_details(self, *args):
        """
        Fetches Job Seeker ID, first name and last name from API for a recordset
        """
        job_seeker_search = self.env.ref("af_ipf.ipf_endpoint_job_seeker_search").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with personal_number
        domain = [('personal_number', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.personal_number:
                _logger.info("Getting Job Seeker Data via API for %s and personal_number %s" % (record,
                                                                                                record.personal_number))
                job_seeker_details = job_seeker_search.call(personal_number=record.personal_number_twelve_digits)
                if len(job_seeker_details) and type(job_seeker_details) == list:
                    job_seeker_details = job_seeker_details[0]

                _logger.debug('==================== Job Seeker Details ====================')
                _logger.debug(job_seeker_details)

                if 'sokandeId' in job_seeker_details:
                    record.write({'job_seeker_id': job_seeker_details['sokandeId']})
                # TODO: When to verify that personal number belongs to this person?
                if 'fornamn' in job_seeker_details:
                    record.write({'first_name': job_seeker_details['fornamn']})
                if 'efternamn' in job_seeker_details:
                    record.write({'last_name': job_seeker_details['efternamn']})
            else:
                _logger.info("Trying to fetch Job Seeker ID via API for %s who does not have personal_number" % record)

    @api.model
    def fetch_migration_details(self, *args):
        """
        Fetches migration details from API for recordset, active_ids or all
        """

        migration = self.env.ref("af_ipf.ipf_endpoint_migration_v2").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with job_seeker_id
        domain = [('job_seeker_id', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.job_seeker_id:
                _logger.info("Getting Migration Data via API for %s and job_seeker_id %s" % (record,
                                                                                             record.job_seeker_id))
                migration_details = migration.call(job_seeker_id=int(record.job_seeker_id))

                _logger.debug('=============================== MIGRATION DETAILS =================================')
                _logger.debug(migration_details)

                if 'klassningskod' in migration_details:
                    existing_code = self.env['casemanagement.code'].search([
                        ['name', '=', str(migration_details['klassningskod']).upper()]
                    ])
                    if existing_code:
                        record.classification_code_id = existing_code[0].id
                if 'senasteUppehallstillstandSlutdatum' in migration_details:
                    try:
                        to_parse = migration_details['senasteUppehallstillstandSlutdatum']
                        if to_parse:
                            date_value = datetime.strptime(to_parse, '%Y-%m-%d').date()
                            record.residence_permit_expires_date = date_value
                    except Exception as e:
                        _logger.error('Failed to parse date from API', exc_info=e)
                if 'senasteUppehallstillstandTypsKod' in migration_details:
                    permit_code = migration_details['senasteUppehallstillstandTypsKod']
                    if permit_code:
                        record.residence_permit_type = str(permit_code).lower()
            else:
                _logger.info("Trying to fetch Migration Data via API for %s who does not have job_seeker_id" % record)

        if len(records_to_update) == 1:
            # If only 1 record updated, show form view of that record
            return {
                "type": "ir.actions.act_window",
                "res_model": "casemanagement.applicant",
                "res_id": records_to_update[0].id,
                "views": [[False, "form"]],
            }
        elif len(records_to_update) == 0:
            return {
                # "type": "ir.actions.act_window",
                # "res_model": "casemanagement.applicant",
                # "views": [[False, "tree"]],
                "view_id": self.env.ref('case_management.list_applicants_tree').id,
            }
        else:
            # Show tree view of updated records
            return {
                "name": _("Applicants"),
                "type": "ir.actions.act_window",
                "res_model": "casemanagement.applicant",
                "views": [[False, "tree"], [False, "form"]],
                "view_id": self.env.ref('case_management.list_applicants_tree').id,
                "domain": [["id", "in", [r.id for r in records_to_update]]],
            }

    @api.model
    def fetch_education_details(self, *args):
        """
        Fetches education details from API for a recordset
        """
        education = self.env.ref("af_ipf.ipf_endpoint_education").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with job_seeker_id
        domain = [('job_seeker_id', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.job_seeker_id:
                _logger.info("Getting Job Seeker Education Data via API "
                             "for %s and job_seeker_id %s" % (record, record.job_seeker_id))
                education_details = education.call(job_seeker_id=record.job_seeker_id)

                _logger.debug('==================== Job Seeker Education Details ====================')
                _logger.debug(education_details)

                if 'utbildning' in education_details:
                    if 'utbildningsniva' in education_details['utbildning']:
                        record.write({'education_level': education_details['utbildning']['utbildningsniva']})
            else:
                _logger.info("Trying to fetch Job Seeker Education Data via API for %s who "
                             "does not have job_seeker_id" % record)

    @api.model
    def fetch_own_business_details(self, *args):
        """
        Fetches details about applicant not owning a business from API for a recordset
        """
        own_business = self.env.ref("af_ipf.ipf_endpoint_bolagsverket").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with personal_number
        domain = [('personal_number', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.personal_number:
                _logger.info("Getting Job Seeker Own Business Data via API "
                             "for %s and personal_number %s" % (record, record.personal_number_ten_digits))
                business_details = own_business.call(personal_number=record.personal_number_ten_digits,
                                                     century=record.century)

                _logger.debug('==================== Job Seeker Own Business Details ====================')
                _logger.debug(business_details)

                if 'Personinformation' in business_details:
                    if 'Person' in business_details['Personinformation']:
                        if 'Foretag' in business_details['Personinformation']['Person']:
                            if not business_details['Personinformation']['Person']['Foretag']:
                                value = 'eligible'
                            else:
                                value = 'examine'
                        else:
                            value = 'eligible'
                    else:
                        value = 'examine'
                else:
                    value = '--'

                record.write({'no_own_business': value})
            else:
                _logger.info("Trying to fetch Job Seeker Own Business Data via API for %s who "
                             "does not have personal_number" % record)

    @api.model
    def fetch_registered_as_unemployed_details(self, *args):
        """
        Fetches details about applicant's status as unemployed from API for a recordset
        """
        unemployed_status = self.env.ref("af_ipf.ipf_endpoint_inskrivning").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with personal_number
        domain = [('personal_number', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.personal_number:
                _logger.info("Getting Job Seeker Unemployment Status Data via API "
                             "for %s and personal_number %s" % (record, record.personal_number_twelve_digits))
                unemployed_details = unemployed_status.call(personal_number=record.personal_number_twelve_digits)

                _logger.debug('==================== Job Seeker Unemployment Status Details ====================')
                _logger.debug(unemployed_details)

                if 'ar_inskriven' in unemployed_details and 'inskrivningsdatum' in unemployed_details:
                    if unemployed_details['ar_inskriven'] is True and \
                            unemployed_details['tillatet_personnummer'] is True:
                        try:
                            value = datetime.strptime(unemployed_details['inskrivningsdatum'], '%Y-%m-%d').date()
                            record.write({'registration_date': value})
                        except Exception as e:
                            _logger.error('Failed to parse date from API', exc_info=e)

                unemployed_full = self.env.ref("af_ipf.ipf_endpoint_inskrivning_full").sudo()
                unemployed_full_details = unemployed_full.call(personal_number=record.personal_number_twelve_digits)
                update_dict = dict()

                if 'sokandekategori_kod' in unemployed_full_details:
                    update_dict['seeker_category_code'] = unemployed_full_details['sokandekategori_kod']
                if 'ansvarig_handlaggare' in unemployed_full_details:
                    update_dict['af_case_worker'] = unemployed_full_details['ansvarig_handlaggare']
                if 'ansvarigt_kontor_kod' in unemployed_full_details:
                    update_dict['af_office_code'] = unemployed_full_details['ansvarigt_kontor_kod']

                if update_dict:
                    record.write(update_dict)
            else:
                _logger.info("Trying to fetch Job Seeker Unemployment Status Data via API for %s who "
                             "does not have personal_number" % record)

    @api.model
    def fetch_contact_details(self, *args):
        """
        Fetches contact (phone number, email) details from API for a recordset
        """
        contact = self.env.ref("af_ipf.ipf_endpoint_contact").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with job_seeker_id
        domain = [('job_seeker_id', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.job_seeker_id:
                _logger.info("Getting Job Seeker Contact Data via API "
                             "for %s and job_seeker_id %s" % (record, record.job_seeker_id))
                contact_details = contact.call(job_seeker_id=record.job_seeker_id)

                _logger.debug('==================== Job Seeker Contact Details ====================')
                _logger.debug(contact_details)

                if 'kontaktuppgifter' in contact_details:
                    details = contact_details['kontaktuppgifter']
                    if 'epost' in details and details['epost']:
                        record.email = details['epost']
                    if 'telefonBostad' in details and details['telefonBostad']:
                        record.phone_home = details['telefonBostad']
                    if 'telefonMobil' in details and details['telefonMobil']:
                        record.phone_mobile = details['telefonMobil']
                    if 'telefonArbetet' in details and details['telefonArbetet']:
                        record.phone_work = details['telefonArbetet']
            else:
                _logger.info("Trying to fetch Job Seeker Contact Data via API for %s who "
                             "does not have job_seeker_id" % record)

    @api.model
    def fetch_office_details(self, *args):
        """
        Fetches AF office details (local office name/code, responsible case officer) from API for a recordset
        """
        office = self.env.ref("af_ipf.ipf_endpoint_office").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with job_seeker_id
        domain = [('job_seeker_id', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.job_seeker_id:
                _logger.info("Getting Job Seeker AF Office Data via API "
                             "for %s and job_seeker_id %s" % (record, record.job_seeker_id))
                office_details = office.call(job_seeker_id=record.job_seeker_id)

                _logger.debug('==================== Job Seeker AF Office Details ====================')
                _logger.debug(office_details)

                if 'kontor' in office_details:
                    details = office_details['kontor']
                    if 'kontorsKod' in details and details['kontorsKod']:
                        record.af_office_code = details['kontorsKod']
                    if 'kontorsNamn' in details and details['kontorsNamn']:
                        record.af_office_name = details['kontorsNamn']
                    if 'ansvarigHandlaggareSignatur' in details and details['ansvarigHandlaggareSignatur']:
                        record.af_case_worker = details['ansvarigHandlaggareSignatur']
            else:
                _logger.info("Trying to fetch Job Seeker AF Office Data via API for %s who "
                             "does not have job_seeker_id" % record)

    @api.model
    def fetch_country_details(self, *args):
        """
        Fetches country of birth details from API for a recordset
        """
        person = self.env.ref("af_ipf.ipf_endpoint_country").sudo()

        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with job_seeker_id
        domain = [('personal_number', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.personal_number:
                _logger.info("Getting Job Seeker country of birth Data via API "
                             "for %s and personal_number %s" % (record, record.personal_number_twelve_digits))
                person_details = person.call(personal_number=record.personal_number_twelve_digits)

                _logger.debug('==================== Job Seeker country of birth Details ====================')
                _logger.debug(person_details)

                if 'personpost' in person_details:
                    details = person_details['personpost']
                    if 'fodelse' in details and details['fodelse']:
                        birth = details['fodelse']
                        if 'ort_utlandet' in birth and birth['ort_utlandet']:
                            foreign = birth['ort_utlandet']
                            if 'fodelseland' in foreign and foreign['fodelseland']:
                                record.origin_country = self._get_origin_country(foreign['fodelseland'])
                        elif 'hemort_sverige' in birth and birth['hemort_sverige']:
                            swedish = birth['hemort_sverige']
                            if 'fodelseforsamling' in swedish and swedish['fodelseforsamling']:
                                record.origin_country = 'sweden'
            else:
                _logger.info("Trying to fetch Job Seeker country of birth Data via API for %s who "
                             "does not have personal_number" % record)

    @api.model
    def has_secret_details(self, personal_number):
        """
        Fetches secrecy details from API for a given personal number
        :param personal_number: Valid personal number to fetch details for
        :type personal_number: str
        :return: True if personal details are secret, False otherwise
        :rtype: bool
        :raises: ValueError
        """
        secrecy = self.env.ref("af_ipf.ipf_endpoint_secrecy").sudo()

        if self.env['casemanagement.validation'].valid_personal_number(personal_number):
            # API expects valid personal number, 12 digits, like 'YYYYMMDDXXXX'
            twelve_digits = Personnummer(personal_number).format(long_format=True)

            _logger.info('Getting Secrecy Data via API for personal_number %s' % personal_number)
            payload = {
                "sokvillkor": [
                    {
                        "identitetsbeteckning": "{}".format(twelve_digits)
                    }
                ]
            }
            secrecy_details = secrecy.call(body=payload)

            _logger.debug('==================== Secrecy Details ====================')
            _logger.debug(secrecy_details)

            if 'folkbokforingsposter' in secrecy_details:
                details = secrecy_details['folkbokforingsposter']

                if not details:
                    raise ValueError('"folkbokforingsposter" present but empty in response')

                for p in details:
                    if 'skyddAvPersonuppgifter' in p:
                        if p['skyddAvPersonuppgifter'] != 'SAKNAS':
                            return True
                return False
            else:
                raise ValueError('"folkbokforingsposter" not found in response')
        else:
            _logger.info('Trying to fetch Secrecy Data with invalid personal_number' % personal_number)
            raise ValueError('Invalid personal number')

    @staticmethod
    def _get_origin_country(country_name):
        """
        Map country to Sweden/Scandinavia/EU/non-EU value, if possible
        :type country_name: str
        :rtype: str
        """
        _logger.debug('=================== _get_origin_country() COUNTRY ===================')
        _logger.debug(country_name)

        country_name = str(country_name).upper().strip()

        if not country_name:
            return '--'

        if country_name == 'SVERIGE':
            return 'sweden'

        # Scandinavia (Norden) without Sweden, in Swedish, upper-case
        scandinavia = [
            'DANMARK',
            'FINLAND',
            'NORGE',
            'ISLAND',
            'GRÖNLAND',
            'FÄRÖARNA',
            'ÅLAND',
        ]
        if country_name in scandinavia:
            return 'scandinavia'

        # EU countries without Sweden, in Swedish, upper-case
        eu = [
            'BELGIEN',
            'BULGARIEN',
            'KROATIEN',
            'CYPERN',
            'TJECKIEN',
            'ESTLAND',
            'FRANKRIKE',
            'GREKLAND',
            'UNGERN',
            'IRLAND',
            'ITALIEN',
            'LETTLAND',
            'LITAUEN',
            'LUXEMBURG',
            'MALTA',
            'NEDERLÄNDERNA',
            'POLEN',
            'PORTUGAL',
            'RUMÄNIEN',
            'SLOVAKIEN',
            'TYSKLAND',
            'SLOVENIEN',
            'SPANIEN',
            'ÖSTERRIKE',
        ]
        if country_name in eu:
            return 'eu'

        return 'non-eu'

    @api.model
    def fetch_all_details(self, *args):
        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Get records with job_seeker_id
        domain = [('personal_number', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        # Fetch all data
        records_to_update.fetch_job_seeker_details()
        records_to_update.fetch_migration_details()
        records_to_update.fetch_education_details()
        records_to_update.fetch_own_business_details()
        records_to_update.fetch_registered_as_unemployed_details()
        records_to_update.fetch_contact_details()
        records_to_update.fetch_office_details()
        records_to_update.fetch_country_details()

    @api.onchange('personal_number')
    def _onchange_personal_number(self):
        """
        Fetch all Applicant data from API when personal number is updated in native Odoo frontend
        """
        _logger.debug('======================= _onchange_personal_number START =================')

        for record in self:
            fetched_applicant = self.env['casemanagement.applicant'].browse(
                self._get_record_id_from_pseudo_record(record)
            ).sudo()
            # Update personal_number immediately because API calls use it
            fetched_applicant.personal_number = record.personal_number

            fetched_applicant.fetch_all_details()
        _logger.debug('======================= _onchange_personal_number FINISH =================')

    @staticmethod
    def _get_record_id_from_pseudo_record(rec):
        return int(str(rec.id).split('_')[-1])
