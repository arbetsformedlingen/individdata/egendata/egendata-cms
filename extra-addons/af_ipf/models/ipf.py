# -*- coding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution, third party addon
#    Copyright (C) 2020 Vertel AB (<http://vertel.se>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import json
import logging
import os

import requests
from odoo.exceptions import UserError
from requests.auth import HTTPBasicAuth
from uuid import uuid4

from odoo import models, fields
from odoo.http import request
from odoo.tools import date_utils

_logger = logging.getLogger(__name__)

API_AIS_CLIENT_ID = os.environ.get("API_AIS_CLIENT_ID", "client_id")
API_AIS_CLIENT_SECRET = os.environ.get("API_AIS_CLIENT_SECRET", "client_secret")
IPF_CUSTOM_CERT = os.environ.get("IPF_CUSTOM_CERT")
API_AIS_JWT_URL = os.environ.get("API_AIS_JWT_URL")
API_AIS_APP_ID = os.environ.get("API_AIS_APP_ID")


def _get_request_object():
    """ Fetch the current request object, if one exists. We often run
    this code in sudo, so self.env.user is not reliable, but the
    request object always has the actual user.
    """
    try:
        # Poke the bear
        request.env
        # It's alive!
        return request
    except Exception:
        # No request is available
        return


class AfIpf(models.Model):
    _name = 'af.ipf'
    _description = 'IPF Integration'

    name = fields.Char(required=True)
    client_id = fields.Char(string='Client ID', help="Found in IPF portal")
    client_secret = fields.Char(string='Client Secret', help="Found in IPF portal")
    auth_user = fields.Char()
    auth_password = fields.Char()
    systemid = fields.Char(default='JobtechCMS')
    environment = fields.Selection(selection=[
        ('U1', 'U1'),
        ('I1', 'I1'),
        ('T1', 'T1'),
        ('T2', 'T2'),
        ('PROD', 'PROD'),
        ('OTHER', 'OTHER')],
        default='T2',
        required=True)
    enduserid = fields.Boolean()
    enduserid_hardcoded = fields.Boolean()
    endpoint_ids = fields.One2many(comodel_name='af.ipf.endpoint', inverse_name='ipf_id')
    url = fields.Char(string='IPF url', help="AF's web address", default='https://ipf-acc.arbetsformedlingen.se',
                      required=True)
    port = fields.Integer(string='IPF port', help="Af's port, default 443", default=443, required=True)
    ssl_verify = fields.Char(default=IPF_CUSTOM_CERT)
    ssl_cert = fields.Char()
    ssl_key = fields.Char()
    use_jwt = fields.Boolean(default=False, help='Some APIs need additional header "X-JWT-Assertion" sent '
                                                 'along with a request')

    def get_auth(self):
        self.ensure_one()
        if self.auth_user and self.auth_password:
            return HTTPBasicAuth(self.auth_user, self.auth_password)

    def get_headers(self, calling_user_login=False):
        """
        :type calling_user_login: str/bool
        """
        self.ensure_one()
        headers = {
            'AF-SystemId': self.systemid,
            'AF-Environment': self.environment,
            'AF-TrackingId': '%s' % uuid4(),
        }
        if self.enduserid:
            if self.enduserid_hardcoded:
                headers['AF-EndUserId'] = '*sys*'
            else:
                login = self.get_current_user().login if not calling_user_login else calling_user_login
                # update with new signature field
                if login:
                    headers['AF-EndUserId'] = login
            if headers['AF-EndUserId'] == '__system__':
                headers['AF-EndUserId'] = '*sys*'
        if self.use_jwt:
            token = self.get_jwt_token(calling_user_login=calling_user_login)
            if token:
                headers['X-JWT-Assertion'] = token
        return headers

    def get_ssl_params(self):
        ssl_params = {}
        if not self.ssl_verify:
            ssl_params['verify'] = False
        elif self.ssl_verify == 'True':
            ssl_params['verify'] = True
        else:
            ssl_params['verify'] = self.ssl_verify
        if self.ssl_cert and self.ssl_key:
            ssl_params['cert'] = (self.ssl_cert, self.ssl_key)
        return ssl_params

    def get_jwt_token(self, calling_user_login=False):
        """
        :type calling_user_login: bool/str
        """
        login = self.get_current_user().login if not calling_user_login else calling_user_login
        jwt_url = '%s%s' % (API_AIS_JWT_URL, API_AIS_APP_ID)

        headers = {
            "PISA_ID": login if len(login) == 5 else "xxint",
            "PISA_AUTH_METHOD": "ELEG",
        }
        try:
            # TODO: make sure no adjustments have to be made for PROD environment
            ssl_params = dict(verify=False) if self.environment != 'PROD' else self.get_ssl_params()
            # TODO: re-use token that has not expired yet, if applicable
            jwt_response = requests.get(jwt_url, headers=headers, **ssl_params).json()

            if "token" in jwt_response:
                _logger.debug("JWT response TOKEN: %s" % json.dumps(jwt_response, indent=4))
                return jwt_response["token"]
            else:
                _logger.debug("JWT response: %s" % jwt_response.text)
        except Exception as e:
            _logger.error("Problem when getting JWT token:")
            _logger.error(e, exc_info=e)

        return 'No real JWT token'

    def get_current_user(self):
        """
        Get current user either from request object or odoo environment
        """
        req = _get_request_object()
        if req:
            user = req.env.user
        else:
            user = self.env.user
        return user


class AfIpfEndpoint(models.Model):
    _name = 'af.ipf.endpoint'
    _description = 'IPF Endpoint'

    name = fields.Char(required=True)
    ipf_id = fields.Many2one(comodel_name='af.ipf', required=True, ondelete='cascade')
    method = fields.Char(required=True, default='get')
    test_value = fields.Char()
    test_value_2 = fields.Char()

    def build_error_msg(self, response, data):
        """Build error message from JSON response."""
        return "%s: %s [%s] %s" % (self.ipf_id.name, self.name, response.status_code, data)

    def call(self, raise_on_error=False, **kw):
        self.ensure_one()
        headers = self.ipf_id.get_headers(calling_user_login=kw.get('calling_user_login'))
        if 'AF-EndUserId' in headers and not headers['AF-EndUserId']:
            # This integration requires a signature, but user doesn't have one.
            if raise_on_error:
                raise UserError("AF-EndUserId required and is missing")
            return
        kw.update({
            'client_id': self.ipf_id.client_id or API_AIS_CLIENT_ID,
            'client_secret': self.ipf_id.client_secret or API_AIS_CLIENT_SECRET,
        })

        url = '%s:%s/%s' % (
            self.ipf_id.url,
            self.ipf_id.port,
            self.name.format(**kw))
        _logger.debug("Unpack url: %s" % url)
        _logger.debug("Unpack headers: %s" % json.dumps(headers, indent=4))

        payload = dict()
        try:
            _logger.debug("Unpack raw payload: %s" % kw.get('body'))
            payload = json.dumps(kw.get('body', {}), default=date_utils.json_default)
            headers['Content-Type'] = 'application/json'
        except Exception as e:
            _logger.error("Failed to parse JSON as payload to send", exc_info=e)
            _logger.error("Unpack raw payload: '%s'" % kw.get('body'))

        response = None
        try:
            method = self.method.lower()
            if hasattr(requests, method):
                method_function = getattr(requests, method)
                response = method_function(
                    url,
                    headers=headers,
                    auth=self.ipf_id.get_auth(),
                    **self.ipf_id.get_ssl_params(),
                    data=payload
                )
        except Exception as e:
            _logger.error('Error in IPF call method', exc_info=e)
            raise e

        _logger.debug("Unpack response.status_code: %s" % response.status_code)
        _logger.debug("Unpack response: %s" % response.__dict__)

        res = dict()
        try:
            res = response.json()
        except ValueError as e:
            _logger.error("Failed to parse JSON from response", exc_info=e)
            _logger.debug("Unpack response.content: '%s'" % response.content)

        if response and response.status_code not in (200, 201):
            error_msg = self.build_error_msg(response, res)
            _logger.warning(error_msg)
            if raise_on_error:
                raise UserError(error_msg)
        _logger.debug("Unpack response JSON: %s" % res)

        self.create_request_history(method, url, response, payload=payload, headers=headers, params=kw)

        return res

    def create_request_history(self, method, url, response, payload=False, headers=False, params=False):
        """
        Creates request history record with more details if failed
        :param method: HTTP method
        :type method: str
        :param url: URL that was called
        :type method: str
        :param response: HTTP response
        :param payload: Optional JSON payload
        :type payload: str|bool
        :param headers: HTTP headers
        :type headers: dict|bool
        :param params: Dictionary with parameters used in URL or JSON body
        :type params: dict|bool
        """
        values = {'config_id': self.ipf_id.id,
                  'method': method,
                  'url': url,
                  'payload': payload,
                  'request_headers': headers,
                  'response_headers': response.headers,
                  'params': params,
                  'response_code': response.status_code}

        if response.status_code == 404:
            pass
        elif response.status_code not in (200, 201):
            values.update(message=json.loads(response.content))
        self.env['af.ipf.request.history'].create(values)

    def test_ipf_endpoint(self):
        for rec in self:
            kw = {}
            if self.name.find("{"):
                parts = self.name.split("{")
                value_1 = parts[1].split("}")[0]
                kw = {
                    value_1: self.test_value,
                }
                if len(parts) > 2:
                    value_2 = parts[2].split("}")[0]
                    kw[value_2] = self.test_value_2
            if self.test_value[0] == "{":
                kw["body"] = self.test_value
            rec.call(**kw)
