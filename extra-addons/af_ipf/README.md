## API edpoints used by CMS are outlined below

```mermaid
graph LR
    A((CMS))
    A -->|/ais-arendehantering/v2/apr/anvisningar| F(AIS Arendehantering / APR)
    A -->|/ais-f-daganteckningar/v1/anteckning| G(AIS-F Daganteckningar)
    A -.->|/ais-f-arbetssokande/v1/arbetssokande/sok| B(AIS-F Arbetssokande)
    A -.->|/ais-f-arbetssokande/v1/arbetssokande/migration| B
    A -.->|/ais-f-arbetssokande/v1/arbetssokande/utbildning| B
    A -.->|/inskrivning/v1/arbetssokande/personnummer/status-inskrivning| C(AIS-F Inskrivning)
    A -.->|/bolagsverket/v1/personinformation| D(Bolagsverket)
    A -.->|/x500-af-person/v1/af-persons| E(x500 AF Person)
    
    subgraph GET
        B & C & D & E
    end

    subgraph POST
        F & G
    end
```
