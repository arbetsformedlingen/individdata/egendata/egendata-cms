# Setup instructions

## Repository

Clone this repository:

`git clone https://gitlab.com/arbetsformedlingen/individdata/egendata/egendata-cms.git`

## Docker Image

`cd egendata-cms`

Build Docker image and tag it with `odoo-jobtech:15.0` (this image is used in `docker-compose.yml` file):

`docker build -t odoo-jobtech:15.0 .`

After successful build, the whole stack can be run with `docker-compose` command (from inside directory containing the `docker-compose.yml` file):

`docker-compose up -d`

If the above step goes well, navigate to [localhost:8069](http://localhost:8069/) and login with `admin:admin`.

## Development

By default, `docker-compose.yml` enables automatic server reloading when Python files are modified (might not work on Windows). This is done with `--dev xml,reload` flag.

The module `case_management` is installed by default, achieved with the `-i base,case_management` flag.

Tests marked with `casemanagement` are run by default on server restart, achieved with the `--test-tags casemanagement` flag.

(If automatic reloading does not work) After changing code, restart `odoo` container either in Docker GUI or on command line:

`docker stop $(docker ps -f name=odoo --quiet) && docker start odoo`

Go to [localhost:8069](http://localhost:8069/) and login with `admin:admin`.

Go to [localhost:8069/casemanagement/](http://localhost:8069/casemanagement/) to see the default module page. This route is serving an Angular App located in the `frontend` directory.

## Manual Module (Re)Install

Find `case_management` module on the [module list page](http://localhost:8069/web#model=ir.module.module&view_type=kanban) (remove the default App filter to see modules).

Click the install button and wait until the first time setup is finished (alternatively, check the logs for `odoo` container to see error messages).

Some sample data (including Employer, Applicant, Application records) is created when `case_management` module is installed.

**Congratulations!** Now the local development environment should be up and running.

## Code contribution

Code contribution is done via a **Merge Request**.

> It is important to supply a clear description of a change that is being suggested. In most cases it should not be harder than a good and clear subject line.

**Process for those who want to contribute**

1. Clone repo: `$ git clone https://gitlab.com/arbetsformedlingen/individdata/egendata/egendata-cms.git`
2. Create a new branch: `$ git checkout -b cool_new_design_with_awesome_content`
3. Do your changes and test them
4. Push your changes: `$ git push origin cool_new_design_with_awesome_content`
5. GitLab will reply with the URL you have to visit in order to create a Merge Request. Visit that URL and make sure to include a clear description of your changes.

**Code approval process**

By sending in a Merge Request you allow the owner of the repo to choose to accept or deny your changes.

Contributions can only be made if you can answer YES to both questions below:
1. Do you license your contribution according to the MIT license?
2. If you work on behalf of your employer, do you have their permission to make this contribution?

## License

MIT
