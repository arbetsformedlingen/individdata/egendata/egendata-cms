import jwcrypto
import jwcrypto.jwk
import jwcrypto.jwt
import json
import base64
import os
import re
import datetime

from solidclient.solid_client import SolidClient


def make_random_string():
    x = base64.urlsafe_b64encode(os.urandom(40)).decode('utf-8')
    x = re.sub('[^a-zA-Z0-9]+', '', x)
    return x


def make_token_for(keypair, uri, method):
    print(keypair)
    jwt = jwcrypto.jwt.JWT(header={
        "typ":
            "dpop+jwt",
        "alg":
            "ES256",
        "jwk":
            keypair.export(private_key=False, as_dict=True)
    },
        claims={
            "jti": make_random_string(),
            "htm": method,
            "htu": uri,
            "iat": int(datetime.datetime.now().timestamp())
        })
    jwt.make_signed_token(keypair)
    return jwt.serialize()


def make_headers(url, method):
    with open('sensitivedata.dat', 'r') as stored_keys:
        json_loaded = json.loads(stored_keys.read())
        keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
        access_token = json_loaded["access_token"]
        headers = {
            "Authorization": ("DPoP " + access_token),
            "DPoP": make_token_for(keypair, url, method)
        }
        return headers


def main():
    # url = "http://localhost:3000/source-test-se/profile/"
    url = "http://localhost:3000/user123/profile/"

    headers = make_headers(url, 'GET')
    # print(headers)

    keypair = None
    access_token = None

    filename = "source-test-se_user.key"
    with open(filename, "r") as stored_keys:
        json_loaded = json.loads(stored_keys.read())
        keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
        access_token = json_loaded["access_token"]

    # foo.SolidClient()
    solid_client = SolidClient(keypair, access_token)
    solid_session = solid_client.login_key_access_token()
    print(solid_session)

    # print("GET user123/profile/")
    # resp = solid_session.get("http://localhost:3000/user123/profile/")
    # print(resp.get_graph())

    resource_name = "share"
    user_pod_name = "user123"
    # print("GET %s/%s/" % (user_pod_name, resource_name))
    # resp2 = solid_session.get("http://localhost:3000/%s/%s/" % (user_pod_name, resource_name))
    # print(resp2.get_graph())

    # print("GET %s/%s/.acl" % (user_pod_name, resource_name))
    # resp3 = solid_session.get("http://localhost:3000/%s/%s/" % (user_pod_name, resource_name))
    # print(resp3.get_graph())

    ### Source PUTs migrationsverket data into user123's POD
    # migrationsverket_data = None
    # with open("data/migrationsverket.ttl", "r") as migrationsverket_data_file:
    #           migrationsverket_data = migrationsverket_data_file.read()

    #           print("PUT %s/%s/migrationsverket.ttl" % (user_pod_name, resource_name))
    #           uri = "http://localhost:3000/%s/%s/migrationsverket.ttl" % (user_pod_name, resource_name)
    #           resp = solid_session.put(uri, migrationsverket_data)
    #           print(resp.get_graph())
    #           print(resp.status_code)

    ### Source PUTs bolagsverket data into user123's PO# D
    # bolagsverket_data = None
    # with open("data/bolagsverket.ttl", "r") as bolagsverket_data_file:
    #     bolagsverket_data = bolagsverket_data_file.read()

    #     print("PUT %s/%s/bolagsverket.ttl" % (user_pod_name, resource_name))
    #     uri = "http://localhost:3000/%s/%s/bolagsverket.ttl" % (user_pod_name, resource_name)
    #     resp = solid_session.put(uri, bolagsverket_data)
    #     print(resp.get_graph())
    #     print(resp.status_code)

    ### Source PUTs skatteverket data into user123's POD
    skatteverket_data = None
    with open("data/skatteverket.ttl", "r") as skatteverket_data_file:
        skatteverket_data = skatteverket_data_file.read()

        print("PUT %s/%s/skatteverket.ttl" % (user_pod_name, resource_name))
        uri = "http://localhost:3000/%s/%s/skatteverket.ttl" % (user_pod_name, resource_name)
        resp = solid_session.put(uri, skatteverket_data)
        print(resp.get_graph())
        print(resp.status_code)

    # acl_file = None
    # with open("acls/user123_share_acl.ttl", "r") as user123_share_acl:
    #     acl_file = user123_share_acl.read()

    # print(acl_file)

    # if acl_file is not None:
    #     acl_uri = "http://localhost:3000/%s/%s/.acl" % (user_pod_name, resource_name)
    #     resp3 = solid_session.put(acl_uri, acl_file)
    #     print(resp3.status_code)
    #     print(resp3.raw_text)

    # resp = solid_session.get("http://localhost:3000/source-test-se/share/.acl")
    # print(resp.get_graph())

    # resource_name = "hej"

    # resp2 = solid_session.get("http://localhost:3000/source-test-se/hej/.acl")
    # if resp2.status_code == 200:
    #     print(resp2.get_graph())
    # else:
    #     print(".acl not found")

    # acl_file = None
    # with open("source_acl.ttl", "r") as source_acl:
    #     acl_file = source_acl.read()

    # print(acl_file)

    # if acl_file is not None:
    #     resp3 = solid_session.put("http://localhost:3000/source-test-se/hej/.acl", acl_file)
    #     print(resp3.status_code)
    #     print(resp3.raw_text)

    # resp4 = solid_session.delete("http://localhost:3000/source-test-se/hej/.acl")
    # print(resp4.status_code)
    # print(resp4.raw_text)


main()
